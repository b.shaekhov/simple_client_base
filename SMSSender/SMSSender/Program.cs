﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using System.Net;

namespace SMSSender
{
    class Program
    {
        private static string Login = null;
        private static string Password = null;
        private static string Sender = null;

        static void Main(string[] args)
        {
            if (!Directory.Exists("Temp"))
                Directory.CreateDirectory("Temp");
            if (!Directory.Exists("Configs"))
                Directory.CreateDirectory("Configs");

            var sendDataFileName = @"Temp\SMSSendData.txt";
            var autorizeFileName = @"Configs\SMSSenderConfig.txt";

            if (!File.Exists(autorizeFileName))
            {
                File.WriteAllText(autorizeFileName, JsonConvert.SerializeObject(new AutorizationData() { login = "", password = "", sender = "" }, Formatting.Indented));
                return;
            }

            if (!File.Exists(sendDataFileName))
                return;

            var rawAutorizeData = File.ReadAllText(autorizeFileName);
            var autorizeData = JsonConvert.DeserializeObject<AutorizationData>(rawAutorizeData);

            Login = autorizeData.login;
            Password = autorizeData.password;
            Sender = autorizeData.sender;

            var rawSendData = File.ReadAllText(sendDataFileName);

            var sendData = JsonConvert.DeserializeObject<SMSSendData>(rawSendData);

            var messages = new List<object>();

            bool emptySender = string.IsNullOrEmpty(Sender);

            int clientsCounter = 0;

            foreach (var rawData in sendData.Data)
            {
                foreach (var phoneNum in rawData.PhoneNums)
                {
                    if (emptySender)
                    {
                        messages.Add(new SiteFormatContainer.SiteMessageWSender()
                        {
                            phone = phoneNum,
                            //sender = Sender,
                            clientId = clientsCounter,
                            text = rawData.Message
                        });
                    }
                    else
                    {
                        messages.Add(new SiteFormatContainer.SiteMessage()
                        {
                            phone = phoneNum,
                            sender = Sender,
                            clientId = clientsCounter,
                            text = rawData.Message
                        });
                    }

                    clientsCounter++;
                }
            }

            bool error = false;

            while (messages.Count > 0)
            {
                var startIndex = Math.Max(0, messages.Count - 195);
                var sendRange = messages.GetRange(startIndex, messages.Count - startIndex).ToArray();
                messages.RemoveRange(startIndex, messages.Count - startIndex);

                var sendContainer = new SiteFormatContainer();
                sendContainer.messages = sendRange;
                sendContainer.login = Login;
                sendContainer.password = Password;

                string sendJsonText = JsonConvert.SerializeObject(sendContainer);

                if (autorizeData.SendSMS)
                {
                    try
                    {
                        var rawAns = POST("http://api.iqsms.ru/messages/v2/send.json", sendJsonText);

                        var ans = JsonConvert.DeserializeObject<SiteAnswer>(rawAns);

                        if (ans.status.ToLower() != "ok")
                        {
                            Console.WriteLine("Ошибка во время рассылки!");
                            Console.WriteLine("status: " + ans.status);
                            error = true;
                        }
                        else
                        {
                            foreach (var mes in ans.messages)
                            {
                                if (mes.status.ToLower() != "accepted")
                                {
                                    if (mes.status.ToLower() == "sender address invalid")
                                    {
                                        Console.WriteLine("Неверное имя отправителя!");
                                        error = true;
                                        break;
                                    }

                                    if (mes.status.ToLower() == "text is empty")
                                    {
                                        Console.WriteLine("Попытка отправить пустой текст!");
                                        error = true;
                                        break;
                                    }

                                    Console.WriteLine("Ошибка в отправке на номер" + GetPhoneFromMessage(messages[mes.clientId]) + " ошибка: " + mes.status);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Ошибка в запросе!\r\n" + e.Message);
                        error = true;
                        //Console.ReadLine();
                    }
                } else
                {
                    Console.WriteLine("Сообщение не отправлено, в соответствии с настройками");
                    Console.WriteLine();
                    Console.WriteLine(sendJsonText);
                    error = true;
                }
            }

            if (error)
                Console.ReadLine();
        }

        private static string GetPhoneFromMessage(object mess)
        {
            return (mess is SiteFormatContainer.SiteMessageWSender 
                    ? ((SiteFormatContainer.SiteMessageWSender)mess).phone 
                    : ((SiteFormatContainer.SiteMessage       )mess).phone);
        }

        private static string POST(string Url, string Data)
        {
            WebRequest req = WebRequest.Create(Url);
            req.Method = "POST";
            req.Timeout = 100000;
            req.ContentType = "application/x-www-form-urlencoded";
            byte[] sentData = Encoding.UTF8.GetBytes(Data);
            req.ContentLength = sentData.Length;
            Stream sendStream = req.GetRequestStream();
            sendStream.Write(sentData, 0, sentData.Length);
            sendStream.Close();
            WebResponse res = req.GetResponse();
            Stream ReceiveStream = res.GetResponseStream();
            StreamReader sr = new StreamReader(ReceiveStream, Encoding.UTF8);
            //Кодировка указывается в зависимости от кодировки ответа сервера
            Char[] read = new Char[256];
            int count = sr.Read(read, 0, 256);
            string Out = String.Empty;
            while (count > 0)
            {
                String str = new String(read, 0, count);
                Out += str;
                count = sr.Read(read, 0, 256);
            }
            return Out;
        }

        class AutorizationData
        {
            public string login = null;
            public string password = null;
            public string sender = null;
            public bool SendSMS = true;
        }

        class SMSSendData
        {
            public ClientData[] Data = null;

            public class ClientData
            {
                public string[] PhoneNums = null;
                //public string Sender = null;
                public string Message = null;
            }
        }
        
        class SiteFormatContainer
        {
            //public string scheduleTime;

            public object[] messages;

            //public string statusQueueName = "myQueue";

            public bool showBillingDetails = true;

            public string login;
            public string password;

            public class SiteMessage
            {
                public string phone = "";
                public string sender = "";
                public int clientId = 0;
                public string text = "";
            }

            public class SiteMessageWSender
            {
                public string phone = "";
                //public string sender = "";
                public int clientId = 0;
                public string text = "";
            }
        }

        class SiteAnswer
        {
            public string status = null;
            public Balance balance = null;

            public AnswerMessage[] messages = null;

            public class Balance
            {
                public float credit = 0;
                public float balance = 0;
                public string type = null;
            }

            public class AnswerMessage
            {
                public string status = null;
                public string smscId = null;
                public int clientId = 0;
            }
        }
    }
}
