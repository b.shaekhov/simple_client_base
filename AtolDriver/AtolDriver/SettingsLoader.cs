﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace AtolDriver
{
    public static class SettingsLoader
    {
        const string ConfigsPath = "Configs/AtolDriverConfig.txt";
        public static SettingsContainer ReadedSettings;

        static SettingsLoader()
        {
            CheckOrCreateConfigs();
        }

        private static void CheckOrCreateConfigs()
        {
            var dir = Path.GetDirectoryName(ConfigsPath);

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            if (File.Exists(ConfigsPath))
                ReadedSettings = JsonConvert.DeserializeObject<SettingsContainer>(File.ReadAllText(ConfigsPath));
            else
            {
                ReadedSettings = new SettingsContainer();
                File.WriteAllText(ConfigsPath, JsonConvert.SerializeObject(ReadedSettings, Formatting.Indented));
            }
        }

        public class SettingsContainer
        {
            [JsonProperty(PropertyName = "Тип_операции")]
            public string TypeName = "sell";
            [JsonProperty(PropertyName = "Электронный_тип_оплаты")]
            public bool ElectronicallyPay = false;
            [JsonProperty(PropertyName = "Тип_налогообложения")]
            public string TaxType = "envd";
            [JsonProperty(PropertyName = "Точка_продаж")]
            public string SellPlace = "Альметьевск. Пушкина, 46А.";
            [JsonProperty(PropertyName = "Тип_элементарной_операции")]
            public string ElementType = "position";
            [JsonProperty(PropertyName = "Объект_продаж")]
            public string PaymentObject = "service";
            [JsonProperty(PropertyName = "Название_оператора")]
            public string OperatorName = "Касса 1";
            [JsonProperty(PropertyName = "Тип_НДС")]
            public string NDSCount = "none";
            [JsonProperty(PropertyName = "Тип_оплаты")]
            public string PayType = "0";
        }
    }
}
