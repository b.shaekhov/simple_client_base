﻿using Newtonsoft.Json;
using System;
using System.IO.Ports;
using System.Linq;

namespace AtolDriver
{
    public class AtolWorker
    {
        IntPtr DriverInstance;

        public bool IsConnected { get { return AtolDriverAPI.libfptr_is_opened(DriverInstance) == 1; } }

        public AtolWorker()
        {
            AtolDriverAPI.libfptr_create(out DriverInstance);
        }

        public void ConnectDevice()
        {
            if (IsConnected)
                return;

            var ports = SerialPort.GetPortNames();

            bool connected = false;

            foreach (var port in ports)
            {
                connected = TryConnect(port, (int)AtolDriverAPI.libfptr_baudrate.LIBFPTR_PORT_BR_115200);

                if (connected)
                    break;

                var vals = Enum.GetValues(typeof(AtolDriverAPI.libfptr_baudrate));

                foreach (int baudrate in vals)
                {
                    connected = TryConnect(port, baudrate);

                    if (connected)
                        break;
                }

                if (connected)
                    break;
            }
        }

        private bool TryConnect(string port, int baudRate)
        {
            var jsonarg = string.Format("{{\"{0}\": {1}, \"{2}\": {3}, \"{4}\": \"{5}\", \"{6}\": {7}}}",
                AtolDriverAPI.LIBFPTR_SETTING_MODEL, (int)AtolDriverAPI.libfptr_model.LIBFPTR_MODEL_ATOL_AUTO,
                AtolDriverAPI.LIBFPTR_SETTING_PORT, (int)AtolDriverAPI.libfptr_port.LIBFPTR_PORT_COM,
                AtolDriverAPI.LIBFPTR_SETTING_COM_FILE, port,
                AtolDriverAPI.LIBFPTR_SETTING_BAUDRATE, baudRate);

            AtolDriverAPI.libfptr_set_settings(DriverInstance, jsonarg);

            AtolDriverAPI.libfptr_open(DriverInstance);
            return IsConnected;
        }

        public void OpenShift()
        {
            SendJson("{\"type\": \"openShift\"}");
        }

        public void CloseShift()
        {
            SendJson("{\"type\": \"closeShift\"}");            
        }
        
        public void SendJson(string json)
        {
            AtolDriverAPI.libfptr_set_param_str(DriverInstance, (int)AtolDriverAPI.libfptr_param.LIBFPTR_PARAM_JSON_DATA, json);
            AtolDriverAPI.libfptr_process_json(DriverInstance);
        }

        public void SellItems(SellItem[] items)
        {
            var check = new FiscalCheck();
            check.items = (from x in items select new FiscalCheck.Element()
            {
                type = SettingsLoader.ReadedSettings.ElementType,
                paymentObject = SettingsLoader.ReadedSettings.PaymentObject,
                name = x.name, price = x.price,
                quantity = x.count,
                amount = x.price * x.count,
                tax = new FiscalCheck.Tax() { type = SettingsLoader.ReadedSettings.NDSCount }
            }).ToArray();
            check.payments = new FiscalCheck.Pay[] { new FiscalCheck.Pay()
            {
                sum = items.Sum(delegate(SellItem cur) { return cur.count * cur.price; }),
                type = SettingsLoader.ReadedSettings.PayType
            } };
            check.Operator = new FiscalCheck.OperatorInfo() { name = SettingsLoader.ReadedSettings.OperatorName };

            check.type = SettingsLoader.ReadedSettings.TypeName;
            check.taxationType = SettingsLoader.ReadedSettings.TaxType;
            check.electronically = SettingsLoader.ReadedSettings.ElectronicallyPay;
            check.paymentsPlace = SettingsLoader.ReadedSettings.SellPlace;
            
            SendJson(JsonConvert.SerializeObject(check));
        }

        public void DisconnectDevice()
        {
            AtolDriverAPI.libfptr_close(DriverInstance);
        }

        ~AtolWorker()
        {
            DisconnectDevice();
            AtolDriverAPI.libfptr_destroy(DriverInstance);
        }

        class FiscalCheck
        {
            public string type; //  = "sell"
            public bool electronically; //  = false
            public string taxationType; //  = "envd"
            public string paymentsPlace; //  = "Альметьевск. Пушкина, 46А."
            public Element[] items;
            public Pay[] payments;
            [JsonProperty(PropertyName = "operator")]
            public OperatorInfo Operator;

            public class Element
            {
                public string type; //  = "position"
                /// <summary>
                /// Имя товара
                /// </summary>
                public string name;
                public float price;
                /// <summary>
                /// Количество
                /// </summary>
                public float quantity;
                public float amount;
                public string paymentObject; //  = "service"
                public Tax tax = new Tax();
            }

            public class OperatorInfo
            {
                public string name; //  = "Касса 1"
            }

            public class Tax
            {
                public string type; //  = "none"
            }

            public class Pay
            {
                public string type; // = "0"
                public float sum;
            }
        }
    }
}
