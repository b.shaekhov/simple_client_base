﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace AtolDriver
{
    public static class AtolDriverAPI
    {
        #region enums
        public enum libfptr_param
        {
            LIBFPTR_PARAM_FIRST = 65536,
            LIBFPTR_PARAM_TEXT = LIBFPTR_PARAM_FIRST,
            LIBFPTR_PARAM_TEXT_WRAP,
            LIBFPTR_PARAM_ALIGNMENT,

            LIBFPTR_PARAM_FONT,
            LIBFPTR_PARAM_FONT_DOUBLE_WIDTH,
            LIBFPTR_PARAM_FONT_DOUBLE_HEIGHT,
            LIBFPTR_PARAM_LINESPACING,
            LIBFPTR_PARAM_BRIGHTNESS,

            LIBFPTR_PARAM_MODEL,
            LIBFPTR_PARAM_RECEIPT_TYPE,
            LIBFPTR_PARAM_REPORT_TYPE,
            LIBFPTR_PARAM_MODE,
            LIBFPTR_PARAM_EXTERNAL_DEVICE_TYPE,
            LIBFPTR_PARAM_EXTERNAL_DEVICE_DATA,
            LIBFPTR_PARAM_FREQUENCY,
            LIBFPTR_PARAM_DURATION,
            LIBFPTR_PARAM_CUT_TYPE,
            LIBFPTR_PARAM_DRAWER_ON_TIMEOUT,
            LIBFPTR_PARAM_DRAWER_OFF_TIMEOUT,
            LIBFPTR_PARAM_DRAWER_ON_QUANTITY,
            LIBFPTR_PARAM_TIMEOUT_ENQ,
            LIBFPTR_PARAM_COMMAND_BUFFER,
            LIBFPTR_PARAM_ANSWER_BUFFER,
            LIBFPTR_PARAM_SERIAL_NUMBER,
            LIBFPTR_PARAM_MANUFACTURER_CODE,
            LIBFPTR_PARAM_NO_NEED_ANSWER,
            LIBFPTR_PARAM_INFO_DISCOUNT_SUM,
            LIBFPTR_PARAM_USE_ONLY_TAX_TYPE,
            LIBFPTR_PARAM_PAYMENT_TYPE,
            LIBFPTR_PARAM_PAYMENT_SUM,
            LIBFPTR_PARAM_REMAINDER,
            LIBFPTR_PARAM_CHANGE,
            LIBFPTR_PARAM_DEPARTMENT,
            LIBFPTR_PARAM_TAX_TYPE,
            LIBFPTR_PARAM_TAX_SUM,
            LIBFPTR_PARAM_TAX_MODE,
            LIBFPTR_PARAM_RECEIPT_ELECTRONICALLY,
            LIBFPTR_PARAM_USER_PASSWORD,
            LIBFPTR_PARAM_SCALE,
            LIBFPTR_PARAM_LEFT_MARGIN,
            LIBFPTR_PARAM_BARCODE,
            LIBFPTR_PARAM_BARCODE_TYPE,
            LIBFPTR_PARAM_BARCODE_PRINT_TEXT,
            LIBFPTR_PARAM_BARCODE_VERSION,
            LIBFPTR_PARAM_BARCODE_CORRECTION,
            LIBFPTR_PARAM_BARCODE_COLUMNS,
            LIBFPTR_PARAM_BARCODE_INVERT,
            LIBFPTR_PARAM_HEIGHT,
            LIBFPTR_PARAM_WIDTH,
            LIBFPTR_PARAM_FILENAME,
            LIBFPTR_PARAM_PICTURE_NUMBER,
            LIBFPTR_PARAM_DATA_TYPE,
            LIBFPTR_PARAM_OPERATOR_ID,
            LIBFPTR_PARAM_LOGICAL_NUMBER,
            LIBFPTR_PARAM_DATE_TIME,
            LIBFPTR_PARAM_FISCAL,
            LIBFPTR_PARAM_SHIFT_STATE,
            LIBFPTR_PARAM_CASHDRAWER_OPENED,
            LIBFPTR_PARAM_RECEIPT_PAPER_PRESENT,
            LIBFPTR_PARAM_COVER_OPENED,
            LIBFPTR_PARAM_SUBMODE,
            LIBFPTR_PARAM_RECEIPT_NUMBER,
            LIBFPTR_PARAM_DOCUMENT_NUMBER,
            LIBFPTR_PARAM_SHIFT_NUMBER,
            LIBFPTR_PARAM_RECEIPT_SUM,
            LIBFPTR_PARAM_RECEIPT_LINE_LENGTH,
            LIBFPTR_PARAM_RECEIPT_LINE_LENGTH_PIX,
            LIBFPTR_PARAM_MODEL_NAME,
            LIBFPTR_PARAM_UNIT_VERSION,
            LIBFPTR_PARAM_PRINTER_CONNECTION_LOST,
            LIBFPTR_PARAM_PRINTER_ERROR,
            LIBFPTR_PARAM_CUT_ERROR,
            LIBFPTR_PARAM_PRINTER_OVERHEAT,
            LIBFPTR_PARAM_UNIT_TYPE,
            LIBFPTR_PARAM_LICENSE_NUMBER,
            LIBFPTR_PARAM_LICENSE_ENTERED,
            LIBFPTR_PARAM_LICENSE,
            LIBFPTR_PARAM_SUM,
            LIBFPTR_PARAM_COUNT,
            LIBFPTR_PARAM_COUNTER_TYPE,
            LIBFPTR_PARAM_STEP_COUNTER_TYPE,
            LIBFPTR_PARAM_ERROR_TAG_NUMBER,
            LIBFPTR_PARAM_TABLE,
            LIBFPTR_PARAM_ROW,
            LIBFPTR_PARAM_FIELD,
            LIBFPTR_PARAM_FIELD_VALUE,
            LIBFPTR_PARAM_FN_DATA_TYPE,
            LIBFPTR_PARAM_TAG_NUMBER,
            LIBFPTR_PARAM_TAG_VALUE,
            LIBFPTR_PARAM_DOCUMENTS_COUNT,
            LIBFPTR_PARAM_FISCAL_SIGN,
            LIBFPTR_PARAM_DEVICE_FFD_VERSION,
            LIBFPTR_PARAM_FN_FFD_VERSION,
            LIBFPTR_PARAM_FFD_VERSION,
            LIBFPTR_PARAM_CHECK_SUM,
            LIBFPTR_PARAM_COMMODITY_NAME,
            LIBFPTR_PARAM_PRICE,
            LIBFPTR_PARAM_QUANTITY,
            LIBFPTR_PARAM_POSITION_SUM,
            LIBFPTR_PARAM_FN_TYPE,
            LIBFPTR_PARAM_FN_VERSION,
            LIBFPTR_PARAM_REGISTRATIONS_REMAIN,
            LIBFPTR_PARAM_REGISTRATIONS_COUNT,
            LIBFPTR_PARAM_NO_ERROR_IF_NOT_SUPPORTED,
            LIBFPTR_PARAM_OFD_EXCHANGE_STATUS,
            LIBFPTR_PARAM_FN_ERROR_DATA,
            LIBFPTR_PARAM_FN_ERROR_CODE,
            LIBFPTR_PARAM_ENVD_MODE,
            LIBFPTR_PARAM_DOCUMENT_CLOSED,
            LIBFPTR_PARAM_JSON_DATA,
            LIBFPTR_PARAM_COMMAND_SUBSYSTEM,
            LIBFPTR_PARAM_FN_OPERATION_TYPE,
            LIBFPTR_PARAM_FN_STATE,
            LIBFPTR_PARAM_ENVD_MODE_ENABLED,
            LIBFPTR_PARAM_SETTING_ID,
            LIBFPTR_PARAM_SETTING_VALUE,
            LIBFPTR_PARAM_MAPPING_KEY,
            LIBFPTR_PARAM_MAPPING_VALUE,
            LIBFPTR_PARAM_COMMODITY_PIECE,
            LIBFPTR_PARAM_POWER_SOURCE_TYPE,
            LIBFPTR_PARAM_BATTERY_CHARGE,
            LIBFPTR_PARAM_VOLTAGE,
            LIBFPTR_PARAM_USE_BATTERY,
            LIBFPTR_PARAM_BATTERY_CHARGING,
            LIBFPTR_PARAM_CAN_PRINT_WHILE_ON_BATTERY,
            LIBFPTR_PARAM_MAC_ADDRESS,
            LIBFPTR_PARAM_FN_FISCAL,
            LIBFPTR_PARAM_NETWORK_ERROR,
            LIBFPTR_PARAM_OFD_ERROR,
            LIBFPTR_PARAM_FN_ERROR,
            LIBFPTR_PARAM_COMMAND_CODE,
            LIBFPTR_PARAM_PRINTER_TEMPERATURE,
            LIBFPTR_PARAM_RECORDS_TYPE,
            LIBFPTR_PARAM_OFD_FISCAL_SIGN,
            LIBFPTR_PARAM_HAS_OFD_TICKET,
            LIBFPTR_PARAM_NO_SERIAL_NUMBER,
            LIBFPTR_PARAM_RTC_FAULT,
            LIBFPTR_PARAM_SETTINGS_FAULT,
            LIBFPTR_PARAM_COUNTERS_FAULT,
            LIBFPTR_PARAM_USER_MEMORY_FAULT,
            LIBFPTR_PARAM_SERVICE_COUNTERS_FAULT,
            LIBFPTR_PARAM_ATTRIBUTES_FAULT,
            LIBFPTR_PARAM_FN_FAULT,
            LIBFPTR_PARAM_INVALID_FN,
            LIBFPTR_PARAM_HARD_FAULT,
            LIBFPTR_PARAM_MEMORY_MANAGER_FAULT,
            LIBFPTR_PARAM_SCRIPTS_FAULT,
            LIBFPTR_PARAM_FULL_RESET,
            LIBFPTR_PARAM_WAIT_FOR_REBOOT,
            LIBFPTR_PARAM_SCALE_PERCENT,
            LIBFPTR_PARAM_FN_NEED_REPLACEMENT,
            LIBFPTR_PARAM_FN_RESOURCE_EXHAUSTED,
            LIBFPTR_PARAM_FN_MEMORY_OVERFLOW,
            LIBFPTR_PARAM_FN_OFD_TIMEOUT,
            LIBFPTR_PARAM_FN_CRITICAL_ERROR,
            LIBFPTR_PARAM_OFD_MESSAGE_READ,
            LIBFPTR_PARAM_DEVICE_MIN_FFD_VERSION,
            LIBFPTR_PARAM_DEVICE_MAX_FFD_VERSION,
            LIBFPTR_PARAM_DEVICE_UPTIME,
            LIBFPTR_PARAM_NOMENCLATURE_TYPE,
            LIBFPTR_PARAM_GTIN,
            LIBFPTR_PARAM_FN_DOCUMENT_TYPE,
            LIBFPTR_PARAM_NETWORK_ERROR_TEXT,
            LIBFPTR_PARAM_FN_ERROR_TEXT,
            LIBFPTR_PARAM_OFD_ERROR_TEXT,
            LIBFPTR_PARAM_USER_SCRIPT_ID,
            LIBFPTR_PARAM_USER_SCRIPT_PARAMETER,
            LIBFPTR_PARAM_USER_MEMORY_OPERATION,
            LIBFPTR_PARAM_USER_MEMORY_DATA,
            LIBFPTR_PARAM_USER_MEMORY_STRING,
            LIBFPTR_PARAM_USER_MEMORY_ADDRESS,
            LIBFPTR_PARAM_FN_PRESENT,
            LIBFPTR_PARAM_BLOCKED,
            LIBFPTR_PARAM_DOCUMENT_PRINTED,
            LIBFPTR_PARAM_DISCOUNT_SUM,
            LIBFPTR_PARAM_SURCHARGE_SUM,
            LIBFPTR_PARAM_LK_USER_CODE,
            LIBFPTR_PARAM_LICENSE_COUNT,

            LIBFPTR_PARAM_LAST
        };

        public enum libfptr_model
        {
            LIBFPTR_MODEL_UNKNOWN = 0,
            LIBFPTR_MODEL_ATOL_AUTO = 500,
            LIBFPTR_MODEL_ATOL_11F = 67,
            LIBFPTR_MODEL_ATOL_15F = 78,
            LIBFPTR_MODEL_ATOL_20F = 81,
            LIBFPTR_MODEL_ATOL_22F = 63,
            LIBFPTR_MODEL_ATOL_25F = 57,
            LIBFPTR_MODEL_ATOL_30F = 61,
            LIBFPTR_MODEL_ATOL_42FS = 77,
            LIBFPTR_MODEL_ATOL_50F = 80,
            LIBFPTR_MODEL_ATOL_52F = 64,
            LIBFPTR_MODEL_ATOL_55F = 62,
            LIBFPTR_MODEL_ATOL_60F = 75,
            LIBFPTR_MODEL_ATOL_77F = 69,
            LIBFPTR_MODEL_ATOL_90F = 72,
            LIBFPTR_MODEL_ATOL_91F = 82,
            LIBFPTR_MODEL_ATOL_92F = 84,
            LIBFPTR_MODEL_ATOL_SIGMA_10 = 86
        };

        public enum libfptr_port
        {
            LIBFPTR_PORT_COM = 0,
            LIBFPTR_PORT_USB,
            LIBFPTR_PORT_TCPIP,
            LIBFPTR_PORT_BLUETOOTH,
        };

        public enum libfptr_baudrate
        {
            LIBFPTR_PORT_BR_1200 = 1200,
            LIBFPTR_PORT_BR_2400 = 2400,
            LIBFPTR_PORT_BR_4800 = 4800,
            LIBFPTR_PORT_BR_9600 = 9600,
            LIBFPTR_PORT_BR_19200 = 19200,
            LIBFPTR_PORT_BR_38400 = 38400,
            LIBFPTR_PORT_BR_57600 = 57600,
            LIBFPTR_PORT_BR_115200 = 115200,
            LIBFPTR_PORT_BR_230400 = 230400,
            LIBFPTR_PORT_BR_460800 = 460800,
            LIBFPTR_PORT_BR_921600 = 921600,
        };

        public const string LIBFPTR_SETTING_LIBRARY_PATH = "LibraryPath";
        public const string LIBFPTR_SETTING_MODEL = "Model";
        public const string LIBFPTR_SETTING_PORT = "Port";
        public const string LIBFPTR_SETTING_BAUDRATE = "BaudRate";
        public const string LIBFPTR_SETTING_BITS = "Bits";
        public const string LIBFPTR_SETTING_PARITY = "Parity";
        public const string LIBFPTR_SETTING_STOPBITS = "StopBits";
        public const string LIBFPTR_SETTING_IPADDRESS = "IPAddress";
        public const string LIBFPTR_SETTING_IPPORT = "IPPort";
        public const string LIBFPTR_SETTING_MACADDRESS = "MACAddress";
        public const string LIBFPTR_SETTING_COM_FILE = "ComFile";
        public const string LIBFPTR_SETTING_USB_DEVICE_PATH = "UsbDevicePath";
        public const string LIBFPTR_SETTING_BT_AUTOENABLE = "AutoEnableBluetooth";
        public const string LIBFPTR_SETTING_BT_AUTODISABLE = "AutoDisableBluetooth";
        public const string LIBFPTR_SETTING_ACCESS_PASSWORD = "AccessPassword";
        public const string LIBFPTR_SETTING_USER_PASSWORD = "UserPassword";
        public const string LIBFPTR_SETTING_OFD_CHANNEL = "OfdChannel";
        public const string LIBFPTR_SETTING_EXISTED_COM_FILES = "ExistedComFiles";
        #endregion

        #region DriverAPI
        [DllImport("fptr10.dll")]
        public extern static int libfptr_create(out IntPtr handle);
        [DllImport("fptr10.dll")]
        public extern static int libfptr_destroy(IntPtr handle);

        [DllImport("fptr10.dll")]
        public extern static int libfptr_set_settings(IntPtr handle, [MarshalAs(UnmanagedType.LPWStr)]string jsonarg);

        [DllImport("fptr10.dll")]
        public extern static int libfptr_open(IntPtr handle);
        [DllImport("fptr10.dll")]
        public extern static int libfptr_close(IntPtr handle);

        [DllImport("fptr10.dll")]
        public extern static int libfptr_set_param_str(IntPtr handle, int param_id, [MarshalAs(UnmanagedType.LPWStr)]string jsonarg);

        [DllImport("fptr10.dll")]
        public extern static int libfptr_process_json(IntPtr handle);

        [DllImport("fptr10.dll")]
        public extern static int libfptr_is_opened(IntPtr handle);

        [DllImport("fptr10.dll", CallingConvention = CallingConvention.Cdecl)]
        public extern static int libfptr_get_param_str(IntPtr handle, int param_id, StringBuilder jsonarg, int size);
        #endregion

    }
}
