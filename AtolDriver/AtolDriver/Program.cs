﻿using System;
using System.Threading;
using Newtonsoft.Json;
using System.IO;

namespace AtolDriver
{
    class Program
    {
        const string SourceFilePath = "Temp/AtolDriverSource.txt";
        const int awaitTime = 1000;

        static void Main(string[] args)
        {
            var source = GetSource();
            if (source == null)
            {
                Console.WriteLine("Неверные входные данные!");
                Thread.Sleep(awaitTime);
                return;
            }

            var worker = new AtolWorker();
            worker.ConnectDevice();

            var baseSet = SettingsLoader.ReadedSettings;
            
            if (worker.IsConnected)
            {
                Console.WriteLine("Печатаю...");
                //worker.CloseShift();
                worker.SellItems(JsonConvert.DeserializeObject<SellItem[]>(source));
                Console.WriteLine("Успех!");
            } else
            {
                Console.WriteLine("Ошибка связи с АТОЛ!");
            }

            Thread.Sleep(awaitTime);
        }

        static string GetSource()
        {
            var dir = Path.GetDirectoryName(SourceFilePath);

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            if (File.Exists(SourceFilePath))
                return File.ReadAllText(SourceFilePath);

            return null;
        }
    }
}
