﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutomaticalChemistry.FormBuilder
{
    public interface IControlsBlock
    {
        string Name { get; set; }
        Control[] GetTabControls();
        void Reset();
    }
}
