﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.FormBuilder.TabBuilders.SendSMSWork
{
    public class SMSSendData
    {
        public ClientData[] Data;

        public class ClientData
        {
            public string[] PhoneNums;
            public string Message;
        }
    }
}
