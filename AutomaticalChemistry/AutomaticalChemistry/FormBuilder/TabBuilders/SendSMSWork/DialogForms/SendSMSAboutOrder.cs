﻿using AutomaticalChemistry.DatabaseWork.HAPI;
using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo;
using AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientFinder;
using AutomaticalChemistry.FormBuilder.Tools;
using AutomaticalChemistry.FormBuilder.Tools.ControlAddons;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static AutomaticalChemistry.MainLogic.MainLogic;

namespace AutomaticalChemistry.FormBuilder.TabBuilders.SendSMSWork.DialogForms
{
    public class SendSMSAboutOrder : IControlsBlock
    {
        const string PatternFileName = @"Temp\PatternSMSAboutOrder.txt";
        const string SMSSendFileName = @"Temp\SMSSendData.txt";

        public string Name { get { return _name; } set { _name = value; } }

        string _name;

        private TableLayoutPanel _headControlTable;
        private TableLayoutPanel _footControlTable;

        private SMSBuilder _messageBuilder;

        public event EmptyD SendedEvent;
        private DBOrder _workOrder;

        int _clientItemsCount;
        int _orderMainInfoItemsCount;
        int _orderAddonItemsCount;

        public SendSMSAboutOrder(string name, DBOrder workOrder)
        {
            _name = name;
            _workOrder = workOrder;
        }

        public Control[] GetTabControls()
        {
            if (_messageBuilder == null)
            {
                var clientComboBoxItems = DBClientName.ClientVarsNames.Union(new string[] { "Номера телефонов" }).ToArray();
                var mainInfoComboBoxItems = DBMainOrderInfo.OrderMainInfoVarsNames.Except(new string[] { DBMainOrderInfo.OrderMainInfoVarsNames[DBMainOrderInfo.OrderMainInfoVarsNames.Length - 1] }).ToArray();
                var addonInfoComboBoxItems = DBAdditionalOrderInfo.OrderAdditionalInfoVarsNames.Union(new string[] { "Пятна" }).ToArray();

                var fullOrderInfoComboBoxItems = clientComboBoxItems.Union(mainInfoComboBoxItems.Union(addonInfoComboBoxItems)).ToArray();

                _clientItemsCount = clientComboBoxItems.Length;
                _orderMainInfoItemsCount = mainInfoComboBoxItems.Length;
                _orderAddonItemsCount = addonInfoComboBoxItems.Length;

                _messageBuilder = new SMSBuilder("builder", fullOrderInfoComboBoxItems);

                //////////////////////////////////////////////

                _headControlTable = new TableLayoutPanel();

                _headControlTable.SuspendLayout();

                _headControlTable.AutoSize = true;
                _headControlTable.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                _headControlTable.Dock = DockStyle.Top;

                _headControlTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;

                for (int i = 0; i < 8; i++)
                    TableLayoutPanelTools.InsertColumn(_headControlTable.ColumnCount, _headControlTable);

                _headControlTable.ColumnStyles[_headControlTable.ColumnCount - 2].Width = 50;
                _headControlTable.ColumnStyles[_headControlTable.ColumnCount - 2].SizeType = SizeType.Percent;

                TableLayoutPanelTools.InsertRow(_headControlTable.RowCount, _headControlTable);
                TableLayoutPanelTools.InsertRow(_headControlTable.RowCount, _headControlTable);

                int columnNum = 0;

                _headControlTable.Controls.Add(LayoutTools.GetLabel("Номер\r\nзаказа", DockStyle.Top, HorizontalAlignment.Center), columnNum, 0); columnNum++;
                _headControlTable.Controls.Add(LayoutTools.GetLabel("Дней\r\nосталось", DockStyle.Top, HorizontalAlignment.Center), columnNum, 0); columnNum++;
                _headControlTable.Controls.Add(LayoutTools.GetLabel("Дата\r\nначала/конца", DockStyle.Top, HorizontalAlignment.Center), columnNum, 0); columnNum++;
                //_headControlTable.Controls.Add(LayoutTools.GetLabel("Дата\r\nконца", DockStyle.Top, HorizontalAlignment.Center), columnNum, 0); columnNum++;
                _headControlTable.Controls.Add(LayoutTools.GetLabel("Информация\r\nо клиенте", DockStyle.Top, HorizontalAlignment.Center), columnNum, 0); columnNum++;
                _headControlTable.Controls.Add(LayoutTools.GetLabel("Изделие", DockStyle.Top, HorizontalAlignment.Center), columnNum, 0); columnNum++;
                _headControlTable.Controls.Add(LayoutTools.GetLabel("Пятна", DockStyle.Top, HorizontalAlignment.Center), columnNum, 0); columnNum++;
                _headControlTable.Controls.Add(LayoutTools.GetLabel("Доп", DockStyle.Top, HorizontalAlignment.Center), columnNum, 0); columnNum++;

                columnNum = 0;

                _headControlTable.Controls.Add(LayoutTools.GetLabel(_workOrder.MainInfo.Id.ToString(), DockStyle.Top, HorizontalAlignment.Left), columnNum, _headControlTable.RowCount - 1); columnNum++;
                _headControlTable.Controls.Add(LayoutTools.GetLabel(_workOrder.MainInfo.Executed ? "-" : (_workOrder.MainInfo.EndDate - DateTime.Now).Days.ToString(), DockStyle.Top, HorizontalAlignment.Left), columnNum, _headControlTable.RowCount - 1); columnNum++;
                _headControlTable.Controls.Add(LayoutTools.GetLabel(_workOrder.MainInfo.StartDate.ToString("dd.MM.yyyy") + "\r\n" + _workOrder.MainInfo.EndDate.ToString("dd.MM.yyyy"), DockStyle.Top, HorizontalAlignment.Left), columnNum, _headControlTable.RowCount - 1); columnNum++;
                //_headControlTable.Controls.Add(LayoutTools.GetLabel(, DockStyle.Top, ContentAlignment.TopLeft), columnNum, _ordersTable.RowCount - 1); columnNum++;
                _headControlTable.Controls.Add(LayoutTools.GetLabel(_workOrder.MainInfo.ClientInfo.ToString(), DockStyle.Top, HorizontalAlignment.Left), columnNum, _headControlTable.RowCount - 1); columnNum++;
                _headControlTable.Controls.Add(LayoutTools.GetLabel(_workOrder.MainInfo.ObjectName, DockStyle.Top, HorizontalAlignment.Left), columnNum, _headControlTable.RowCount - 1); columnNum++;

                StringBuilder spotsText = new StringBuilder();

                foreach (var spot in _workOrder.Spots)
                {
                    spotsText.Append(string.Format("{0}\r\n"/*+"{1} {2} {3} {4}\r\n"*/, spot.Description/*, spot.PriceWholePart, Price.WholePartName,spot.PriceFractionalPart, Price.FractionalPartName*/));
                }

                _headControlTable.Controls.Add(LayoutTools.GetLabel(spotsText.ToString(), DockStyle.Top, HorizontalAlignment.Left), columnNum, _headControlTable.RowCount - 1); columnNum++;

                var label = LayoutTools.GetLabel(BuildTextFromAdditionalInfo(_workOrder.AdditionalInfo), DockStyle.Top, HorizontalAlignment.Left);

                label.Font = new Font("Consolas", label.Font.Size);

                _headControlTable.Controls.Add(label, columnNum, _headControlTable.RowCount - 1); columnNum++;

                _headControlTable.ResumeLayout();

                //////////////////////////////////////////////

                _footControlTable = new TableLayoutPanel();

                _footControlTable.SuspendLayout();

                _footControlTable.AutoSize = true;
                _footControlTable.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                _footControlTable.Dock = DockStyle.Top;

                TableLayoutPanelTools.InsertColumn(_footControlTable.ColumnCount, _footControlTable);
                TableLayoutPanelTools.InsertColumn(_footControlTable.ColumnCount, _footControlTable);
                TableLayoutPanelTools.InsertRow(_footControlTable.RowCount, _footControlTable);

                var loadPatternBut = LayoutTools.GetButton("Загрузить из шаблона");
                loadPatternBut.Click += LoadPatternClick;
                TableLayoutPanelTools.InsertRow(_footControlTable.RowCount, _footControlTable);
                _footControlTable.Controls.Add(loadPatternBut, 0, _footControlTable.RowCount - 1);

                var createPatternBut = LayoutTools.GetButton("Сохранить шаблон");
                createPatternBut.Click += CreatePatternClick;
                //TableLayoutPanelTools.InsertRow(_footControlTable.RowCount, _footControlTable);
                _footControlTable.Controls.Add(createPatternBut, 1, _footControlTable.RowCount - 1);

                var sendBut = LayoutTools.GetButton("Отправить!");
                sendBut.Click += SendClick;
                TableLayoutPanelTools.InsertRow(_footControlTable.RowCount, _footControlTable);
                _footControlTable.Controls.Add(sendBut, 0, _footControlTable.RowCount - 1);

                _footControlTable.ResumeLayout();
            }

            var messageBuilderControls = _messageBuilder.GetTabControls();

            var ans = new Control[messageBuilderControls.Length + 2];

            int indexer = 0;

            ans[indexer] = _footControlTable;
            indexer++;

            foreach (var mbcControl in messageBuilderControls)
            {
                ans[indexer] = mbcControl;
                indexer++;
            }

            ans[indexer] = _headControlTable;
            indexer++;

            return ans;
        }

        void LoadPatternClick(object sender, EventArgs e)
        {
            try
            {
                _messageBuilder.ConfigureFromSerialisedPattern(File.ReadAllText(PatternFileName));
            } catch (Exception exc)
            {
                ShowInfoForUser.Error("Не удалось загрузить шаблон!\r\n" + exc.Message);
            }
        }

        void CreatePatternClick(object sender, EventArgs e)
        {
            File.WriteAllText(PatternFileName, _messageBuilder.GetSerialisedPattern());
            ShowInfoForUser.Message("Шаблон сохранен");
        }

        string BuildTextFromAdditionalInfo(DBAdditionalOrderInfo addionalInfo)
        {
            var names = DBAdditionalOrderInfo.OrderAdditionalInfoVarsNames;
            StringBuilder builder = new StringBuilder();

            int maxLen = 0;

            foreach (var name in names)
            {
                if (name.Length > maxLen)
                    maxLen = name.Length;
            }

            int counter = 0;
            
            foreach (var name in names)
            {
                builder.Append(name.PadRight(maxLen) + " | " + addionalInfo.GetValueFromNameIndex(counter) + "\r\n");
                counter++;
            }
            return builder.ToString();
        }

        void SendClick(object sender, EventArgs e)
        {
            var error = _messageBuilder.CheckInfoBoxes();

            if (error != null)
            {
                ShowInfoForUser.Error(error);
                return;
            }

            if (!ShowInfoForUser.Question("Вы действительно хотите послать СМС\r\nсогласно данным настройкам?"))
                return;

            if (!_workOrder.MainInfo.ClientInfo.Name.PhoneNotification)
            {
                if (!ShowInfoForUser.Question("Вы действительно хотите послать СМС\r\nклиенту, который отказался от уведомлений?"))
                    return;
            }

            //var sendClients = ClientFinder.GetFilteredClients(_filterTextBox.Text, DBController.GetAllClientInfo(false));

            var sendData = new SMSSendData()
            {
                Data = new SMSSendData.ClientData[] { new SMSSendData.ClientData()
                {
                    PhoneNums = (from x in _workOrder.MainInfo.ClientInfo.PhoneNumbers select x.Value).ToArray(),
                    Message = BuildMessage()
                }
                }
            };
            
            File.WriteAllText(SMSSendFileName, JsonConvert.SerializeObject(sendData, Formatting.Indented));

            try
            {
                var senderWork = Process.Start(Application.StartupPath + "\\SMSSender.exe");

                _messageBuilder.Enabled = false;
                _headControlTable.Enabled = false;
                _footControlTable.Enabled = false;

                senderWork.Exited += EndSend;
            }
            catch (Exception ex)
            {
                ShowInfoForUser.Error("Не удалось запустить программу-рассылатель СМС!\r\n" + ex.Message);
            }
        }

        void EndSend(object sender, EventArgs e)
        {
            ((Process)sender).Exited -= EndSend;

            _messageBuilder.Enabled = true;
            _headControlTable.Enabled = true;
            _footControlTable.Enabled = true;

            SendedEvent?.Invoke();
        }

        string BuildMessage()
        {
            var ansBuilder = new StringBuilder();

            var infoBoxes = _messageBuilder.InfoBoxes;

            foreach (var field in infoBoxes)
            {
                if (field is TextBox)
                {
                    ansBuilder.Append(field.Text);
                }
                if (field is ComboBox)
                {
                    var selectIndex = ((ComboBox)field).SelectedIndex;

                    if (selectIndex < _clientItemsCount)
                    {

                        if (selectIndex == _clientItemsCount - 1)
                        {
                            ansBuilder.Append(_workOrder.MainInfo.ClientInfo.BuildStringFromPhoneNums());
                        }
                        else
                        {
                            ansBuilder.Append(_workOrder.MainInfo.ClientInfo.Name.GetValueFromNameIndex(selectIndex));
                        }
                    }
                    else
                    {
                        selectIndex -= _clientItemsCount;
                        if (selectIndex < _orderMainInfoItemsCount)
                        {
                            ansBuilder.Append(_workOrder.MainInfo.GetValueFromNameIndex(selectIndex));
                        }
                        else
                        {
                            selectIndex -= _orderMainInfoItemsCount;

                            if (selectIndex == _orderAddonItemsCount - 1)
                            {
                                foreach (var spot in _workOrder.Spots)
                                {
                                    ansBuilder.Append(spot.Description);
                                }
                            }
                            else
                                ansBuilder.Append(_workOrder.AdditionalInfo.GetValueFromNameIndex(selectIndex));
                        }
                    }
                }
            }
            
            return ansBuilder.ToString();
        }
        
        public void Reset()
        {
            _messageBuilder.Reset();
        }
    }
}
