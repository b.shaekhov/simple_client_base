﻿using AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo;
using AutomaticalChemistry.Properties;
using System.Windows.Forms;

namespace AutomaticalChemistry.FormBuilder.TabBuilders.SendSMSWork.DialogForms
{
    class SendSMSAboutOrderForm : Form
    {
        static SendSMSAboutOrderForm _instance;

        public SendSMSAboutOrderForm()
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        }

        public static void SendOrderToClient(DBOrder order)
        {
            _instance = new SendSMSAboutOrderForm();

            _instance.Icon = Resources.Program;

            _instance.AutoScroll = true;
            _instance.FormBorderStyle = FormBorderStyle.SizableToolWindow;

            var sender = new SendSMSAboutOrder("orderSender",order);

            sender.SendedEvent += SendEnd;

            _instance.Controls.AddRange(sender.GetTabControls());

            _instance.Size = new System.Drawing.Size(900, 350);

            _instance.ShowDialog();
            _instance = null;
        }

        static void SendEnd()
        {
            _instance.Close();
        }
    }
}
