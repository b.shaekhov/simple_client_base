﻿using AutomaticalChemistry.DatabaseWork.HAPI;
using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientFinder;
using AutomaticalChemistry.FormBuilder.Tools;
using AutomaticalChemistry.FormBuilder.Tools.ControlAddons;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutomaticalChemistry.FormBuilder.TabBuilders.SendSMSWork
{
    public class SMSBuilder : IControlsBlock
    {
        public bool Enabled { set { _workTable.Enabled = value; _controlTables.Enabled = value; } }

        public string Name { get { return _name; } set { _name = value; } }

        string _name;

        private TableLayoutPanel _workTable;
        private TableLayoutPanel _controlTables;

        private List<Button> _removeButtons = new List<Button>();
        private List<Control> _infoBoxes = new List<Control>();

        public Control[] InfoBoxes { get { return _infoBoxes.ToArray(); } }

        private IntCounterRow _columnsCounter;
        
        string[] _comboBoxItems;

        public SMSBuilder(string name, string[] comboBoxItems)
        {
            _name = name;
            _comboBoxItems = comboBoxItems;
        }

        public Control[] GetTabControls()
        {
            var ans = new Control[2];

            if (_workTable == null)
            {
                //_comboBoxItems = DBClientName.ClientVarsNames.Union(new string[] { "Номера телефонов" }).ToArray();

                _workTable = new TableLayoutPanel();

                _workTable.SuspendLayout();

                _workTable.AutoSize = true;
                _workTable.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                _workTable.Dock = DockStyle.Top;

                //_workTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Inset;

                _workTable.ResumeLayout();


                /////////////////////////////////////////////////////////////


                _controlTables = new TableLayoutPanel();

                _controlTables.SuspendLayout();

                _controlTables.AutoSize = true;
                _controlTables.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                _controlTables.Dock = DockStyle.Top;

                TableLayoutPanelTools.InsertColumn(_controlTables.ColumnCount, _controlTables);
                TableLayoutPanelTools.InsertColumn(_controlTables.ColumnCount, _controlTables);
                TableLayoutPanelTools.InsertRow(_controlTables.RowCount, _controlTables);

                var insParamBut = LayoutTools.GetButton("Добавить поле-параметр");
                insParamBut.Click += AddParamButtonClick;
                _controlTables.Controls.Add(insParamBut, 0, 0);

                var insTextBut = LayoutTools.GetButton("Добавить поле-текст");
                insTextBut.Click += AddTextButtonClick;
                _controlTables.Controls.Add(insTextBut, 1, 0);

                _columnsCounter = new IntCounterRow("Количество столбцов", false, null);
                _columnsCounter.ApplyToTable(_controlTables, _controlTables.RowCount, true);
                _columnsCounter.Value = 3;
                _columnsCounter.ChangeValueEvent += ChangeTableColumn;
                
                _controlTables.ResumeLayout();
            }

            ans[0] = _controlTables;
            ans[1] = _workTable;

            return ans;
        }
        
        public string CheckInfoBoxes()
        {
            if (_infoBoxes.Count < 1)
                return "Не задано ни одного поля!";

            foreach (var field in _infoBoxes)
            {
                if (field is TextBox)
                {
                    if (field.Text == "" || field.Text == null)
                    {
                        return "Текстовое поле не должно быть пустым!";
                    }
                }
                if (field is ComboBox)
                {
                    if (((ComboBox)field).SelectedIndex < 0)
                    {
                        return "Не все поля-параметры заполнены верно!";
                    }
                }
            }

            return null;
        }
        
        void AddParamButtonClick(object sender, EventArgs e)
        {
            var box = LayoutTools.GetComboBox();
            box.Items.AddRange(_comboBoxItems);

            AddControl(box);
        }

        void AddTextButtonClick(object sender, EventArgs e)
        {
            AddControl(LayoutTools.GetTextBox(300));
        }

        void AddControl(Control c)
        {
            _infoBoxes.Add(c);
            _removeButtons.Add(LayoutTools.GetButton("Удалить поле"));
            _removeButtons[_removeButtons.Count - 1].Click += RemoveButtonClick;

            _workTable.SuspendLayout();

            var rowNum = (_infoBoxes.Count - 1) / _columnsCounter.Value;
            var columNum = (_infoBoxes.Count - 1) % _columnsCounter.Value;

            while (_workTable.ColumnCount < columNum + 1)
            {
                TableLayoutPanelTools.InsertColumn(_workTable.ColumnCount, _workTable);
            }

            while (_workTable.RowCount < (rowNum + 1) * 2)
            {
                TableLayoutPanelTools.InsertRow(_workTable.RowCount, _workTable);
            }

            _workTable.Controls.Add(_infoBoxes[_infoBoxes.Count - 1], columNum, rowNum * 2);
            _workTable.Controls.Add(_removeButtons[_removeButtons.Count - 1], columNum, (rowNum * 2) + 1);

            _workTable.ResumeLayout();
        }

        void ChangeTableColumn()
        {
            if (_columnsCounter.Value < 1)
                _columnsCounter.Value = 1;

            if (_workTable.ColumnCount == _columnsCounter.Value)
                return;

            _workTable.SuspendLayout();

            _workTable.Controls.Clear();
            _workTable.ColumnCount = 0;
            _workTable.RowCount = 0;
            _workTable.RowStyles.Clear();
            _workTable.ColumnStyles.Clear();

            var rowNum = (_infoBoxes.Count - 1) / _columnsCounter.Value;

            while (_workTable.ColumnCount < _columnsCounter.Value)
            {
                TableLayoutPanelTools.InsertColumn(_workTable.ColumnCount, _workTable);
            }

            while (_workTable.RowCount < (rowNum + 1) * 2)
            {
                TableLayoutPanelTools.InsertRow(_workTable.RowCount, _workTable);
            }

            int counter = 0;

            for (int j = 0; j < _workTable.RowCount; j += 2)
            {
                for (int i = 0; i < _workTable.ColumnCount; i++)
                {
                    if (counter >= _infoBoxes.Count)
                        break;

                    _workTable.Controls.Add(_infoBoxes[counter], i, j);
                    _workTable.Controls.Add(_removeButtons[counter], i, j + 1);

                    counter++;
                }
            }

            _workTable.ResumeLayout();
        }

        void RemoveButtonClick(object sender, EventArgs e)
        {
            _workTable.SuspendLayout();

            var curBut = (Button)sender;

            var index = _removeButtons.IndexOf(curBut);

            var j = (index / _columnsCounter.Value) * 2;
            var i = index % _columnsCounter.Value;

            while (j < _workTable.RowCount)
            {
                while (i < _workTable.ColumnCount)
                {
                    if (i == _workTable.ColumnCount - 1)
                    {
                        if (j == _workTable.RowCount - 2)
                        {
                            var tel = _workTable.GetControlFromPosition(i, j);
                            var tbut = _workTable.GetControlFromPosition(i, j + 1);

                            _workTable.Controls.Remove(tel);
                            _workTable.Controls.Remove(tbut);
                            break;
                        }

                        var el = _workTable.GetControlFromPosition(i, j);
                        var but = _workTable.GetControlFromPosition(i, j + 1);

                        _workTable.Controls.Remove(el);
                        _workTable.Controls.Remove(but);

                        el = _workTable.GetControlFromPosition(0, j + 2);
                        but = _workTable.GetControlFromPosition(0, j + 3);

                        _workTable.Controls.Remove(el);
                        _workTable.Controls.Remove(but);

                        _workTable.Controls.Add(el, i, j);
                        _workTable.Controls.Add(but, i, j + 1);

                        break;
                    }
                    else
                    {
                        var el = _workTable.GetControlFromPosition(i, j);
                        var but = _workTable.GetControlFromPosition(i, j + 1);

                        _workTable.Controls.Remove(el);
                        _workTable.Controls.Remove(but);

                        el = _workTable.GetControlFromPosition(i + 1, j);
                        but = _workTable.GetControlFromPosition(i + 1, j + 1);

                        if (el != null)
                            _workTable.Controls.Remove(el);
                        if (but != null)
                            _workTable.Controls.Remove(but);

                        if (el != null)
                            _workTable.Controls.Add(el, i, j);
                        if (but != null)
                            _workTable.Controls.Add(but, i, j + 1);
                    }
                    i++;
                }
                i = 0;
                j += 2;
            }

            _infoBoxes.RemoveAt(index);
            _removeButtons.RemoveAt(index);

            while (_infoBoxes.Count > 0 && ((_infoBoxes.Count - 1) / _columnsCounter.Value) * 2 < _workTable.RowCount - 2)
            {
                TableLayoutPanelTools.RemoveRow(_workTable.RowCount - 1, _workTable);
                TableLayoutPanelTools.RemoveRow(_workTable.RowCount - 1, _workTable);
            }

            if (_infoBoxes.Count < 1)
            {
                while (_workTable.RowCount > 0)
                    TableLayoutPanelTools.RemoveRow(_workTable.RowCount - 1, _workTable);
            }

            if (_workTable.RowCount <= 2)
            {
                while (_workTable.ColumnCount > _infoBoxes.Count)
                    TableLayoutPanelTools.RemoveColumn(_workTable.ColumnCount - 1, _workTable);
            }

            _workTable.ResumeLayout();
        }

        public string GetSerialisedPattern()
        {
            var serData = new SerialiseData()
            {
                InfoBoxes = new SerialiseData.InfoBox[InfoBoxes.Length],
                ColumnCount = _columnsCounter.Value
            };

            int counter = 0;
            foreach (var box in InfoBoxes)
            {
                serData.InfoBoxes[counter] = new SerialiseData.InfoBox()
                {
                    TextBox = box is TextBox,
                    Value = box is TextBox ? box.Text : ((ComboBox)box).SelectedIndex.ToString()
                };
                counter++;
            }

            return JsonConvert.SerializeObject(serData);
        }

        public void ConfigureFromSerialisedPattern(string pattern)
        {
            var serData = JsonConvert.DeserializeObject<SerialiseData>(pattern);
            Reset();

            _columnsCounter.Value = serData.ColumnCount;

            foreach (var box in serData.InfoBoxes)
            {
                if (box.TextBox)
                {
                    AddTextButtonClick(null,null);
                    _infoBoxes[_infoBoxes.Count - 1].Text = box.Value;
                } else
                {
                    AddParamButtonClick(null, null);
                    ((ComboBox)_infoBoxes[_infoBoxes.Count - 1]).SelectedIndex = int.Parse(box.Value);
                }
            }
        }

        public void Reset()
        {
            if (_workTable == null)
                return;

            _workTable.SuspendLayout();

            while (_removeButtons.Count > 0)
            {
                RemoveButtonClick(_removeButtons[_removeButtons.Count - 1], null);
            }

            _workTable.ResumeLayout();

            //AddButtonClick(null, null);
        }

        class SerialiseData
        {
            public InfoBox[] InfoBoxes = null;
            public int ColumnCount = 0;

            public class InfoBox
            {
                public bool TextBox = false;
                public string Value = null;
            }
        }
    }
}
