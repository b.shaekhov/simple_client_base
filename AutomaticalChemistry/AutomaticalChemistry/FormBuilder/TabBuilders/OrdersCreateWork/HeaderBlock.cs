﻿using AutomaticalChemistry.FormBuilder.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutomaticalChemistry.FormBuilder.TabBuilders.OrdersCreateWork
{
    public class HeaderBlock : IControlsBlock
    {
        public string Name { get { return _name; } set { _name = value; } }

        private string _name = "HeaderBlock";

        public Control[] GetTabControls()
        {
            var ans = new Control[1];

            var _clientsTable = new TableLayoutPanel();

            _clientsTable.Dock = DockStyle.Top;
            _clientsTable.Name = "headerBlockTable";
            _clientsTable.AutoSize = true;

            TableLayoutPanelTools.InsertColumn(_clientsTable.ColumnCount, _clientsTable);
            TableLayoutPanelTools.InsertRow(_clientsTable.RowCount, _clientsTable);

            var label = LayoutTools.GetLabel("Анкета", DockStyle.Top, HorizontalAlignment.Center);
            
            _clientsTable.Controls.Add(label, 0, _clientsTable.RowCount - 1);

            ans[0] = _clientsTable;

            return ans;
        }

        public void Reset()
        {
            
        }
    }
}
