﻿using AutomaticalChemistry.DatabaseWork.HAPI;
using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo;
using AutomaticalChemistry.FormBuilder.Tools.ControlAddons;
using AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms;
using AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientFinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static AutomaticalChemistry.MainLogic.MainLogic;
using AutomaticalChemistry.FormBuilder.Tools;

namespace AutomaticalChemistry.FormBuilder.TabBuilders.OrdersCreateWork
{
    public class MainConfigBlock : IControlsBlock
    {
        public string Name { get { return _name; } set { _name = value; } }

        private string _name = "MainConfigBlock";

        //public event EmptyD GarantyUpdateEvent;
        //public event EmptyD FullPriceUpdateEvent;

        /*public bool Garanty { get { return _markExist.CheckedIndex == 0; } }
        public Price FullPrice {
            get
            {
                Price ans = Price.Zero;

                foreach (var price in _spotsPrices)
                {
                    ans.PriceWholePart += price.ViewPrice.PriceWholePart;
                    ans.PriceFractionalPart += price.ViewPrice.PriceFractionalPart;
                }

                ans.PriceWholePart += ans.PriceFractionalPart / 100;
                ans.PriceFractionalPart = ans.PriceFractionalPart % 100;
                return ans;
            }
        }*/

        LabelRow _orderId;
        DateRow _orderCreateDate;
        DateRow _orderCloseDate;
        LabelRow _clientInfoLabel;
        DBClientInfo _curClientInfo;
        
        RadioButtonsRow _objectType;

        TextBoxRow _objectName;
        
        CheckBoxsRow _dirtValue;
        RadioButtonsRow _wearValue;
        CheckBoxsRow _coatingState;
        RadioButtonsRow _clayAndDetailsExist;
        RadioButtonsRow _difColor;
        RadioButtonsRow _markExist;
        FullPriceRow _fullPrice;
        RadioButtonsRow _payType;
        TextBoxRow _prePayPercent;
        
        List<TextBoxRow> _spotsTexts = new List<TextBoxRow>();
        List<FullPriceRow> _spotsPrices = new List<FullPriceRow>();
        //List<FullPriceRow> _spotsPrices = new List<FullPriceRow>();

        TableLayoutPanel _orderTable;

        public Control[] GetTabControls()
        {
            var ans = new Control[1];

            _orderTable = new TableLayoutPanel();

            _orderTable.SuspendLayout();

            _orderTable.Dock = DockStyle.Top;
            _orderTable.Name = "mainConfigBlockTable";
            _orderTable.AutoSize = true;
            //_orderTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;

            TableLayoutPanelTools.InsertColumn(_orderTable.ColumnCount, _orderTable);
            TableLayoutPanelTools.InsertColumn(_orderTable.ColumnCount, _orderTable);

            ConfigOrderId();
            ConfigOrderCreateDate();
            ConfigOrderCloseDate();
            ConfigClientInfoLabel();

            ConfigObjectType();

            ConfigObjectName();

            ConfigDirtValue();
            ConfigWearValue();
            ConfigCoatingState();
            ConfigClayAndDetailsExist();
            ConfigDifColor();
            ConfigMarkExist();

            ConfigAddSpotsButton();

            ConfigPayType();
            ConfigPrePayPercent();

            ConfigPrice();

            ConfigSpotsText(this, null);
            RemoveSpotText(_spotsTexts[0], null);
            ConfigSpotsText(this, null);

            _orderTable.ResumeLayout();
            
            ans[0] = _orderTable;

            return ans;
        }

        void ConfigOrderId()
        {
            _orderId = new LabelRow(DBMainOrderInfo.OrderMainInfoVarsNames[0] + ":", (DBController.GetMaxOrderId() + 1).ToString(), false, null);
            _orderId.ApplyToTable(_orderTable, _orderTable.RowCount, true);
        }

        void ConfigOrderCreateDate()
        {
            _orderCreateDate = new DateRow(DBMainOrderInfo.OrderMainInfoVarsNames[1] + ":",false, null);
            _orderCreateDate.ApplyToTable(_orderTable, _orderTable.RowCount, true);
        }

        void ConfigOrderCloseDate()
        {
            _orderCloseDate = new DateRow(DBMainOrderInfo.OrderMainInfoVarsNames[2] + ":", false, null);
            _orderCloseDate.ApplyToTable(_orderTable, _orderTable.RowCount, true);
        }

        void ConfigClientInfoLabel()
        {
            _clientInfoLabel = new LabelRow(DBMainOrderInfo.OrderMainInfoVarsNames[5] + ":", " ", false, null);
            _clientInfoLabel.ApplyToTable(_orderTable, _orderTable.RowCount, true);

            TableLayoutPanelTools.InsertRow(_orderTable.RowCount, _orderTable);
            var but = LayoutTools.GetButton("Выбрать");
            but.Click += SetClientInfo;
            _orderTable.Controls.Add(but, 0, _orderTable.RowCount-1);
        }

        void SetClientInfo(object sender, EventArgs e)
        {
            _curClientInfo = ClientFinderForm.GetClientInfo();
            if (_curClientInfo != null)
            {
                _clientInfoLabel.ChangeInfoLabelValue(_curClientInfo.ToString());
            }
        }

        void ConfigObjectType()
        {
            _objectType = new RadioButtonsRow(DBAdditionalOrderInfo.OrderAdditionalInfoVarsNames[0] + ":", DBAdditionalOrderInfo.ObjectTypeTexts, 3);
            _objectType.ApplyToTable(_orderTable, _orderTable.RowCount, true);
        }

        void ConfigObjectName()
        {
            _objectName = new TextBoxRow(DBMainOrderInfo.OrderMainInfoVarsNames[3] + ":", false, null, TextProcessor.AllowableSymbols.Liters | TextProcessor.AllowableSymbols.TextSymbols | TextProcessor.AllowableSymbols.Digits | TextProcessor.AllowableSymbols.Brackets, 300);
            _objectName.ApplyToTable(_orderTable, _orderTable.RowCount, true);
        }

        void ConfigDirtValue()
        {
            _dirtValue = new CheckBoxsRow(DBAdditionalOrderInfo.OrderAdditionalInfoVarsNames[1] + ":", DBAdditionalOrderInfo.DirtValueTexts, 3);
            _dirtValue.ApplyToTable(_orderTable, _orderTable.RowCount, true);

            //_dirtValue.CheckChangedEvent += GarantyRBChanged;
        }

        void ConfigWearValue()
        {
            _wearValue = new RadioButtonsRow(DBAdditionalOrderInfo.OrderAdditionalInfoVarsNames[2] + ":", DBAdditionalOrderInfo.WearValueTexts, 6);
            _wearValue.ApplyToTable(_orderTable, _orderTable.RowCount, true);
        }

        void ConfigCoatingState()
        {
            _coatingState = new CheckBoxsRow(DBAdditionalOrderInfo.OrderAdditionalInfoVarsNames[3] + ":", DBAdditionalOrderInfo.CoatingStateTexts, 2);
            _coatingState.ApplyToTable(_orderTable, _orderTable.RowCount, true);

            //_coatingState.CheckChangedEvent += GarantyRBChanged;
        }

        void ConfigClayAndDetailsExist()
        {
            _clayAndDetailsExist = new RadioButtonsRow(DBAdditionalOrderInfo.OrderAdditionalInfoVarsNames[4] + ":", DBAdditionalOrderInfo.ClayOrDetailsTexts, 3);
            _clayAndDetailsExist.ApplyToTable(_orderTable, _orderTable.RowCount, true);

            //_clayAndDetailsExist.CheckChangedEvent += GarantyRBChanged;
        }

        void ConfigDifColor()
        {
            _difColor = new RadioButtonsRow(DBAdditionalOrderInfo.OrderAdditionalInfoVarsNames[5] + ":", DBAdditionalOrderInfo.DifColorTexts, 3);
            _difColor.ApplyToTable(_orderTable, _orderTable.RowCount, true);

            //_difColor.CheckChangedEvent += GarantyRBChanged;
        }

        void ConfigMarkExist()
        {
            _markExist = new RadioButtonsRow(DBAdditionalOrderInfo.OrderAdditionalInfoVarsNames[6] + ":", DBAdditionalOrderInfo.MarkExistTexts, 3);
            _markExist.ApplyToTable(_orderTable, _orderTable.RowCount, true);

            //_markExist.CheckChangedEvent += GarantyRBChanged;
        }

        void ConfigPrice()
        {
            _fullPrice = new FullPriceRow("Сумма:", false);
            _fullPrice.ApplyToTable(_orderTable, _orderTable.RowCount, true);
        }

        void ConfigPayType()
        {
            _payType = new RadioButtonsRow(DBAdditionalOrderInfo.OrderAdditionalInfoVarsNames[7] + ":", DBAdditionalOrderInfo.PayTypeTexts, 3);
            _payType.ApplyToTable(_orderTable, _orderTable.RowCount, true);
        }

        void ConfigPrePayPercent()
        {
            _prePayPercent = new TextBoxRow(DBAdditionalOrderInfo.OrderAdditionalInfoVarsNames[8] + ":", false, "%", TextProcessor.AllowableSymbols.Digits, 30, 3);
            _prePayPercent.ApplyToTable(_orderTable, _orderTable.RowCount, true);
        }
        
        int _spotsStartRowNum;
        int _spotsCount = 0;

        void ConfigAddSpotsButton()
        {
            _spotsStartRowNum = _orderTable.RowCount;
            TableLayoutPanelTools.InsertRow(_orderTable.RowCount, _orderTable);
            var but = LayoutTools.GetButton("Добавить описание");
            but.AutoSize = true;
            but.Click += ConfigSpotsText;
            _orderTable.Controls.Add(but, 0, _spotsStartRowNum);
        }

        void ConfigSpotsText(object sender, EventArgs e)
        {
            _orderTable.SuspendLayout();

            _spotsTexts.Add(new TextBoxRow("Описание пятна:", sender != null, null, TextProcessor.AllowableSymbols.Liters | TextProcessor.AllowableSymbols.TextSymbols | TextProcessor.AllowableSymbols.Digits | TextProcessor.AllowableSymbols.Brackets, 300));
            _spotsTexts[_spotsTexts.Count - 1].RemoveClickEvent += RemoveSpotText;

            
            _spotsPrices.Add(new FullPriceRow("Цена:", true));
            _spotsPrices[_spotsPrices.Count - 1].PriceChangedEvent += SpotPriceChanged;


            _spotsPrices[_spotsPrices.Count - 1].ApplyToTable(_orderTable, _spotsStartRowNum + _spotsCount * 2, true);
            _spotsTexts[_spotsTexts.Count - 1].ApplyToTable(_orderTable, _spotsStartRowNum + _spotsCount*2, true);
            
            _orderTable.ResumeLayout();

            _spotsCount++;
        }

        void SpotPriceChanged()
        {
            var sum = Price.Zero;
            
            foreach (var spot in _spotsPrices)
            {
                sum += spot.ViewPrice;
            }

            _fullPrice.ViewPrice = sum;
        }

        void RemoveSpotText(object sender, EventArgs e)
        {
            TextBoxRow senderTextBox = (TextBoxRow)sender;
            var index = _spotsTexts.IndexOf(senderTextBox);

            _orderTable.SuspendLayout();

            //_spotsPrices[index].RemoveFromTable(_orderTable, true);
            _spotsTexts[index].RemoveFromTable(_orderTable, true);
            _spotsPrices[index].RemoveFromTable(_orderTable, true);

            _orderTable.ResumeLayout();

            //_spotsPrices.RemoveAt(index);
            _spotsTexts.RemoveAt(index);
            _spotsPrices.RemoveAt(index);

            _spotsCount--;
        }

        /*void GarantyRBChanged()
        {
            GarantyUpdateEvent?.Invoke();
        }

        void FullPriceChanged()
        {
            FullPriceUpdateEvent?.Invoke();
        }*/

        public DBOrder GetOrder()
        {
            var ans = new DBOrder()
            {
                MainInfo = new DBMainOrderInfo()
                {
                    Id = int.Parse(_orderId.InfoLabelText),
                    StartDate = _orderCreateDate.picker.Value,
                    EndDate = _orderCloseDate.picker.Value,

                    ClientInfo = _curClientInfo,
                    ObjectName = _objectName.Text
                },
                AdditionalInfo = new DBAdditionalOrderInfo()
                {
                    ObjectType = _objectType.CheckedIndex,
                    DirtValue = _dirtValue.CheckedIndexes,
                    WearValue = _wearValue.CheckedIndex,
                    CoatingState = _coatingState.CheckedIndexes,
                    ClayAndDetailsExist = _clayAndDetailsExist.CheckedIndex,
                    DifColor = _difColor.CheckedIndex,
                    MarkExist = _markExist.CheckedIndex,
                    //Price = _price.ViewPrice,
                    PayType = _payType.CheckedIndex,
                    PrePayPercent = int.Parse(string.IsNullOrEmpty(_prePayPercent.Text) ? "0" : _prePayPercent.Text)
                },
                Spots = new DBOrderSpot[_spotsCount]
            };

            for (int i = 0; i < ans.Spots.Length; i++)
            {
                ans.Spots[i] = new DBOrderSpot()
                {
                    Description = _spotsTexts[i].Text,
                    Price = _spotsPrices[i].ViewPrice,
                    //PriceWholePart = _spotsPrices[i].ViewPrice.PriceWholePart,
                    //PriceFractionalPart = _spotsPrices[i].ViewPrice.PriceFractionalPart
                };
            }

            return ans;
        }

        public void Reset()
        {
            while (_spotsTexts.Count > 1)
            {
                RemoveSpotText(_spotsTexts[1], null);
            }

            _spotsTexts[0].Reset();
            //_spotsPrices[0].Reset();

            _orderId.Reset();
            _orderId.ChangeInfoLabelValue((DBController.GetMaxOrderId()+1).ToString());
            _orderCreateDate.Reset();
            _orderCloseDate.Reset();
            _curClientInfo = null;
            _clientInfoLabel.Reset();
            _objectName.Reset();

            _objectType.Reset();

            _dirtValue          .Reset();
            _wearValue          .Reset();
            _coatingState       .Reset();
            _clayAndDetailsExist.Reset();
            _difColor           .Reset();
            _markExist          .Reset();
            _fullPrice          .Reset();
            _payType            .Reset();
            _prePayPercent      .Reset();
        }
    }
}
