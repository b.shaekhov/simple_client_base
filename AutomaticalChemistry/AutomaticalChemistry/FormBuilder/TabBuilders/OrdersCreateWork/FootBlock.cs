﻿using AutomaticalChemistry.FormBuilder.Tools;
using System;
using System.Windows.Forms;
using static AutomaticalChemistry.MainLogic.MainLogic;

namespace AutomaticalChemistry.FormBuilder.TabBuilders.OrdersCreateWork
{
    public class FootBlock : IControlsBlock
    {
        public string Name { get { return _name; } set { _name = value; } }

        private string _name = "FootBlock";

        public event EmptyD CreateOrderEvent;
        public event EmptyD PrintOrderEvent;
        public event EmptyD PrintOrderCheckEvent;

        public Control[] GetTabControls()
        {
            var ans = new Control[1];

            var _clientsTable = new TableLayoutPanel();

            _clientsTable.Dock = DockStyle.Top;
            _clientsTable.Name = "footBlockTable";
            _clientsTable.AutoSize = true;

            TableLayoutPanelTools.InsertColumn(_clientsTable.ColumnCount, _clientsTable);
            TableLayoutPanelTools.InsertColumn(_clientsTable.ColumnCount, _clientsTable);
            TableLayoutPanelTools.InsertColumn(_clientsTable.ColumnCount, _clientsTable);
            TableLayoutPanelTools.InsertRow(_clientsTable.RowCount, _clientsTable);

            var createBut = LayoutTools.GetButton("Создать");
            var printBut = LayoutTools.GetButton("Отправить на печать");
            var printCheckBut = LayoutTools.GetButton("Печать чека");

            createBut.Click += CreateOrder;
            printBut.Click += PrintOrderClicked;
            printCheckBut.Click += PrintOrderCheckClicked;

            _clientsTable.Controls.Add(createBut, 0, _clientsTable.RowCount - 1);
            _clientsTable.Controls.Add(printBut, 1, _clientsTable.RowCount - 1);
            _clientsTable.Controls.Add(printCheckBut, 2, _clientsTable.RowCount - 1);

            ans[0] = _clientsTable;

            return ans;
        }

        void CreateOrder(object sender, EventArgs e)
        {
            CreateOrderEvent?.Invoke();
        }

        void PrintOrderClicked(object sender, EventArgs e)
        {
            PrintOrderEvent?.Invoke();
        }

        void PrintOrderCheckClicked(object sender, EventArgs e)
        {
            PrintOrderCheckEvent?.Invoke();
        }

        public void Reset()
        {
            
        }
    }
}
