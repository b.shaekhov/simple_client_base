﻿using AutomaticalChemistry.DatabaseWork.HAPI;
using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo;
using AutomaticalChemistry.FormBuilder.Tools;
using AutomaticalChemistry.FormBuilder.Tools.Print;
using AutomaticalChemistry.FormBuilder.TabBuilders.SendSMSWork.DialogForms;
using AutomaticalChemistry.FormBuilder.Tools.PrintCheck;

namespace AutomaticalChemistry.FormBuilder.TabBuilders
{
    public class OrdersViewTab : IControlsBlock
    {
        public string Name { get { return _name; } set { _name = value; } }

        private TableLayoutPanel _ordersTable;

        DBOrder[] _viewOrders;
        Button[] _orderPrintButs;
        Button[] _orderCheckPrintButs;
        Button[] _orderEndedButs;
        Button[] _orderDeleteButs;
        Button[] _orderSendSMSButs;

        string _name;

        public OrdersViewTab(string name)
        {
            _name = name;
        }

        public Control[] GetTabControls()
        {
            var ans = new Control[1];

            OrdersViewUpdated();

            ans[0] = _ordersTable;

            MainLogic.MainLogic.OrdersDBUpdatedEvent -= OrdersViewUpdated;
            MainLogic.MainLogic.OrdersDBUpdatedEvent += OrdersViewUpdated;

            return ans;
        }

        void OrdersViewUpdated()
        {
            if (_ordersTable == null)
                _ordersTable = new TableLayoutPanel();
            else
            {
                _ordersTable.SuspendLayout();

                _ordersTable.Controls.Clear();
                _ordersTable.ColumnCount = 0;
                _ordersTable.RowCount = 0;
                _ordersTable.ColumnStyles.Clear();
                _ordersTable.RowStyles.Clear();

                _ordersTable.ResumeLayout();
            }

            _ordersTable.SuspendLayout();

            _ordersTable.Dock = DockStyle.Top;
            _ordersTable.Name = "clientsTable";
            _ordersTable.AutoSize = true;
            _ordersTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;

            _ordersTable.ColumnCount = 7;

            for (int i = 0; i < _ordersTable.ColumnCount; i++)
                _ordersTable.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));

            _ordersTable.ColumnStyles[_ordersTable.ColumnCount - 2].Width = 50;
            _ordersTable.ColumnStyles[_ordersTable.ColumnCount - 2].SizeType = SizeType.Percent;

            _ordersTable.RowCount = 1;
            _ordersTable.RowStyles.Add(new RowStyle(SizeType.AutoSize));

            int columnNum = 0;

            _ordersTable.Controls.Add(LayoutTools.GetLabel("Номер\r\nзаказа", DockStyle.Top, HorizontalAlignment.Center), columnNum, 0); columnNum++;
            _ordersTable.Controls.Add(LayoutTools.GetLabel("Дней\r\nосталось", DockStyle.Top, HorizontalAlignment.Center), columnNum, 0); columnNum++;
            _ordersTable.Controls.Add(LayoutTools.GetLabel("Дата\r\nначала/конца", DockStyle.Top, HorizontalAlignment.Center), columnNum, 0); columnNum++;
            //_ordersTable.Controls.Add(LayoutTools.GetLabel("Дата\r\nконца", DockStyle.Top, HorizontalAlignment.Center), columnNum, 0); columnNum++;
            _ordersTable.Controls.Add(LayoutTools.GetLabel("Информация\r\nо клиенте", DockStyle.Top, HorizontalAlignment.Center), columnNum, 0); columnNum++;
            _ordersTable.Controls.Add(LayoutTools.GetLabel("Изделие", DockStyle.Top, HorizontalAlignment.Center), columnNum, 0); columnNum++;
            _ordersTable.Controls.Add(LayoutTools.GetLabel("Пятна", DockStyle.Top, HorizontalAlignment.Center), columnNum, 0); columnNum++;

            _viewOrders = DBController.GetAllOrders();
            _orderPrintButs = new Button[_viewOrders.Length];
            _orderCheckPrintButs = new Button[_viewOrders.Length];
            _orderEndedButs = new Button[_viewOrders.Length];
            _orderDeleteButs = new Button[_viewOrders.Length];
            _orderSendSMSButs = new Button[_viewOrders.Length];

            var comparer = new DBOrder.DBOrderEndDateComparer();

            Array.Sort(_viewOrders, comparer);

            int butsCounter = 0;

            foreach (var order in _viewOrders)
            {
                TableLayoutPanelTools.InsertRow(_ordersTable.RowCount, _ordersTable);

                columnNum = 0;

                var idPanel = new Panel();
                idPanel.AutoSize = true;
                idPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;

                var idLabel = LayoutTools.GetLabel(order.MainInfo.Id.ToString(), DockStyle.Top, HorizontalAlignment.Left);
                var idPrintBut = LayoutTools.GetButton("Печать");
                idPrintBut.Click += PrintButClick;
                
                _orderPrintButs[butsCounter] = idPrintBut;
                
                idPrintBut.Location = new Point(0, idLabel.PreferredSize.Height);

                idPrintBut.AutoSize = true;
                idPrintBut.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                
                idPanel.Controls.Add(idLabel);
                idPanel.Controls.Add(idPrintBut);

                _ordersTable.Controls.Add(idPanel, columnNum, _ordersTable.RowCount - 1); columnNum++;
                _ordersTable.Controls.Add(LayoutTools.GetLabel(order.MainInfo.Executed ? "-" : (order.MainInfo.EndDate - DateTime.Now).Days.ToString(), DockStyle.Top, HorizontalAlignment.Left), columnNum, _ordersTable.RowCount - 1); columnNum++;
                _ordersTable.Controls.Add(LayoutTools.GetLabel(order.MainInfo.StartDate.ToString("dd.MM.yyyy") + "\r\n" + order.MainInfo.EndDate.ToString("dd.MM.yyyy"), DockStyle.Top, HorizontalAlignment.Left), columnNum, _ordersTable.RowCount - 1); columnNum++;
                //_ordersTable.Controls.Add(LayoutTools.GetLabel(, DockStyle.Top, ContentAlignment.TopLeft), columnNum, _ordersTable.RowCount - 1); columnNum++;
                _ordersTable.Controls.Add(LayoutTools.GetLabel(order.MainInfo.ClientInfo.ToString(), DockStyle.Top, HorizontalAlignment.Left), columnNum, _ordersTable.RowCount - 1); columnNum++;
                _ordersTable.Controls.Add(LayoutTools.GetLabel(order.MainInfo.ObjectName, DockStyle.Top, HorizontalAlignment.Left), columnNum, _ordersTable.RowCount - 1); columnNum++;
                StringBuilder spotsText = new StringBuilder();
                
                foreach (var spot in order.Spots)
                {
                    spotsText.Append(string.Format("{0}\r\n"/*+"{1} {2} {3} {4}\r\n"*/, spot.Description/*, spot.PriceWholePart, Price.WholePartName,spot.PriceFractionalPart, Price.FractionalPartName*/));
                }

                _ordersTable.Controls.Add(LayoutTools.GetLabel(spotsText.ToString(), DockStyle.Top, HorizontalAlignment.Left), columnNum, _ordersTable.RowCount - 1); columnNum++;

                var butsPanel = new Panel();
                butsPanel.AutoSize = true;
                butsPanel.Margin = Padding.Empty;

                var idSendSMSBut = LayoutTools.GetButton("Отправить СМС");
                idSendSMSBut.Click += SendSMSClick;
                _orderSendSMSButs[butsCounter] = idSendSMSBut;

                var idEndedBut = LayoutTools.GetButton(order.MainInfo.Executed ? "Не исполнено" : "Исполнено");
                idEndedBut.Location = new Point(idSendSMSBut.PreferredSize.Width, 0);
                idEndedBut.Click += EndButClick;
                _orderEndedButs[butsCounter] = idEndedBut;

                var idDeleteBut = LayoutTools.GetButton("Удалить заказ");
                idDeleteBut.Location = new Point(0, idEndedBut.PreferredSize.Height);
                idDeleteBut.Click += DeleteButClick;

                var idCheckPrintBut = LayoutTools.GetButton("Печать чека");
                idCheckPrintBut.Location = new Point(idSendSMSBut.PreferredSize.Width, idEndedBut.PreferredSize.Height);
                idCheckPrintBut.Click += PrintCheckButClick;
                
                _orderDeleteButs[butsCounter] = idDeleteBut;
                _orderCheckPrintButs[butsCounter] = idCheckPrintBut;
                butsCounter++;

                butsPanel.Controls.Add(idSendSMSBut);
                butsPanel.Controls.Add(idEndedBut);
                butsPanel.Controls.Add(idDeleteBut);
                butsPanel.Controls.Add(idCheckPrintBut);

                _ordersTable.Controls.Add(butsPanel, columnNum, _ordersTable.RowCount - 1); columnNum++;
            }

            _ordersTable.ResumeLayout();
        }

        void PrintButClick(object sender, EventArgs e)
        {
            //#warning ChangeME!!
            PrintOrder.Print(_viewOrders[Array.IndexOf(_orderPrintButs, sender)]);
            //PrintForm.ShowForm(_viewOrders[Array.IndexOf(_orderPrintButs, sender)]);
        }

        void PrintCheckButClick(object sender, EventArgs e)
        {
            //#warning ChangeME!!
            CheckPrinter.PrintCheck(_viewOrders[Array.IndexOf(_orderCheckPrintButs, sender)]);
            //PrintForm.ShowForm(_viewOrders[Array.IndexOf(_orderPrintButs, sender)]);
        }

        void SendSMSClick(object sender, EventArgs e)
        {
            var order = _viewOrders[Array.IndexOf(_orderSendSMSButs, sender)];

            SendSMSAboutOrderForm.SendOrderToClient(order);
        }

        void EndButClick(object sender, EventArgs e)
        {
            var order = _viewOrders[Array.IndexOf(_orderEndedButs, sender)];

            var result = ShowInfoForUser.Question("Вы уверены что заказ " + ((Button)sender).Text.ToLower() + "?");

            if (result)
            {
                order.MainInfo.Executed = !order.MainInfo.Executed;

                MainLogic.MainLogic.UpdateExecuteOrder(order);
            }
        }

        void DeleteButClick(object sender, EventArgs e)
        {
            var order = _viewOrders[Array.IndexOf(_orderDeleteButs, sender)];

            var result = ShowInfoForUser.Question("Вы уверены что хотите удалить заказ?");

            if (result)
            {
                MainLogic.MainLogic.DeleteOrder(order);
            }
        }

        public void Reset()
        {

        }
    }
}
