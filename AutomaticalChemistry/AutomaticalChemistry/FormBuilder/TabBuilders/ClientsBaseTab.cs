﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork;

namespace AutomaticalChemistry.FormBuilder.TabBuilders
{
    public class ClientsBaseTab : IControlsBlock
    {
        public string Name { get { return _name; } set { _name = value; } }

        string _name;

        private IControlsBlock _viewClientsBlock;
        private IControlsBlock _clientsWorkBlock;
        private IControlsBlock[] _controlBlocks;

        public ClientsBaseTab(string name)
        {
            _name = name;
        }

        public Control[] GetTabControls()
        {
            Init();

            _controlBlocks = new IControlsBlock[] { _viewClientsBlock, _clientsWorkBlock };

            Control[][] blocksControls = new Control[_controlBlocks.Length][];

            int length = 0;
            for (int i = 0; i < _controlBlocks.Length; i++)
            {
                blocksControls[i] = _controlBlocks[i].GetTabControls();
                length += blocksControls[i].Length;
            }

            var ans = new Control[length];

            var ansCounter = 0;

            for (int i = 0; i < blocksControls.Length; i++)
            {
                foreach (var control in blocksControls[i])
                {
                    ans[ansCounter] = control;
                    ansCounter++;
                }
            }

            return ans;
        }

        void Init()
        {
            if (_viewClientsBlock == null)
                _viewClientsBlock = new ClientsViewerBlock();
            if (_clientsWorkBlock == null)
                _clientsWorkBlock = new ClientsWorkBlock();
        }

        public void Reset()
        {
            if (_controlBlocks == null)
                return;

            for (int i = 0; i < _controlBlocks.Length; i++)
            {
                _controlBlocks[i].Reset();
            }
        }
    }
}
