﻿using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using AutomaticalChemistry.FormBuilder.Tools;
using System;
using System.Windows.Forms;
using System.Linq;
using AutomaticalChemistry.FormBuilder.Tools.ControlAddons;
using AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientFinder;
using Newtonsoft.Json;
using AutomaticalChemistry.DatabaseWork.HAPI;
using System.Text;
using System.IO;
using System.Diagnostics;
using AutomaticalChemistry.FormBuilder.TabBuilders.SendSMSWork;

namespace AutomaticalChemistry.FormBuilder.TabBuilders
{
    public class SendSMSTab : IControlsBlock
    {
        const string PatternFileName = @"Temp\PatternSMSSubscription.txt";

        public string Name { get { return _name; } set { _name = value; } }
        
        string _name;
        
        private TableLayoutPanel _controlTable;
        
        private TextBoxRow _filterTextBox;

        private SMSBuilder _messageBuilder;

        int comboBoxItemsCount;

        public SendSMSTab(string name)
        {
            _name = name;
        }

        public Control[] GetTabControls()
        {
            if (_messageBuilder == null)
            {
                var comboBoxItems = DBClientName.ClientVarsNames.Union(new string[] { "Номера телефонов" }).ToArray();
                comboBoxItemsCount = comboBoxItems.Length;

                _messageBuilder = new SMSBuilder("builder", comboBoxItems);
                
                _controlTable = new TableLayoutPanel();

                _controlTable.SuspendLayout();

                _controlTable.AutoSize = true;
                _controlTable.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                _controlTable.Dock = DockStyle.Top;

                TableLayoutPanelTools.InsertColumn(_controlTable.ColumnCount, _controlTable);
                TableLayoutPanelTools.InsertColumn(_controlTable.ColumnCount, _controlTable);
                TableLayoutPanelTools.InsertRow(_controlTable.RowCount, _controlTable);
                
                _filterTextBox = new TextBoxRow("Фильтр", false, null, TextProcessor.AllowableSymbols.Liters | TextProcessor.AllowableSymbols.TextSymbols | TextProcessor.AllowableSymbols.PhoneSymbols | TextProcessor.AllowableSymbols.Digits | TextProcessor.AllowableSymbols.Brackets);
                _filterTextBox.Text = DBClientName.AgreeSubscriptionTexts[0];
                _filterTextBox.ApplyToTable(_controlTable, _controlTable.RowCount, true);

                var checkFilterBut = LayoutTools.GetButton("Проверить фильтр");
                checkFilterBut.Click += CheckFiltrClick;
                TableLayoutPanelTools.InsertRow(_controlTable.RowCount, _controlTable);
                _controlTable.Controls.Add(checkFilterBut, 0, _controlTable.RowCount-1);

                var loadPatternBut = LayoutTools.GetButton("Загрузить из шаблона");
                loadPatternBut.Click += LoadPatternClick;
                TableLayoutPanelTools.InsertRow(_controlTable.RowCount, _controlTable);
                _controlTable.Controls.Add(loadPatternBut, 0, _controlTable.RowCount - 1);

                var createPatternBut = LayoutTools.GetButton("Сохранить шаблон");
                createPatternBut.Click += CreatePatternClick;
                //TableLayoutPanelTools.InsertRow(_controlTable.RowCount, _controlTable);
                _controlTable.Controls.Add(createPatternBut, 1, _controlTable.RowCount - 1);

                var sendBut = LayoutTools.GetButton("Разослать!");
                sendBut.Click += SendClick;
                TableLayoutPanelTools.InsertRow(_controlTable.RowCount, _controlTable);
                _controlTable.Controls.Add(sendBut, 0, _controlTable.RowCount - 1);

                _controlTable.ResumeLayout();
            }

            var messageBuilderControls = _messageBuilder.GetTabControls();

            var ans = new Control[messageBuilderControls.Length+1];

            int indexer = 0;

            ans[indexer] = _controlTable;
            indexer++;

            foreach (var mbcControl in messageBuilderControls)
            {
                ans[indexer] = mbcControl;
                indexer++;
            }
            
            return ans;
        }

        void LoadPatternClick(object sender, EventArgs e)
        {
            try
            {
                _messageBuilder.ConfigureFromSerialisedPattern(File.ReadAllText(PatternFileName));
            }
            catch (Exception exc)
            {
                ShowInfoForUser.Error("Не удалось загрузить шаблон!\r\n" + exc.Message);
            }
        }

        void CreatePatternClick(object sender, EventArgs e)
        {
            File.WriteAllText(PatternFileName, _messageBuilder.GetSerialisedPattern());
            ShowInfoForUser.Message("Шаблон сохранен");
        }

        void SendClick(object sender, EventArgs e)
        {
            var error = _messageBuilder.CheckInfoBoxes();

            if (error != null)
            {
                ShowInfoForUser.Error(error);
                return;
            }

            if (!ShowInfoForUser.Question("Вы действительно хотите разослать СМС\r\nсообщения согласно данным настройкам?"))
                return;
            
            var sendClients = ClientFinder.GetFilteredClients(_filterTextBox.Text, DBController.GetAllClientInfo(false));

            var sendData = new SMSSendData()
            {
                Data = new SMSSendData.ClientData[sendClients.Count()]
            };

            int counter = 0;

            bool badClient = false;

            foreach (var client in sendClients)
            {
                if (!badClient && !client.Name.AgreeSubscription)
                {
                    if (!ShowInfoForUser.Question("Вы действительно хотите разослать СМС\r\nклиентам, которые отказались от рассылки?"))
                        return;

                    badClient = true;
                }

                sendData.Data[counter] = new SMSSendData.ClientData()
                {
                    PhoneNums = (from x in client.PhoneNumbers select x.Value).ToArray(),
                    Message = BuildMessageForClient(client)
                };
                counter++;
            }

            File.WriteAllText("SMSSendData.txt",JsonConvert.SerializeObject(sendData, Formatting.Indented));

            try
            {
                var senderWork = Process.Start(Application.StartupPath + "\\" + ConfigsControl.ReadedConfigs.SMSSenderName);

                _messageBuilder.Enabled = false;
                _controlTable.Enabled = false;

                senderWork.Exited += EndSend;
            } catch (Exception ex)
            {
                ShowInfoForUser.Error("Не удалось запустить программу-рассылатель СМС!\r\n" + ex.Message);
            }
        }

        void EndSend(object sender, EventArgs e)
        {
            ((Process)sender).Exited -= EndSend;

            _messageBuilder.Enabled = true;
            _controlTable.Enabled = true;
        }

        string BuildMessageForClient(DBClientInfo client)
        {
            var ansBuilder = new StringBuilder();

            var infoBoxes = _messageBuilder.InfoBoxes;

            foreach (var field in infoBoxes)
            {
                if (field is TextBox)
                {
                    ansBuilder.Append(field.Text);
                }
                if (field is ComboBox)
                {
                    var index = ((ComboBox)field).SelectedIndex;

                    if (index == comboBoxItemsCount - 1)
                    {
                        ansBuilder.Append(client.BuildStringFromPhoneNums());
                    } else
                    {
                        ansBuilder.Append(client.Name.GetValueFromNameIndex(index));
                    }
                }
            }

            return ansBuilder.ToString();
        }
        
        void CheckFiltrClick(object sender, EventArgs e)
        {
            ClientFinderForm.GetClientInfo(true, _filterTextBox.Text);
        }
        
        public void Reset()
        {
            _messageBuilder.Reset();
        }
    }
}
