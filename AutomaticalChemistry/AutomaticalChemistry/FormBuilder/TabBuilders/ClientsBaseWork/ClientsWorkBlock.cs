﻿using System;
using System.Windows.Forms;
using AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientFinder;
using AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientCreater;
using AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientChanger;
using AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientRemover;
using AutomaticalChemistry.FormBuilder.Tools;

namespace AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork
{
    public class ClientsWorkBlock : IControlsBlock
    {
        public string Name { get { return _name; } set { _name = value; } }
        string _name;
        
        public Control[] GetTabControls()
        {
            var ans = new Control[1];
            
            var _clientsTable = new TableLayoutPanel();

            _clientsTable.Dock = DockStyle.Top;
            _clientsTable.Name = "clientsCreaterTable";
            _clientsTable.AutoSize = true;

            TableLayoutPanelTools.InsertColumn(_clientsTable.ColumnCount, _clientsTable);
            //TableLayoutPanelTools.InsertColumn(_clientsTable.ColumnCount, _clientsTable);
            //TableLayoutPanelTools.InsertColumn(_clientsTable.ColumnCount, _clientsTable);
            //TableLayoutPanelTools.InsertColumn(_clientsTable.ColumnCount, _clientsTable);
            TableLayoutPanelTools.InsertRow(_clientsTable.RowCount, _clientsTable);
            TableLayoutPanelTools.InsertRow(_clientsTable.RowCount, _clientsTable);
            TableLayoutPanelTools.InsertRow(_clientsTable.RowCount, _clientsTable);
            TableLayoutPanelTools.InsertRow(_clientsTable.RowCount, _clientsTable);

            var findBut = LayoutTools.GetButton("Поиск клиента");
            findBut.Click += FindClickEv;
            var createBut = LayoutTools.GetButton("Создание клиента");
            createBut.Click += CreateClickEv;
            var changeBut = LayoutTools.GetButton("Изменение клиента");
            changeBut.Click += ChangeClickEv;
            var deleteBut = LayoutTools.GetButton("Удаление клиента");
            deleteBut.Click += DeleteClickEv;

            _clientsTable.Controls.Add(findBut, 0, 0);
            _clientsTable.Controls.Add(createBut, 0, 1);
            _clientsTable.Controls.Add(changeBut, 0, 2);
            _clientsTable.Controls.Add(deleteBut, 0, 3);

            ans[0] = _clientsTable;

            return ans;
        }
        
        void FindClickEv(object sender, EventArgs e)
        {
            ClientFinderForm.GetClientInfo(true);
        }

        void CreateClickEv(object sender, EventArgs e)
        {
            ClientCreaterForm.CreateNewClient();
        }

        void ChangeClickEv(object sender, EventArgs e)
        {
            ClientChangerForm.ChangeClient();
        }

        void DeleteClickEv(object sender, EventArgs e)
        {
            ClientRemover.RemoveClient();
        }

        public void Reset()
        {
            
        }
    }
}
