﻿using AutomaticalChemistry.DatabaseWork.HAPI;
using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientFinder;
using System;
using System.Windows.Forms;

namespace AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientRemover
{
    public static class ClientRemover
    {
        public static void RemoveClient()
        {
            while (true)
            {
                var foundedData = ClientFinderForm.GetClientInfo();

                if (foundedData != null)
                    if (!MainLogic.MainLogic.TryRemoveClient(foundedData))
                        continue;
                break;
            }
        }
    }
}
