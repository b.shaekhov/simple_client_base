﻿using AutomaticalChemistry.DatabaseWork.HAPI;
using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientFinder;
using AutomaticalChemistry.Properties;
using System;
using System.Windows.Forms;

namespace AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientChanger
{
    public class ClientChangerForm : Form
    {
        static ClientChangerForm _instance;

        public ClientChangerForm()
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        }

        public static void ChangeClient()
        {
            var changableClient = ClientFinderForm.GetClientInfo();

            if (changableClient == null)
                return;

            _instance = new ClientChangerForm();
            _instance.AutoScroll = true;
            _instance.FormBorderStyle = FormBorderStyle.SizableToolWindow;

            _instance.Icon = Resources.Program;

            var changer = new ClientChanger();
            
            changer.SetChangeClient(changableClient);
            changer.ChangedEvent += ChangeEnd;

            _instance.Controls.AddRange(changer.GetTabControls());

            _instance.Size = new System.Drawing.Size(_instance.PreferredSize.Width + 50, 350);

            _instance.ShowDialog();
            _instance = null;
        }
        
        static void ChangeEnd()
        {
            _instance.Close();
        }
    }
}
