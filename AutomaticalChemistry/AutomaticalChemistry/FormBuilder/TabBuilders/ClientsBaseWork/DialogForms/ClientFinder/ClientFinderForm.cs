﻿using AutomaticalChemistry.DatabaseWork.HAPI;
using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using AutomaticalChemistry.Properties;
using System;
using System.Windows.Forms;

namespace AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientFinder
{
    public class ClientFinderForm : Form
    {
        static ClientFinderForm _instance;

        public ClientFinderForm()
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        }

        public static DBClientInfo GetClientInfo(bool preview = false, string staticFilter = null)
        {
            _instance = new ClientFinderForm();
            _instance.AutoScroll = true;
            _instance.FormBorderStyle = FormBorderStyle.SizableToolWindow;

            _instance.Icon = Resources.Program;

            var finder = new ClientFinder(preview, staticFilter);

            var clientInfos = DBController.GetAllClientInfo(false);
            finder.SetInfos(clientInfos);
            finder.FoundEvent += FoundedData;

            _instance.Controls.AddRange(finder.GetTabControls());

            _instance.Size = new System.Drawing.Size(_instance.PreferredSize.Width, Math.Min(_instance.PreferredSize.Height, 400));

            _instance.ShowDialog();
            _instance = null;

            return finder.FindedInfo;
        }
        
        static void FoundedData()
        {
            _instance.Close();
        }
    }
}
