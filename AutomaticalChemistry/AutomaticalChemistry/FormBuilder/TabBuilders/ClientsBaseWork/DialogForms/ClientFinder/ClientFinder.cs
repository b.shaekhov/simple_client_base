﻿using AutomaticalChemistry.DatabaseWork.HAPI;
using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using AutomaticalChemistry.FormBuilder.Tools;
using AutomaticalChemistry.FormBuilder.Tools.ControlAddons;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static AutomaticalChemistry.MainLogic.MainLogic;

namespace AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientFinder
{
    public class ClientFinder : IControlsBlock
    {
        public string Name { get { return _name; } set { _name = value; } }

        string _name;

        TextBoxRow _finderTextBox;
        TableLayoutPanel _findedTable;
        Button _updateButton;

        public DBClientInfo FindedInfo { get; private set; }

        DBClientInfo[] _allInfo;
        DBClientInfo[] _viewInfo;
        Button[] _choiceButs;

        public event EmptyD FoundEvent;

        private bool _previewMode;
        private string _staticFilter;

        public ClientFinder(bool previewMode, string staticFilter)
        {
            _previewMode = previewMode;
            _staticFilter = staticFilter;
        }

        public Control[] GetTabControls()
        {
            return new Control[] { _findedTable };
        }
        
        public void SetInfos(DBClientInfo[] infos)
        {
            _allInfo = infos;
            UpdateTable();
        }

        void UpdateTable()
        {
            if (_findedTable == null)
                _findedTable = new TableLayoutPanel();
            else
            {
                _findedTable.SuspendLayout();

                _findedTable.Controls.Clear();
                _findedTable.ColumnCount = 0;
                _findedTable.RowCount = 0;
                _findedTable.ColumnStyles.Clear();
                _findedTable.RowStyles.Clear();

                _findedTable.ResumeLayout();
            }

            _findedTable.SuspendLayout();

            //_findedTable.Dock = DockStyle.Top;
            _findedTable.Name = "clientsTable";
            _findedTable.AutoSize = true;
            _findedTable.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _findedTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Inset;

            TableLayoutPanelTools.InsertColumn(_findedTable.ColumnCount, _findedTable);
            TableLayoutPanelTools.InsertColumn(_findedTable.ColumnCount, _findedTable);
            TableLayoutPanelTools.InsertColumn(_findedTable.ColumnCount, _findedTable);
            if (!_previewMode)
                TableLayoutPanelTools.InsertColumn(_findedTable.ColumnCount, _findedTable);
            
            _findedTable.RowCount = 3;
            _findedTable.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            _findedTable.RowStyles.Add(new RowStyle(SizeType.Absolute, 20));
            _findedTable.RowStyles.Add(new RowStyle(SizeType.AutoSize));

            _findedTable.Controls.Add(LayoutTools.GetLabel("Ф.И.О.", DockStyle.Top, HorizontalAlignment.Center), 0, 2);
            _findedTable.Controls.Add(LayoutTools.GetLabel("Номер телефона", DockStyle.Top, HorizontalAlignment.Center), 1, 2);
            _findedTable.Controls.Add(LayoutTools.GetLabel("Рассылка/Оповещания", DockStyle.Top, HorizontalAlignment.Center), 2, 2);

            if (_finderTextBox == null)
            {
                _finderTextBox = new TextBoxRow("Фильтр", false, null, TextProcessor.AllowableSymbols.Liters | TextProcessor.AllowableSymbols.Digits | TextProcessor.AllowableSymbols.PhoneSymbols | TextProcessor.AllowableSymbols.TextSymbols, 400, 500);

                if (_staticFilter != null)
                {
                    _finderTextBox.Text = _staticFilter;
                    _finderTextBox.InternalTextBox.Enabled = false;
                }
                else
                {
                    _updateButton = LayoutTools.GetButton("Применить");
                    _updateButton.Click += OnUpdateClick;
                }
            }

            _finderTextBox.ApplyToTable(_findedTable, 0, false);

            if (_staticFilter == null)
                _findedTable.Controls.Add(_updateButton, 2, 0);

            _viewInfo = GetFilteredClients(_finderTextBox.Text, _allInfo).ToArray();

            if (!_previewMode)
                _choiceButs = new Button[_viewInfo.Length];

            int butsCounter = 0;

            foreach (var info in _viewInfo)
            {
                TableLayoutPanelTools.InsertRow(_findedTable.RowCount, _findedTable);
                _findedTable.Controls.Add(LayoutTools.GetLabel(info.Name.ToString(), DockStyle.None, HorizontalAlignment.Left), 0, _findedTable.RowCount - 1);
                StringBuilder nums = new StringBuilder();

                foreach (var num in info.PhoneNumbers)
                {
                    nums.Append(num.Value);
                    nums.Append("\r\n");
                }

                _findedTable.Controls.Add(LayoutTools.GetLabel(nums.ToString(), DockStyle.None, HorizontalAlignment.Left), 1, _findedTable.RowCount - 1);
                _findedTable.Controls.Add(LayoutTools.GetLabel((info.Name.GetValueFromNameIndex(3) + "/") + (info.Name.GetValueFromNameIndex(4)), DockStyle.Top, HorizontalAlignment.Left), 2, _findedTable.RowCount - 1);

                if (!_previewMode)
                {
                    _choiceButs[butsCounter] = LayoutTools.GetButton("Выбрать");
                    _choiceButs[butsCounter].Click += ChoiceClick;
                    _findedTable.Controls.Add(_choiceButs[butsCounter], 3, _findedTable.RowCount - 1);

                    butsCounter++;
                }
            }

            _findedTable.ResumeLayout();
        }

        void ChoiceClick(object sender, EventArgs e)
        {
            FindedInfo = _viewInfo[Array.IndexOf(_choiceButs, sender)];
            FoundEvent?.Invoke();
        }

        void OnUpdateClick(object sender, EventArgs e)
        {
            UpdateTable();
        }

        public static IEnumerable<DBClientInfo> GetFilteredClients(string clientsFilter, DBClientInfo[] fullInfo)
        {
            string[] filters = clientsFilter.Split(' ');

            Queue<DBClientInfo> resultInfos = new Queue<DBClientInfo>();

            if (filters.Length < 1 || (filters.Length == 1 && string.IsNullOrEmpty(filters[0])))
            {
                foreach (var info in fullInfo)
                {
                    resultInfos.Enqueue(info);
                }
            }
            else
            {
                foreach (var info in fullInfo)
                {
                    bool insert = true;

                    foreach (var rawFilter in filters)
                    {
                        var filter = rawFilter.ToLower();

                        if (info.Name.SubName.ToLower().Contains(filter) ||
                            info.Name.Name.ToLower().Contains(filter) ||
                            info.Name.FatherName.ToLower().Contains(filter) ||
                            info.Name.GetValueFromNameIndex(3).ToLower() == filter ||
                            info.Name.GetValueFromNameIndex(4).ToLower() == filter)
                        {
                            continue;
                        }

                        bool numFiltered = false;

                        foreach (var num in info.PhoneNumbers)
                        {
                            if (num.Value.ToLower().Contains(filter))
                            {
                                numFiltered = true;
                                break;
                            }
                        }

                        if (numFiltered)
                            continue;

                        insert = false;
                        break;
                    }

                    if (insert)
                        resultInfos.Enqueue(info);
                }
            }

            return resultInfos;
        }
        
        public void Reset()
        {

        }
    }
}