﻿using AutomaticalChemistry.DatabaseWork.HAPI;
using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using AutomaticalChemistry.Properties;
using System;
using System.Windows.Forms;

namespace AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientCreater
{
    public class ClientCreaterForm : Form
    {
        static ClientCreaterForm _instance;

        public ClientCreaterForm()
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        }

        public static void CreateNewClient()
        {
            _instance = new ClientCreaterForm();
            _instance.AutoScroll = true;
            _instance.FormBorderStyle = FormBorderStyle.SizableToolWindow;

            _instance.Icon = Resources.Program;

            var creater = new ClientCreater();
            
            creater.CreatedEvent += EndCreate;

            _instance.Controls.AddRange(creater.GetTabControls());

            _instance.Size = new System.Drawing.Size(_instance.PreferredSize.Width + 50, 350);

            _instance.ShowDialog();
            _instance = null;
        }
        
        static void EndCreate()
        {
            _instance.Close();
        }
    }
}
