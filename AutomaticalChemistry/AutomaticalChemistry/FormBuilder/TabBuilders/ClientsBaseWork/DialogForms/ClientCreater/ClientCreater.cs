﻿using AutomaticalChemistry.DatabaseWork.HAPI;
using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using AutomaticalChemistry.FormBuilder.Tools;
using AutomaticalChemistry.FormBuilder.Tools.ControlAddons;
using AutomaticalChemistry.MainLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static AutomaticalChemistry.MainLogic.MainLogic;

namespace AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork.DialogForms.ClientCreater
{
    public class ClientCreater : IControlsBlock
    {
        public string Name { get { return _name; } set { _name = value; } }

        string _name;

        TextBoxRow _subNameRow;
        TextBoxRow _nameRow;
        TextBoxRow _fatherNameRow;
        CheckBoxsRow _agreeSubscriptionRow;
        CheckBoxsRow _phoneNotificationRow;

        public List<TextBoxRow> PhoneRows = new List<TextBoxRow>();

        TableLayoutPanel _clientsTable;

        public event EmptyD CreatedEvent;

        public Control[] GetTabControls()
        {
            var ans = new Control[1];

            _clientsTable = new TableLayoutPanel();

            //_clientsTable.Dock = DockStyle.Top;
            _clientsTable.Name = "clientsCreaterTable";
            _clientsTable.AutoSize = true;

            TableLayoutPanelTools.InsertColumn(_clientsTable.ColumnCount, _clientsTable);
            TableLayoutPanelTools.InsertColumn(_clientsTable.ColumnCount, _clientsTable);

            _subNameRow = new TextBoxRow("Фамилия клиента", false, null, TextProcessor.AllowableSymbols.Liters);
            
            _subNameRow.ApplyToTable(_clientsTable, 0, true);

            _nameRow = new TextBoxRow("Имя клиента", false, null, TextProcessor.AllowableSymbols.Liters);
            

            _nameRow.ApplyToTable(_clientsTable, 1, true);

            _fatherNameRow = new TextBoxRow("Отчество клиента", false, null, TextProcessor.AllowableSymbols.Liters);
            
            _fatherNameRow.ApplyToTable(_clientsTable, 2, true);

            _agreeSubscriptionRow = new CheckBoxsRow("Согласие на рассылку", new string[] { "" }, 1);
            _agreeSubscriptionRow.ApplyToTable(_clientsTable, 3, true);

            _phoneNotificationRow = new CheckBoxsRow("Оповещание по телефону", new string[] { "" }, 1);
            _phoneNotificationRow.ApplyToTable(_clientsTable, 4, true);

            TableLayoutPanelTools.InsertRow(_clientsTable.RowCount, _clientsTable);

            var addButton = LayoutTools.GetButton("Добавить номер телефона");

            addButton.Click += AddRow;

            curRow = _clientsTable.RowCount - 1;
            _clientsTable.Controls.Add(addButton, 0, _clientsTable.RowCount - 1);

            TableLayoutPanelTools.InsertRow(_clientsTable.RowCount, _clientsTable);

            var createClientButton = LayoutTools.GetButton("Добавить запись");
            createClientButton.Click += CreateClient;
            _clientsTable.Controls.Add(createClientButton, 1, _clientsTable.RowCount - 1);

            AddRow(addButton, null);

            ans[0] = _clientsTable;

            return ans;
        }

        int curRow = 3;

        void RemoveRow(object sender, EventArgs e)
        {
            TextBoxRow senderTextBox = (TextBoxRow)sender;
            var index = PhoneRows.IndexOf(senderTextBox);

            _clientsTable.SuspendLayout();

            PhoneRows[index].RemoveFromTable(_clientsTable, true);

            _clientsTable.ResumeLayout();

            PhoneRows.RemoveAt(index);

            curRow--;
        }

        void AddRow(object sender, EventArgs e)
        {
            var phoneRow = new TextBoxRow("Номер телефона", true, null, TextProcessor.AllowableSymbols.Digits | TextProcessor.AllowableSymbols.PhoneSymbols);
            
            phoneRow.RemoveClickEvent += RemoveRow;

            PhoneRows.Add(phoneRow);

            _clientsTable.SuspendLayout();

            phoneRow.ApplyToTable(_clientsTable, curRow, true);

            _clientsTable.ResumeLayout();

            curRow++;
        }

        void CreateClient(object sender, EventArgs e)
        {
            var clientInfo = new DBClientInfo()
            {
                Name = new DBClientName()
                {
                    SubName = _subNameRow.Text,
                    Name = _nameRow.Text,
                    FatherName = _fatherNameRow.Text,
                    AgreeSubscription = _agreeSubscriptionRow.CheckedIndexes != 0,
                    PhoneNotification = _phoneNotificationRow.CheckedIndexes != 0
                },

                PhoneNumbers = (from x in PhoneRows select new DBPhoneNum() { Value = x.Text }).ToArray()
            };

            if (CreateNewClient(clientInfo))
            {
                Reset();
                CreatedEvent?.Invoke();
            }
        }

        public void Reset()
        {
            _clientsTable.SuspendLayout();

            while (PhoneRows.Count > 0)
            {
                RemoveRow(PhoneRows[0], null);
            }

            _clientsTable.ResumeLayout();

            _subNameRow.Reset();
            _nameRow.Reset();
            _fatherNameRow.Reset();
        }
    }
}