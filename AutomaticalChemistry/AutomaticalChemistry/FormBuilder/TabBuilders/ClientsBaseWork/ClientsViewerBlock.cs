﻿using System.Text;
using System.Windows.Forms;
using AutomaticalChemistry.DatabaseWork.HAPI;
using AutomaticalChemistry.FormBuilder.Tools;

namespace AutomaticalChemistry.FormBuilder.TabBuilders.ClientsBaseWork
{
    public class ClientsViewerBlock : IControlsBlock
    {
        public string Name { get { return _name; } set { _name = value; } }

        private TableLayoutPanel _clientsTable;

        string _name;

        public Control[] GetTabControls()
        {
            var ans = new Control[1];

            ClientsBaseUpdated();
            
            ans[0] = _clientsTable;

            MainLogic.MainLogic.ClientsDBUpdatedEvent -= ClientsBaseUpdated;
            MainLogic.MainLogic.ClientsDBUpdatedEvent += ClientsBaseUpdated;

            return ans;
        }

        void ClientsBaseUpdated()
        {
            if (_clientsTable == null)
                _clientsTable = new TableLayoutPanel();
            else
            {
                _clientsTable.SuspendLayout();

                _clientsTable.Controls.Clear();
                _clientsTable.ColumnCount = 0;
                _clientsTable.RowCount = 0;
                _clientsTable.ColumnStyles.Clear();
                _clientsTable.RowStyles.Clear();

                _clientsTable.ResumeLayout();
            }

            _clientsTable.SuspendLayout();

            _clientsTable.Dock = DockStyle.Top;
            _clientsTable.Name = "clientsTable";
            _clientsTable.AutoSize = true;
            _clientsTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;

            TableLayoutPanelTools.InsertColumn(_clientsTable.ColumnCount, _clientsTable);
            TableLayoutPanelTools.InsertColumn(_clientsTable.ColumnCount, _clientsTable);
            TableLayoutPanelTools.InsertColumn(_clientsTable.ColumnCount, _clientsTable);

            _clientsTable.ColumnStyles[_clientsTable.ColumnCount - 2].Width = 50;
            _clientsTable.ColumnStyles[_clientsTable.ColumnCount - 2].SizeType = SizeType.Percent;

            TableLayoutPanelTools.InsertRow(_clientsTable.RowCount, _clientsTable);

            _clientsTable.Controls.Add(LayoutTools.GetLabel("Ф.И.О.", DockStyle.Top, HorizontalAlignment.Center), 0, 0);
            _clientsTable.Controls.Add(LayoutTools.GetLabel("Номер телефона", DockStyle.Top, HorizontalAlignment.Center), 1, 0);
            _clientsTable.Controls.Add(LayoutTools.GetLabel("Рассылка/Оповещания", DockStyle.Top, HorizontalAlignment.Center), 2, 0);

            var clients = DBController.GetAllClientInfo(false);
            foreach (var client in clients)
            {
                TableLayoutPanelTools.InsertRow(_clientsTable.RowCount, _clientsTable);

                _clientsTable.Controls.Add(LayoutTools.GetLabel(client.Name.ToString(), DockStyle.Top, HorizontalAlignment.Left), 0, _clientsTable.RowCount-1);
                StringBuilder nums = new StringBuilder();

                foreach (var num in client.PhoneNumbers)
                {
                    nums.Append(num.Value);
                    nums.Append("\r\n");
                }

                _clientsTable.Controls.Add(LayoutTools.GetLabel(nums.ToString(), DockStyle.Top, HorizontalAlignment.Left), 1, _clientsTable.RowCount - 1);
                _clientsTable.Controls.Add(LayoutTools.GetLabel((client.Name.AgreeSubscription ? "Да/" : "Нет/")+ (client.Name.PhoneNotification ? "Да" : "Нет"), DockStyle.Top, HorizontalAlignment.Left), 2, _clientsTable.RowCount - 1);
            }

            _clientsTable.ResumeLayout();
        }

        public void Reset()
        {
            
        }
    }
}
