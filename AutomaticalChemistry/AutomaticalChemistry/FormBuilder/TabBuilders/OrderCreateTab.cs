﻿using AutomaticalChemistry.FormBuilder.TabBuilders.OrdersCreateWork;
using AutomaticalChemistry.FormBuilder.Tools;
using AutomaticalChemistry.FormBuilder.Tools.Print;
using AutomaticalChemistry.FormBuilder.Tools.PrintCheck;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutomaticalChemistry.FormBuilder.TabBuilders
{
    public class OrderCreateTab : IControlsBlock
    {
        public string Name { get { return _name; } set { _name = value; } }

        string _name;

        private IControlsBlock _headerBlock;
        private IControlsBlock _mainConfigBlock;
        //private IControlsBlock _fullPriceBlock;
        private IControlsBlock _footBlock;
        private IControlsBlock[] _controlBlocks;

        public OrderCreateTab(string name)
        {
            _name = name;
        }

        public Control[] GetTabControls()
        {
            Init();
            
            _controlBlocks = new IControlsBlock[] { _footBlock/*, _fullPriceBlock*/, _mainConfigBlock, _headerBlock };

            Control[][] blocksControls = new Control[_controlBlocks.Length][];

            int length = 0;
            for (int i = 0; i < _controlBlocks.Length; i++)
            {
                blocksControls[i] = _controlBlocks[i].GetTabControls();
                length += blocksControls[i].Length;
            }
            
            var ans = new Control[length];

            var ansCounter = 0;

            for (int i = 0; i < blocksControls.Length; i++)
            {
                foreach (var control in blocksControls[i])
                {
                    ans[ansCounter] = control;
                    ansCounter++;
                }
            }

            //((MainConfigBlock)_mainConfigBlock).GarantyUpdateEvent += GarantyUpdate;
            //((MainConfigBlock)_mainConfigBlock).FullPriceUpdateEvent += FullPriceUpdated;

            ((FootBlock)_footBlock).CreateOrderEvent += CreateOrder;
            ((FootBlock)_footBlock).PrintOrderEvent += OnPrintOrder;
            ((FootBlock)_footBlock).PrintOrderCheckEvent += OnPrintOrderCheck;

            return ans;
        }

        /*void GarantyUpdate()
        {
            ((FullPriceBlock)_fullPriceBlock).UpdateGaranty(((MainConfigBlock)_mainConfigBlock).Garanty);
        }

        void FullPriceUpdated()
        {
            ((FullPriceBlock)_fullPriceBlock).UpdatePrice(((MainConfigBlock)_mainConfigBlock).FullPrice);
        }*/

        void CreateOrder()
        {
            if (MainLogic.MainLogic.CreateNewOrder(((MainConfigBlock)_mainConfigBlock).GetOrder(), false)){
                Reset();
            }
        }

        void OnPrintOrder()
        {
            var order = ((MainConfigBlock)_mainConfigBlock).GetOrder();
            if (MainLogic.MainLogic.CreateNewOrder(order, true))
            {
                PrintOrder.Print(order);
            }
        }

        void OnPrintOrderCheck()
        {
            var order = ((MainConfigBlock)_mainConfigBlock).GetOrder();
            if (MainLogic.MainLogic.CreateNewOrder(order, true))
            {
                CheckPrinter.PrintCheck(order);
            }
        }

        void Init()
        {
            if (_headerBlock == null)
                _headerBlock = new HeaderBlock();
            if (_mainConfigBlock == null)
                _mainConfigBlock = new MainConfigBlock();
            //if (_fullPriceBlock == null)
                //_fullPriceBlock = new FullPriceBlock();
            if (_footBlock == null)
                _footBlock = new FootBlock();
        }

        public void Reset()
        {
            if (_controlBlocks == null)
                return;

            for (int i = 0; i < _controlBlocks.Length; i++)
            {
                _controlBlocks[i].Reset();
            }
        }
    }
}
