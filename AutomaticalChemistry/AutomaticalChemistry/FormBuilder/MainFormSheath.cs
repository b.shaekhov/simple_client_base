﻿using System.Windows.Forms;
using System.Drawing;
using AutomaticalChemistry.FormBuilder.TabBuilders;
using System;
using Microsoft.Win32;
using System.IO;
using System.Security.Principal;
using AutomaticalChemistry.FormBuilder.Tools;

namespace AutomaticalChemistry.FormBuilder
{
    public class MainFormSheath
    {
        IControlsBlock[] _tabPages = new IControlsBlock[]
        {
            new ClientsBaseTab("Клиентская база"),
            new OrderCreateTab("Новый заказ"),
            new OrdersViewTab("Список заказов"),
            new SendSMSTab("Рассылка СМС")
        };

        public Form Form { get { return _form; } }
        private Form _form;

        private TabControl _tabsController;

        /*private bool Protect()
        {
            WindowsPrincipal pricipal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            bool hasAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator);

            if (!hasAdministrativeRight)
            {
                ShowInfoForUser.Warning("Запустите от имени администратора!");
                return false;
            }

            if (DateTime.Now > new DateTime(2018, 4, 25))
            {
                Registry.LocalMachine.OpenSubKey("Software", true).CreateSubKey("WinProgs", RegistryKeyPermissionCheck.ReadWriteSubTree).SetValue("Block", 1);
            }

            if (Registry.LocalMachine.OpenSubKey("Software", true).OpenSubKey("WinProgs") != null)
            {
                ShowInfoForUser.Warning("Заплатите разработчику!");
                return false;
            }
            return true;
        }*/

        public bool Init()
        {
            //ShowInfoForUser.Warning("Включи защиту");
            //if (!Protect())
                //return false;
            
            if (!Directory.Exists("Resources"))
                Directory.CreateDirectory("Resources");

            if (!Directory.Exists("Temp"))
                Directory.CreateDirectory("Temp");

            _form = new MainForm();
            
            _form.SuspendLayout();
            
            _form.AutoScaleMode = AutoScaleMode.Font;
            _form.ClientSize = new Size(640, 480);
            _form.Name = "Химчистка";
            _form.Text = "Химчистка";

            _form.AutoScroll = true;

            _tabsController = new TabControl();
            _tabsController.SuspendLayout();

            _tabsController.Dock = DockStyle.Fill;
            _tabsController.Location = new Point(0, 0);
            _tabsController.Name = "MainTabsControl";
            
            _tabsController.SelectedIndexChanged += (s, e) => _tabsController.TabPages[_tabsController.SelectedIndex].Focus();
            _form.Activated += (s, e) => _tabsController.TabPages[_tabsController.SelectedIndex].Focus();

            foreach (var tab in _tabPages)
            {
                var curPage = new TabPage();
                
                curPage.SuspendLayout();

                curPage.AutoScroll = true;

                curPage.Name = tab.Name+"Page";
                curPage.Text = tab.Name;
                curPage.UseVisualStyleBackColor = true;
                
                curPage.Controls.AddRange(tab.GetTabControls());

                _tabsController.TabPages.Add(curPage);
               
                curPage.ResumeLayout();
            }
            
            _form.Controls.Add(_tabsController);

            _tabsController.ResumeLayout();
            
            _form.ResumeLayout();

            return true;
        }
    }
}