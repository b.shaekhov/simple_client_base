﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutomaticalChemistry.FormBuilder.Tools
{
    public static class TableLayoutPanelTools
    {
        public static void InsertRow(int rowNum, TableLayoutPanel table)
        {
            if (rowNum > table.RowCount || rowNum < 0)
                throw new IndexOutOfRangeException();

            table.RowCount++;
            table.RowStyles.Add(new RowStyle(SizeType.AutoSize));

            for (int i = table.RowCount-1; i > rowNum; i--)
            {
                for (int j = 0; j < table.ColumnCount; j++)
                {
                    var control = table.GetControlFromPosition(j, i - 1);

                    if (control != null)
                    {
                        table.Controls.Remove(control);
                        table.Controls.Add(control, j, i);
                    }
                }
            }
        }

        public static void InsertColumn(int columnNum, TableLayoutPanel table)
        {
            if (columnNum > table.ColumnCount || columnNum < 0)
                throw new IndexOutOfRangeException();

            table.ColumnCount++;
            table.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));

            for (int i = table.ColumnCount - 1; i > columnNum; i--)
            {
                for (int j = 0; j < table.RowCount; j++)
                {
                    var control = table.GetControlFromPosition(i - 1, j);

                    if (control != null)
                    {
                        table.Controls.Remove(control);
                        table.Controls.Add(control, i, j);
                    }
                }
            }
        }

        public static void RemoveRow(int rowNum, TableLayoutPanel table)
        {
            if (rowNum >= table.RowCount || rowNum < 0)
                throw new IndexOutOfRangeException();
            
            for (int i = rowNum; i < table.RowCount - 1; i++)
            {
                for (int j = 0; j < table.ColumnCount; j++)
                {
                    var newControl = table.GetControlFromPosition(j, i + 1);
                    var curControl = table.GetControlFromPosition(j, i);

                    if (curControl != null)
                        table.Controls.Remove(curControl);
                    if (newControl != null)
                        table.Controls.Add(newControl, j, i);
                }
            }

            table.RowCount--;
            table.RowStyles.RemoveAt(table.RowStyles.Count-1);
        }

        public static void RemoveColumn(int columnNum, TableLayoutPanel table)
        {
            if (columnNum >= table.ColumnCount || columnNum < 0)
                throw new IndexOutOfRangeException();
            
            for (int i = columnNum; i < table.ColumnCount - 1; i++)
            {
                for (int j = 0; j < table.RowCount; j++)
                {
                    var newControl = table.GetControlFromPosition(i + 1, j);
                    var curControl = table.GetControlFromPosition(i, j);

                    if (curControl != null)
                        table.Controls.Remove(curControl);
                    if (newControl != null)
                        table.Controls.Add(newControl, i, j);
                }
            }

            table.ColumnCount--;
            table.ColumnStyles.RemoveAt(table.ColumnStyles.Count - 1);
        }
    }
}
