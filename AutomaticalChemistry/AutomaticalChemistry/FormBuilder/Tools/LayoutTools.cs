﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace AutomaticalChemistry.FormBuilder.Tools
{
    public static class LayoutTools
    {
        public static NumericUpDown GetNumericUpDown()
        {
            var intCounter = new NumericUpDown();

            intCounter.AutoSize = true;
            intCounter.Name = "numericUpDown";
            intCounter.TabStop = true;

            return intCounter;
        }

        public static RadioButton GetRadioButton(string text)
        {
            var radioButton = new RadioButton();

            radioButton.AutoSize = true;
            radioButton.Name = text+"radioButton";
            radioButton.TabStop = true;
            radioButton.Text = text;
            radioButton.UseVisualStyleBackColor = true;

            return radioButton;
        }

        public static CheckBox GetCheckBox(string text)
        {
            var checkBox = new CheckBox();

            checkBox.AutoSize = true;
            checkBox.Name = text + "checkBox";
            checkBox.TabStop = true;
            checkBox.Text = text;
            checkBox.UseVisualStyleBackColor = true;

            return checkBox;
        }

        public static Button GetButton(string text)
        {
            var button = new Button();
            button.AutoSize = true;
            button.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            button.Name = text + "Button";
            button.Text = text;
            return button;
        }

        public static Label GetLabel(string text, DockStyle dockStyle, HorizontalAlignment textAlign)
        {
            var label = new Label();

            label.Text = text;
            
            label.Margin = new Padding(3, 3, 3, 3);
            label.TextAlign = (ContentAlignment)Enum.Parse(typeof(ContentAlignment), "Top" + textAlign.ToString());
            label.Dock = dockStyle;
            label.AutoSize = true;
            label.Name = text + "Label";
            label.Text = text;
            
            return label;
        }

        public static DateTimePicker GetDateTimePicker()
        {
            var picker = new DateTimePicker();
            
            picker.Name = "datePicker";

            return picker;
        }

        public static ComboBox GetComboBox()
        {
            var comboBox = new ComboBox();

            comboBox.FormattingEnabled = true;
            comboBox.Name = "comboBox";

            comboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            comboBox.AutoCompleteSource = AutoCompleteSource.ListItems;

            return comboBox;
        }

        public static TextBox GetTextBox(int width)
        {
            var textBox = new TextBox();

            textBox.MaxLength = 300;
            textBox.Size = new Size(width, textBox.Size.Height);
            textBox.Name = "textBox";

            return textBox;
        }
    }
}
