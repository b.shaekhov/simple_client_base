﻿using Newtonsoft.Json;
using System.IO;

namespace AutomaticalChemistry.FormBuilder.Tools
{
    public static class ConfigsControl
    {
        const string ConfigsPath = "Configs/ChemistryConfig.txt";
        public static ConfigsContainer ReadedConfigs;

        static ConfigsControl()
        {
            CheckOrCreateConfigs();
        }

        private static void CheckOrCreateConfigs()
        {
            var dir = Path.GetDirectoryName(ConfigsPath);

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            if (File.Exists(ConfigsPath))
                ReadedConfigs = JsonConvert.DeserializeObject<ConfigsContainer>(File.ReadAllText(ConfigsPath));
            else
            {
                ReadedConfigs = new ConfigsContainer();
                File.WriteAllText(ConfigsPath, JsonConvert.SerializeObject(ReadedConfigs, Formatting.Indented));
            }
        }

        public class ConfigsContainer
        {
            [JsonProperty(PropertyName = "Имя_программы_смс_расылателя")]
            public string SMSSenderName = "SMSSender.exe";
            [JsonProperty(PropertyName = "Имя_программы_драйвера_устройства_печати_чеков")]
            public string CheckPrinterName = "AtolDriver.exe";
        }
    }
}
