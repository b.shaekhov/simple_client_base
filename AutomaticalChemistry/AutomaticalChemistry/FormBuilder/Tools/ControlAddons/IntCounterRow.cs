﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static AutomaticalChemistry.MainLogic.MainLogic;

namespace AutomaticalChemistry.FormBuilder.Tools.ControlAddons
{
    public class IntCounterRow : IRowControl
    {
        public int Value { get { return (int)_intCounter.Value;  } set { _intCounter.Value = value; } }

        NumericUpDown _intCounter;

        Control _label;

        Control _secondComponent;

        public event EventHandler RemoveClickEvent;
        public event EmptyD ChangeValueEvent;

        public IntCounterRow(string rowName, bool removable, string addonText)
        {
            _intCounter = LayoutTools.GetNumericUpDown();
            _intCounter.ValueChanged += ValueChanged;

            _label = LayoutTools.GetLabel(rowName, DockStyle.None, HorizontalAlignment.Left);

            _secondComponent = _intCounter;
            if (removable || !string.IsNullOrEmpty(addonText))
            {
                _secondComponent = new TableLayoutPanel();

                var table = ((TableLayoutPanel)_secondComponent);

                table.AutoSize = true;

                table.Margin = Padding.Empty;

                table.RowCount = 1;
                table.RowStyles.Add(new RowStyle(SizeType.AutoSize));

                table.ColumnCount = 1;
                table.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));

                table.Controls.Add(_intCounter, 0, 0);

                if (removable)
                {
                    TableLayoutPanelTools.InsertColumn(table.ColumnCount, table);

                    var but = LayoutTools.GetButton("Удалить");
                    but.Click += Remove;

                    table.Controls.Add(but, table.ColumnCount - 1, 0);
                }

                if (!string.IsNullOrEmpty(addonText))
                {
                    TableLayoutPanelTools.InsertColumn(table.ColumnCount, table);

                    var label = LayoutTools.GetLabel(addonText, DockStyle.None, HorizontalAlignment.Left);

                    table.Controls.Add(label, table.ColumnCount - 1, 0);
                }
            }
        }

        void ValueChanged(object sender, EventArgs e)
        {
            ChangeValueEvent?.Invoke();
        }

        void Remove(object sender, EventArgs e)
        {
            RemoveClickEvent?.Invoke(this, null);
        }

        public void ApplyToTable(TableLayoutPanel table, int row, bool insert)
        {
            if (insert)
                TableLayoutPanelTools.InsertRow(row, table);

            table.Controls.Add(_label, 0, row);
            table.Controls.Add(_secondComponent, 1, row);
        }

        public void RemoveFromTable(TableLayoutPanel table, bool removeRow)
        {
            var rowNum = table.GetRow(_label);

            table.Controls.Remove(_label);
            table.Controls.Remove(_secondComponent);

            if (removeRow)
            {
                TableLayoutPanelTools.RemoveRow(rowNum, table);
            }
        }

        public void Reset()
        {
            _intCounter.Value = 0;
            //_infoLabel.Text = "";
        }
    }
}
