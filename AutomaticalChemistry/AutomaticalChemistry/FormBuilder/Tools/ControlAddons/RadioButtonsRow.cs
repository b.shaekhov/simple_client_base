﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static AutomaticalChemistry.MainLogic.MainLogic;

namespace AutomaticalChemistry.FormBuilder.Tools.ControlAddons
{
    public class RadioButtonsRow : IRowControl
    {
        //public DateTimePicker picker;
        public TableLayoutPanel backTable;
        
        public int CheckedIndex {
            get
            {
                int ans = -1;
                for (int i =0; i < buttons.Length; i++)
                {
                    if (buttons[i].Checked)
                    {
                        ans = i;
                        break;
                    }
                }
                return ans;
            } }

        Control _label;
        RadioButton[] buttons;

        public event EmptyD CheckChangedEvent;

        public RadioButtonsRow(string rowName, string[] names, int columns)
        {
            _label = LayoutTools.GetLabel(rowName, DockStyle.None, HorizontalAlignment.Left);

            backTable = new TableLayoutPanel();

            backTable.Dock = DockStyle.Top;
            backTable.Name = "radioButtonsTable";
            backTable.AutoSize = true;

            backTable.ColumnCount = columns;
            
            for (int i = 0; i < columns; i++)
            {
                backTable.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            }
            
            backTable.RowCount = (int)(names.Length/columns)+(names.Length% columns>0?1:0);

            for (int i = 0; i < backTable.RowCount; i++)
            {
                backTable.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            }

            buttons = new RadioButton[names.Length];

            for (int i = 0; i < names.Length; i++)
            {
                buttons[i] = LayoutTools.GetRadioButton(names[i]);
                buttons[i].CheckedChanged += CheckChanged;
                backTable.Controls.Add(buttons[i], i% columns, i/ columns);
            }

            if (buttons.Length > 0)
                buttons[0].Checked = true;
        }

        void CheckChanged(object sender, EventArgs e)
        {
            CheckChangedEvent?.Invoke();
        }

        public void ApplyToTable(TableLayoutPanel table, int row, bool insert)
        {
            if (insert)
                TableLayoutPanelTools.InsertRow(row, table);

            table.Controls.Add(_label, 0, row);
            table.Controls.Add(backTable, 1, row);
        }

        public void RemoveFromTable(TableLayoutPanel table, bool removeRow)
        {
            var rowNum = table.GetRow(_label);

            table.Controls.Remove(_label);
            table.Controls.Remove(backTable);

            if (removeRow)
            {
                TableLayoutPanelTools.RemoveRow(rowNum, table);
            }
        }
        
        public void Reset()
        {
            foreach (var but in buttons)
            {
                but.Checked = false;
            }

            if (buttons.Length > 0)
                buttons[0].Checked = true;
        }
    }
}
