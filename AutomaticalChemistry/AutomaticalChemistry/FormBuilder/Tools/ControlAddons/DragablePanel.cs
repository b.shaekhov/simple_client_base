﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace AutomaticalChemistry.FormBuilder.Tools.ControlAddons
{
    public class DragablePanel : IControlsBlock
    {
        public Control ControlsPanel { get { return _controlsPanel; } }

        public string Name { get { return _name; } set { _name = value; } }
        private string _name;

        private Panel _mainPanel;
        private Panel _controlsPanel;

        private bool _dragging;
        private Point _startMousePos;
        
        public Control[] GetTabControls()
        {
            if (_mainPanel == null)
            {
                var moveBut = LayoutTools.GetButton("+");
                moveBut.Margin = Padding.Empty;
                moveBut.MouseDown += StartDrag;
                moveBut.MouseUp += EndDrag;
                moveBut.MouseMove += OnDrag;
                
                _controlsPanel = new Panel();
                _controlsPanel.Margin = Padding.Empty;
                _controlsPanel.AutoSize = true;
                _controlsPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                _controlsPanel.Location = new Point(moveBut.PreferredSize.Width, 0);

                _mainPanel = new Panel();
                _mainPanel.AutoSize = true;
                _mainPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                _mainPanel.Controls.Add(moveBut);
                _mainPanel.Controls.Add(_controlsPanel);
            }

            return new Control[] { _mainPanel };
        }

        void StartDrag(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _dragging = true;
                _startMousePos = e.Location;
            }
        }

        void OnDrag(object sender, MouseEventArgs e)
        {
            if (_dragging && e.Button == MouseButtons.Left)
            {
                _mainPanel.Location = new Point(_mainPanel.Location.X + e.X - _startMousePos.X, _mainPanel.Location.Y + e.Y - _startMousePos.Y);
                _mainPanel.TopLevelControl.Refresh();
            }
        }

        void EndDrag(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _dragging = false;
            }
        }
        
        public void Reset()
        {
            
        }
    }
}
