﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using static AutomaticalChemistry.MainLogic.MainLogic;

namespace AutomaticalChemistry.FormBuilder.Tools.ControlAddons
{
    public class LabelRow : IRowControl
    {
        public string InfoLabelText { get { return _infoLabel.Text; } }

        public void ChangeInfoLabelValue(string newValue)
        {
            //var label = LayoutTools.GetLabel(newValue, _infoLabel.Dock, _infoLabel.TextAlign);
            _infoLabel.Text = newValue;
            //_infoLabel.Size = label.Size;
        }

        Control _infoLabel;

        Control _label;

        Control _secondComponent;

        public event EventHandler RemoveClickEvent;

        public LabelRow(string rowName, string value, bool removable, string addonText)
        {
            _infoLabel = LayoutTools.GetLabel(value, DockStyle.None, HorizontalAlignment.Left);
            _label = LayoutTools.GetLabel(rowName, DockStyle.None, HorizontalAlignment.Left);

            _secondComponent = _infoLabel;
            if (removable || !string.IsNullOrEmpty(addonText))
            {
                _secondComponent = new TableLayoutPanel();

                var table = ((TableLayoutPanel)_secondComponent);

                table.AutoSize = true;

                table.Margin = Padding.Empty;

                table.RowCount = 1;
                table.RowStyles.Add(new RowStyle(SizeType.AutoSize));

                table.ColumnCount = 1;
                table.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));

                table.Controls.Add(_infoLabel, 0, 0);

                if (removable)
                {
                    TableLayoutPanelTools.InsertColumn(table.ColumnCount, table);

                    var but = LayoutTools.GetButton("Удалить");
                    but.Click += Remove;

                    table.Controls.Add(but, table.ColumnCount - 1, 0);
                }

                if (!string.IsNullOrEmpty(addonText))
                {
                    TableLayoutPanelTools.InsertColumn(table.ColumnCount, table);

                    var label = LayoutTools.GetLabel(addonText, DockStyle.None, HorizontalAlignment.Left);

                    table.Controls.Add(label, table.ColumnCount - 1, 0);
                }
            }
        }

        void Remove(object sender, EventArgs e)
        {
            RemoveClickEvent?.Invoke(this, null);
        }

        public void ApplyToTable(TableLayoutPanel table, int row, bool insert)
        {
            if (insert)
                TableLayoutPanelTools.InsertRow(row, table);

            table.Controls.Add(_label, 0, row);
            table.Controls.Add(_secondComponent, 1, row);
        }

        public void RemoveFromTable(TableLayoutPanel table, bool removeRow)
        {
            var rowNum = table.GetRow(_label);

            table.Controls.Remove(_label);
            table.Controls.Remove(_secondComponent);

            if (removeRow)
            {
                TableLayoutPanelTools.RemoveRow(rowNum, table);
            }
        }

        public void Reset()
        {
            _infoLabel.Text = "";
        }
    }
}
