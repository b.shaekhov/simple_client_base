﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static AutomaticalChemistry.MainLogic.MainLogic;

namespace AutomaticalChemistry.FormBuilder.Tools.ControlAddons
{
    public class ChoiceListRow : IRowControl
    {
        public ComboBox comboBox;

        Control _label;

        Control _secondComponent;

        public event EventHandler RemoveClickEvent;

        public ChoiceListRow(string rowName, string[] list, bool removable, string addonText, bool emptyMargin = false)
        {
            _label = LayoutTools.GetLabel(rowName, DockStyle.None, HorizontalAlignment.Left);

            comboBox = LayoutTools.GetComboBox();
            if (list != null)
                comboBox.Items.AddRange(list);
            
            _secondComponent = comboBox;
            if (removable || !string.IsNullOrEmpty(addonText))
            {
                _secondComponent = new TableLayoutPanel();

                var table = ((TableLayoutPanel)_secondComponent);

                table.AutoSize = true;

                table.Margin = Padding.Empty;

                table.RowCount = 1;
                table.RowStyles.Add(new RowStyle(SizeType.AutoSize));

                table.ColumnCount = 1;
                table.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));

                table.Controls.Add(comboBox, 0, 0);

                if (removable)
                {
                    TableLayoutPanelTools.InsertColumn(table.ColumnCount, table);

                    var but = LayoutTools.GetButton("Удалить");
                    but.Click += Remove;

                    if (emptyMargin)
                        but.Margin = Padding.Empty;

                    table.Controls.Add(but, table.ColumnCount-1, 0);
                }
                
                if (!string.IsNullOrEmpty(addonText))
                {
                    TableLayoutPanelTools.InsertColumn(table.ColumnCount, table);

                    var label = LayoutTools.GetLabel(addonText, DockStyle.None, HorizontalAlignment.Left);
                    if (emptyMargin)
                        label.Margin = Padding.Empty;

                    table.Controls.Add(label, table.ColumnCount - 1, 0);
                }
            }

            if (emptyMargin)
            {
                _label.Margin = Padding.Empty;
                comboBox.Margin = Padding.Empty;
            }
        }

        void Remove(object sender, EventArgs e)
        {
            RemoveClickEvent?.Invoke(this, null);
        }

        public void ApplyToTable(TableLayoutPanel table, int row, bool insert)
        {
            if (insert)
                TableLayoutPanelTools.InsertRow(row, table);

            table.Controls.Add(_label, 0, row);
            table.Controls.Add(_secondComponent, 1, row);
        }

        public void RemoveFromTable(TableLayoutPanel table, bool removeRow)
        {
            var rowNum = table.GetRow(_label);

            table.Controls.Remove(_label);
            table.Controls.Remove(_secondComponent);

            if (removeRow)
            {
                TableLayoutPanelTools.RemoveRow(rowNum, table);
            }
        }

        public void Reset()
        {
            comboBox.ResetText();
        }
    }
}
