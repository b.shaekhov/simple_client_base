﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static AutomaticalChemistry.MainLogic.MainLogic;

namespace AutomaticalChemistry.FormBuilder.Tools.ControlAddons
{
    public class CheckBoxsRow : IRowControl
    {
        //public DateTimePicker picker;
        public TableLayoutPanel backTable;
        
        public int CheckedIndexes {
            get
            {
                int ans = 0;
                for (int i =0; i < boxes.Length; i++)
                {
                    ans |= ((boxes[i].Checked ? 1 : 0) << i);
                }
                return ans;
            }
            set
            {
                var val = value;

                foreach (var box in boxes)
                {
                    box.Checked = (val & 1) == 1;
                    val >>= 1;
                }
            }
        }

        Control _label;
        CheckBox[] boxes;

        public event EmptyD CheckChangedEvent;

        public CheckBoxsRow(string rowName, string[] names, int columns)
        {
            _label = LayoutTools.GetLabel(rowName, DockStyle.None, HorizontalAlignment.Left);

            backTable = new TableLayoutPanel();

            backTable.Dock = DockStyle.Top;
            backTable.Name = "radioButtonsTable";
            backTable.AutoSize = true;

            backTable.ColumnCount = columns;
            
            for (int i = 0; i < columns; i++)
            {
                backTable.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            }
            
            backTable.RowCount = (int)(names.Length/columns)+(names.Length% columns>0?1:0);

            for (int i = 0; i < backTable.RowCount; i++)
            {
                backTable.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            }

            boxes = new CheckBox[names.Length];

            for (int i = 0; i < names.Length; i++)
            {
                boxes[i] = LayoutTools.GetCheckBox(names[i]);
                boxes[i].CheckedChanged += CheckChanged;
                backTable.Controls.Add(boxes[i], i% columns, i/ columns);
            }
        }

        void CheckChanged(object sender, EventArgs e)
        {
            CheckChangedEvent?.Invoke();
        }

        public void ApplyToTable(TableLayoutPanel table, int row, bool insert)
        {
            if (insert)
                TableLayoutPanelTools.InsertRow(row, table);

            table.Controls.Add(_label, 0, row);
            table.Controls.Add(backTable, 1, row);
        }

        public void RemoveFromTable(TableLayoutPanel table, bool removeRow)
        {
            var rowNum = table.GetRow(_label);

            table.Controls.Remove(_label);
            table.Controls.Remove(backTable);

            if (removeRow)
            {
                TableLayoutPanelTools.RemoveRow(rowNum, table);
            }
        }
        
        public void Reset()
        {
            foreach (var but in boxes)
            {
                but.Checked = false;
            }
        }
    }
}
