﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static AutomaticalChemistry.MainLogic.MainLogic;
using static AutomaticalChemistry.FormBuilder.Tools.TextProcessor;

namespace AutomaticalChemistry.FormBuilder.Tools.ControlAddons
{
    public class TextBoxRow : IRowControl
    {
        public TextBox InternalTextBox { get { return _textBox; } }
        private TextBox _textBox;
        public string Text { get { return _textBox.Text; } set { _textBox.Text = value; } }

        public int MaxLength { get { return _maxLength; } set { _maxLength = value; _textBox.MaxLength = _maxLength + 10; } }
        private int _maxLength;

        Control _label;

        public AllowableSymbols MyAllowableSymbols;

        Control _secondComponent;

        public event EventHandler RemoveClickEvent;

        public event EmptyD NewTextCheckedEvent;

        public TextBoxRow(string rowName, bool removable, string addonText, AllowableSymbols myAllowableSymbols, int width = 150, int maxLength = 300)
        {
            _label = LayoutTools.GetLabel(rowName, DockStyle.None, HorizontalAlignment.Left);

            MyAllowableSymbols = myAllowableSymbols;

            _textBox = LayoutTools.GetTextBox(width);
            MaxLength = maxLength;

            _textBox.TextChanged += TextBoxChanged;
            

            _secondComponent = _textBox;
            if (removable || !string.IsNullOrEmpty(addonText))
            {
                _secondComponent = new TableLayoutPanel();

                var table = ((TableLayoutPanel)_secondComponent);

                table.AutoSize = true;

                table.Margin = Padding.Empty;

                table.RowCount = 1;
                table.RowStyles.Add(new RowStyle(SizeType.AutoSize));

                table.ColumnCount = 1;
                table.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));

                table.Controls.Add(_textBox, 0, 0);

                if (removable)
                {
                    TableLayoutPanelTools.InsertColumn(table.ColumnCount, table);

                    var but = LayoutTools.GetButton("Удалить");
                    but.Click += Remove;

                    table.Controls.Add(but, table.ColumnCount - 1, 0);
                }

                if (!string.IsNullOrEmpty(addonText))
                {
                    TableLayoutPanelTools.InsertColumn(table.ColumnCount, table);

                    var label = LayoutTools.GetLabel(addonText, DockStyle.None, HorizontalAlignment.Left);

                    table.Controls.Add(label, table.ColumnCount - 1, 0);
                }
            }
        }

        void Remove(object sender, EventArgs e)
        {
            RemoveClickEvent?.Invoke(this, null);
        }

        bool disabler = false;
        void TextBoxChanged(object sender, EventArgs e)
        {
            if (disabler)
                return;

            disabler = true;

            string t = _textBox.Text;
            NormaliseText(ref t, MyAllowableSymbols);
            if (t.Length > MaxLength)
                t = t.Substring(0, MaxLength);
            var tmp = _textBox.SelectionStart;
            tmp = Math.Max(0, tmp - (_textBox.Text.Length - t.Length));
            _textBox.Text = t;
            _textBox.SelectionStart = tmp;

            disabler = false;

            NewTextCheckedEvent?.Invoke();
        }

        public void ApplyToTable(TableLayoutPanel table, int row, bool insert)
        {
            if (insert)
                TableLayoutPanelTools.InsertRow(row, table);

            table.Controls.Add(_label, 0, row);
            table.Controls.Add(_secondComponent, 1, row);
        }

        public void RemoveFromTable(TableLayoutPanel table, bool removeRow)
        {
            var rowNum = table.GetRow(_label);

            table.Controls.Remove(_label);
            table.Controls.Remove(_secondComponent);

            if (removeRow)
            {
                TableLayoutPanelTools.RemoveRow(rowNum, table);
            }
        }

        public void Reset()
        {
            Text = "";
        }
    }
}
