﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutomaticalChemistry.FormBuilder.Tools.ControlAddons
{
    public interface IRowControl
    {
        //string LabelText { get; set; }

        void ApplyToTable(TableLayoutPanel table, int row, bool insert);
        void RemoveFromTable(TableLayoutPanel table, bool removeRow);

        void Reset();
    }
}
