﻿using AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static AutomaticalChemistry.MainLogic.MainLogic;

namespace AutomaticalChemistry.FormBuilder.Tools.ControlAddons
{
    public class FullPriceRow : IRowControl
    {
        Control _wholePartViewer;
        Control _fractionalPartViewer;
        //Label _wholePartName;
        //Label _fractionalPartName;
        TableLayoutPanel _priceViewTable;

        public Price ViewPrice { get { return _viewPrice; } set { _viewPrice = value; CheckState(); } }

        Price _viewPrice;

        Control _label;

        public event EmptyD PriceChangedEvent;

        public FullPriceRow(string rowName, bool changable)
        {
            _viewPrice = Price.Zero;
            _label = LayoutTools.GetLabel(rowName, DockStyle.None, HorizontalAlignment.Left);

            if (changable)
            {
                _wholePartViewer = LayoutTools.GetTextBox(70);
                _fractionalPartViewer = LayoutTools.GetTextBox(70);

                _wholePartViewer.Text = "0";
                _fractionalPartViewer.Text = "0";

                ((TextBox)_wholePartViewer).TextChanged += TextChanged;
                ((TextBox)_fractionalPartViewer).TextChanged += TextChanged;
            }
            else
            {
                _wholePartViewer = LayoutTools.GetLabel("", DockStyle.None, HorizontalAlignment.Left);
                _fractionalPartViewer = LayoutTools.GetLabel("", DockStyle.None, HorizontalAlignment.Left);
            }

            var wholePartName = LayoutTools.GetLabel(Price.WholePartName, DockStyle.None, HorizontalAlignment.Left);
            var fractionalPartName = LayoutTools.GetLabel(Price.FractionalPartName, DockStyle.None, HorizontalAlignment.Left);

            _priceViewTable = new TableLayoutPanel();

            _priceViewTable.Name = "fullPriceRowTable";
            _priceViewTable.AutoSize = true;
            _priceViewTable.AutoSizeMode = AutoSizeMode.GrowAndShrink;

            //_priceViewTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;

            _priceViewTable.ColumnCount = 4;
            _priceViewTable.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            _priceViewTable.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            _priceViewTable.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            _priceViewTable.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));

            _priceViewTable.RowCount = 1;
            _priceViewTable.RowStyles.Add(new RowStyle(SizeType.AutoSize));

            _priceViewTable.Controls.Add(_wholePartViewer, 0, 0);
            _priceViewTable.Controls.Add(wholePartName, 1, 0);
            _priceViewTable.Controls.Add(_fractionalPartViewer, 2, 0);
            _priceViewTable.Controls.Add(fractionalPartName, 3, 0);

            
            CheckState();
        }

        bool disabler;
        void TextChanged(object sender, EventArgs e)
        {
            if (disabler)
                return;

            disabler = true;

            string t = _wholePartViewer.Text;
            TextProcessor.NormaliseText(ref t, TextProcessor.AllowableSymbols.Digits);
            
            if (t.Length > 9)
                t = t.Substring(0, 9);
            var tmp = ((TextBox)_wholePartViewer).SelectionStart;
            tmp = Math.Max(0, tmp - (_wholePartViewer.Text.Length - t.Length));
            _wholePartViewer.Text = t;
            ((TextBox)_wholePartViewer).SelectionStart = tmp;

            t = _fractionalPartViewer.Text;
            TextProcessor.NormaliseText(ref t, TextProcessor.AllowableSymbols.Digits);
            if (t.Length > 2)
                t = t.Substring(0, 2);
            tmp = ((TextBox)_fractionalPartViewer).SelectionStart;
            tmp = Math.Max(0, tmp - (_fractionalPartViewer.Text.Length - t.Length));
            _fractionalPartViewer.Text = t;
            ((TextBox)_fractionalPartViewer).SelectionStart = tmp;

            disabler = false;

            _viewPrice.PriceWholePart = int.Parse(_wholePartViewer.Text == "" ? "0" : _wholePartViewer.Text);
            _viewPrice.PriceFractionalPart = int.Parse(_fractionalPartViewer.Text == "" ? "0" : _fractionalPartViewer.Text);

            PriceChangedEvent?.Invoke();
        }

        public void ApplyToTable(TableLayoutPanel table, int row, bool insert)
        {
            if (insert)
                TableLayoutPanelTools.InsertRow(row, table);

            table.Controls.Add(_label, 0, row);
            table.Controls.Add(_priceViewTable, 1, row);
        }

        public void RemoveFromTable(TableLayoutPanel table, bool removeRow)
        {
            var rowNum = table.GetRow(_label);

            table.Controls.Remove(_label);
            table.Controls.Remove(_priceViewTable);

            if (removeRow)
            {
                TableLayoutPanelTools.RemoveRow(rowNum, table);
            }
        }
        
        public void Reset()
        {
            if (_wholePartViewer is TextBox)
            {
                ((TextBox)_wholePartViewer).TextChanged -= TextChanged;
                ((TextBox)_fractionalPartViewer).TextChanged -= TextChanged;
            }

            ViewPrice = Price.Zero;

            if (_wholePartViewer is TextBox)
            {
                ((TextBox)_wholePartViewer).TextChanged += TextChanged;
                ((TextBox)_fractionalPartViewer).TextChanged += TextChanged;
            }
        }

        void CheckState()
        {   
            _wholePartViewer.Text = _viewPrice.PriceWholePart.ToString();
            _fractionalPartViewer.Text = _viewPrice.PriceFractionalPart.ToString();
        }
    }
}
