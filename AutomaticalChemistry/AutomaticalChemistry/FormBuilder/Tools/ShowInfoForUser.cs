﻿using System.Windows.Forms;

namespace AutomaticalChemistry.FormBuilder.Tools
{
    public static class ShowInfoForUser
    {
        public static void Error(object message)
        {
            MessageBox.Show(message.ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void Warning(object message)
        {
            MessageBox.Show(message.ToString(), "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void Message(object message)
        {
            MessageBox.Show(message.ToString(), "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static bool Question(object message)
        {
            return MessageBox.Show(message.ToString(), "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
        } 
    }
}
