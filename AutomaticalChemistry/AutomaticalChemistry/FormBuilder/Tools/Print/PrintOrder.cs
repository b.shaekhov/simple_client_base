﻿using AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo;
using System;
using System.IO;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Diagnostics;

namespace AutomaticalChemistry.FormBuilder.Tools.Print
{
    public static class PrintOrder
    {
        public static void Print(DBOrder order)
        {
            _currentOrder = order;

            string printFilePath = @"Temp\orderPrint.png";

            var printIm = BuildPrintImage();
            printIm.Save(printFilePath, ImageFormat.Png);

            var vinDir = Environment.GetEnvironmentVariable("WINDIR");

            bool normal = true;
            
            #region StandartWinImageView
            var comand = vinDir + @"\System32\rundll32.exe";
            var args = vinDir + @"\System32\shimgvw.dll,ImageView_Fullscreen " + Application.StartupPath + @"\" +printFilePath;

            try
            {
                Process.Start(comand, args);
            } catch (Exception e)
            {
                ShowInfoForUser.Error("Не удалось запустить стандартное средство просмотра изображений!\r\n" +
                                e.Message + "\r\n" +
                                "Попытка запуска команды:\r\n" +
                                comand + " " + args);
                normal = false;
            }
            #endregion

            //_currentOrder = null;

            if (normal)
                return;

            normal = true;

            //_currentOrder = order;

            #region TryOpenImage
            try
            {
                var proc = new Process();
                proc.StartInfo.FileName = printFilePath;
                proc.StartInfo.UseShellExecute = true;
                proc.Start();
            }
            catch (Exception e)
            {
                ShowInfoForUser.Error("Не удалось запустить ассоциированное средство просмотра изображений!\r\n" +
                                e.Message + "\r\n" +
                                "Попытка запуска файла:\r\n" +
                                printFilePath);
                normal = false;
            }
            #endregion

            //_currentOrder = null;

            if (normal)
                return;

            //_currentOrder = order;

            ManualPrinting.Print(printIm);
            
            //_currentOrder = null;
        }

        static DBOrder _currentOrder;
        

        static Bitmap BuildPrintImage()
        {
            var printBaseImage = GetPrintBaseImage();
            var graphics = Graphics.FromImage(printBaseImage);
            
            var font = new Font("Arial", 14);
            var brush = Brushes.Black;
           
            var maskImage = (Bitmap)GetPrintBaseImageMask();
            var readedPoints = PrintMaskWork.FindPointsInMask(maskImage);

            Point[] maskPoints = new Point[17];

            if (maskPoints.Length > readedPoints.Length)
                ShowInfoForUser.Warning("Проверьте изображение маски!\r\nНе хватает точек!\r\nТочек должно быть не менее "+ maskPoints.Length);

            for (int i = 0; i < maskPoints.Length; i++)
            {
                if (i < readedPoints.Length)
                    maskPoints[i] = readedPoints[i];
                else
                    maskPoints[i] = new Point(maskPoints[i > 0 ? i-1 : 0].X, maskPoints[i > 0 ? i - 1 : 0].Y+50);
            }

            int counter = 0; //max 17
            graphics.DrawString(_currentOrder.MainInfo.Id.ToString()                              , font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;
            graphics.DrawString(_currentOrder.MainInfo.StartDate.ToString("dd.MM.yyyy")           , font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;
            graphics.DrawString(_currentOrder.MainInfo.EndDate  .ToString("dd.MM.yyyy")           , font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;
            graphics.DrawString(_currentOrder.MainInfo.ClientInfo.Name.ToString()                 , font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;
            graphics.DrawString(_currentOrder.MainInfo.ClientInfo.BuildStringFromPhoneNums()      , font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;
            graphics.DrawString(_currentOrder.MainInfo.ObjectName                                 , font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;
            
            graphics.DrawString(DBAdditionalOrderInfo.ObjectTypeTexts       [_currentOrder.AdditionalInfo.ObjectType          ], font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;
            graphics.DrawString(DBAdditionalOrderInfo.BuildDecodedTexts(_currentOrder.AdditionalInfo.DirtValue   , DBAdditionalOrderInfo.   DirtValueTexts), font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;
            graphics.DrawString(DBAdditionalOrderInfo.WearValueTexts        [_currentOrder.AdditionalInfo.WearValue           ], font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;
            graphics.DrawString(DBAdditionalOrderInfo.BuildDecodedTexts(_currentOrder.AdditionalInfo.CoatingState, DBAdditionalOrderInfo.CoatingStateTexts), font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;
            graphics.DrawString(DBAdditionalOrderInfo.ClayOrDetailsTexts    [_currentOrder.AdditionalInfo.ClayAndDetailsExist ], font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;
            graphics.DrawString(DBAdditionalOrderInfo.DifColorTexts         [_currentOrder.AdditionalInfo.DifColor            ], font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;
            graphics.DrawString(DBAdditionalOrderInfo.MarkExistTexts        [_currentOrder.AdditionalInfo.MarkExist           ], font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;

            graphics.DrawString(_currentOrder.BuildStringFromSpots(), font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;

            graphics.DrawString(DBAdditionalOrderInfo.PayTypeTexts[_currentOrder.AdditionalInfo.PayType], font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;
            graphics.DrawString(_currentOrder.AdditionalInfo.PrePayPercent + "%", font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;
            graphics.DrawString(_currentOrder.FullPrice.ToString()                                                  , font, brush, new PointF(maskPoints[counter].X, maskPoints[counter].Y)); counter++;
            
            return (Bitmap)printBaseImage;
        }

        #region GetImageFile
        public static Image GetPrintBaseImage()
        {
            return GetAnyImage("PrintBaseImage.*");
        }

        public static Image GetPrintBaseImageMask()
        {
            return GetAnyImage("PrintBaseImageMask.*");
        }

        static Image GetAnyImage(string filter)
        {
            var files = Directory.GetFiles(Application.StartupPath + @"\Resources", filter, SearchOption.TopDirectoryOnly);

            if (files.Length > 1)
            {
                ShowInfoForUser.Warning("Найдено больше 1 "+ filter + " файла!");
            }

            if (files.Length < 1)
            {
                ShowInfoForUser.Warning("Не найден " + filter + " файл!");
                return new Bitmap(1, 1);
            }

            var image = Image.FromFile(files[0]);

            return image;
        }
        #endregion
    }
}
