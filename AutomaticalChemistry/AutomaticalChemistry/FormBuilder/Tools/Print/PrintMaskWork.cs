﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.FormBuilder.Tools.Print
{
    public static class PrintMaskWork
    {
        public static Point[] FindPointsInMask(Bitmap bitmap)
        {
            int step = 3;

            Queue<Point> ans = new Queue<Point>();

            Stack<Point> currentPoints = new Stack<Point>();
            Stack<byte> statePoints = new Stack<byte>();

            for (int y = 0; y < bitmap.Height; y += step)
            {
                for (int x = 0; x < bitmap.Width; x += step)
                {
                    bool ansPointReq = false;
                    var ansPoint = new Point(x, y);

                    currentPoints.Push(new Point(x, y));
                    statePoints.Push(0);

                    while (currentPoints.Count > 0)
                    {
                        var curPoint = currentPoints.Peek();

                        var color = bitmap.GetPixel(curPoint.X, curPoint.Y);

                        if (statePoints.Peek() < 4 && (statePoints.Peek() > 0 || color.R < 128 || color.G < 128 || color.B < 128))
                        {
                            if (statePoints.Peek() == 0)
                            {
                                if (!ansPointReq || (curPoint.X + curPoint.Y) < (ansPoint.X + ansPoint.Y))
                                {
                                    ansPoint.X = curPoint.X;
                                    ansPoint.Y = curPoint.Y;
                                    ansPointReq = true;
                                }

                                bitmap.SetPixel(curPoint.X, curPoint.Y, Color.White);

                                var state = statePoints.Pop();
                                state++;
                                statePoints.Push(state);

                                if (curPoint.X + step < bitmap.Width)
                                {
                                    currentPoints.Push(new Point(curPoint.X + step, curPoint.Y));
                                    statePoints.Push(0);
                                }

                                continue;
                            }

                            if (statePoints.Peek() == 1)
                            {
                                var state = statePoints.Pop();
                                state++;
                                statePoints.Push(state);

                                if (curPoint.Y + step < bitmap.Height)
                                {
                                    currentPoints.Push(new Point(curPoint.X, curPoint.Y + step));
                                    statePoints.Push(0);
                                }

                                continue;
                            }

                            if (statePoints.Peek() == 2)
                            {
                                var state = statePoints.Pop();
                                state++;
                                statePoints.Push(state);

                                if (curPoint.Y - step > 0)
                                {
                                    currentPoints.Push(new Point(curPoint.X, curPoint.Y - step));
                                    statePoints.Push(0);
                                }

                                continue;
                            }

                            if (statePoints.Peek() == 3)
                            {
                                var state = statePoints.Pop();
                                state++;
                                statePoints.Push(state);

                                if (curPoint.X - step > 0)
                                {
                                    currentPoints.Push(new Point(curPoint.X - step, curPoint.Y));
                                    statePoints.Push(0);
                                }

                                continue;
                            }
                        }
                        else
                        {
                            currentPoints.Pop();
                            statePoints.Pop();
                        }
                    }

                    if (ansPointReq)
                        ans.Enqueue(ansPoint);
                    ansPointReq = false;
                }
            }

            return ans.ToArray();
        }
    }
}
