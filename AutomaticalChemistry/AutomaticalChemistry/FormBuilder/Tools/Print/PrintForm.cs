﻿using AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo;
using AutomaticalChemistry.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutomaticalChemistry.FormBuilder.Tools.Print
{
    public class PrintForm : Form
    {
        static PrintForm _instance;

        public PrintForm()
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        }

        public static void ShowForm(DBOrder order)
        {
            _instance = new PrintForm();
            _instance.AutoScroll = true;
            _instance.FormBorderStyle = FormBorderStyle.SizableToolWindow;

            _instance.Icon = Resources.Program;

            var printer = new PrintDialogBuilder(order);
            
            printer.EndPrintEvent += PrintOrderEnd;

            _instance.Controls.AddRange(printer.GetTabControls());

            _instance.Size = new System.Drawing.Size(_instance.PreferredSize.Width+50, 480);

            _instance.ShowDialog();
            _instance = null;
        }

        static void PrintOrderEnd()
        {
            _instance.Close();
        }
    }
}
