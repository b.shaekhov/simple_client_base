﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutomaticalChemistry.FormBuilder.Tools.Print
{
    public static class ManualPrinting
    {
        static Bitmap _printImage = null;

        public static void Print(Bitmap image)
        {
            _printImage = image;

            var printDocument = new PrintDocument();

            //printDocument.DefaultPageSettings.Landscape = true;
            printDocument.PrintPage += OnPrintPage;

            var printDialog = new PrintDialog();

            printDialog.AllowSomePages = true;
            printDialog.ShowHelp = true;
            printDialog.Document = printDocument;
            printDialog.UseEXDialog = true;
            var ans = printDialog.ShowDialog();

            if (ans == DialogResult.OK)
            {
                printDocument.Print();
            }
            else
            {
                var prevDialog = new PrintPreviewDialog();
                prevDialog.Name = "printPreviewDialog";
                prevDialog.Document = printDocument;

                prevDialog.ShowDialog();
            }

            _printImage = null;
        }


        static void OnPrintPage(object sender, PrintPageEventArgs e)
        {
            var printableArea = GetPrintableArea(e.PageSettings.PrintableArea, e.PageSettings.Landscape);
            var printImage = _printImage;

            var coef = printableArea.Width / (float)printImage.Width;

            if (printImage.Height * coef > printableArea.Height)
            {
                coef = printableArea.Height / (float)printImage.Height;
            }

            e.Graphics.DrawImage(printImage, new RectangleF(printableArea.X, printableArea.Y, printImage.Width * coef, printImage.Height * coef));
        }
        
        static Rectangle GetPrintableArea(RectangleF printableArea, bool landscape)
        {
            if (landscape && printableArea.Height > printableArea.Width)
            {
                var tmp = printableArea.X;
                printableArea.X = printableArea.Y;
                printableArea.Y = tmp;

                tmp = printableArea.Width;
                printableArea.Width = printableArea.Height;
                printableArea.Height = tmp;
            }

            return new Rectangle((int)printableArea.X, (int)printableArea.Y, (int)printableArea.Width, (int)printableArea.Height);
        }
    }
}
