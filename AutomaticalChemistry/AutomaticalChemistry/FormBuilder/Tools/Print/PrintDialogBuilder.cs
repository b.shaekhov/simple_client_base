﻿using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo;
using AutomaticalChemistry.FormBuilder.Tools.ControlAddons;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static AutomaticalChemistry.MainLogic.MainLogic;

namespace AutomaticalChemistry.FormBuilder.Tools.Print
{
    public class PrintDialogBuilder : IControlsBlock
    {
        public string Name { get { return _name; } set { _name = value; } }
        private string _name;

        public EmptyD EndPrintEvent;

        private DBOrder _currentOrder;
        
        private Panel _mainPanel;

        int _clientItemsCount;
        int _orderMainInfoItemsCount;
        int _orderAddonItemsCount;
        string[] _fullOrderInfoComboBoxItems;

        private List<DragablePanel> _dragablePanels = new List<DragablePanel>();
        private List<ChoiceListRow> _paramBoxes = new List<ChoiceListRow>();

        public PrintDialogBuilder(DBOrder order)
        {
            _currentOrder = order;
        }
        
        public Control[] GetTabControls()
        {
            if (_mainPanel == null)
            {
                var clientComboBoxItems = DBClientName.ClientVarsNames.Union(new string[] { "Номера телефонов" }).ToArray();
                var mainInfoComboBoxItems = DBMainOrderInfo.OrderMainInfoVarsNames.Except(new string[] { DBMainOrderInfo.OrderMainInfoVarsNames[DBMainOrderInfo.OrderMainInfoVarsNames.Length - 1] }).ToArray();
                var addonInfoComboBoxItems = DBAdditionalOrderInfo.OrderAdditionalInfoVarsNames.Union(new string[] { "Пятна" }).ToArray();

                _fullOrderInfoComboBoxItems = clientComboBoxItems.Union(mainInfoComboBoxItems.Union(addonInfoComboBoxItems)).ToArray();

                _clientItemsCount = clientComboBoxItems.Length;
                _orderMainInfoItemsCount = mainInfoComboBoxItems.Length;
                _orderAddonItemsCount = addonInfoComboBoxItems.Length;
                
                _mainPanel = new Panel();
                _mainPanel.AutoSize = true;
                _mainPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;

                var image = new PictureBox();

                var maxWidth = 700;

                var rawImage = PrintOrder.GetPrintBaseImage();

                var coef = maxWidth / (float)rawImage.Width;
                
                var imSize = rawImage.Size;

                image.SizeMode = PictureBoxSizeMode.StretchImage;
                image.Image = rawImage;

                image.Size = new Size((int)(imSize.Width * coef), (int)(imSize.Height * coef));

                _mainPanel.Controls.Add(image);

                var controlTable = new TableLayoutPanel();

                controlTable.AutoSize = true;
                controlTable.AutoSizeMode = AutoSizeMode.GrowAndShrink;

                controlTable.Location = new Point(image.Size.Width, 0);

                TableLayoutPanelTools.InsertColumn(controlTable.ColumnCount, controlTable);
                
                var addButton = LayoutTools.GetButton("Добавить параметр");
                addButton.Click += AddControl;
                TableLayoutPanelTools.InsertRow(controlTable.RowCount, controlTable);
                controlTable.Controls.Add(addButton, 0, controlTable.RowCount-1);

                //var addButton = LayoutTools.GetButton("Добавить параметр");
                //addButton.Click += AddControl;
                //TableLayoutPanelTools.InsertRow(controlTable.RowCount, controlTable);
                //controlTable.Controls.Add(addButton, 0, controlTable.RowCount - 1);

                _mainPanel.Controls.Add(controlTable);
            }

            return new Control[] { _mainPanel };
        }

        void AddControl(object sender, EventArgs e)
        {
            var control = new DragablePanel();
            var controlObjects = control.GetTabControls();

            var paramViewBuilder = new TableLayoutPanel();
            paramViewBuilder.Margin = Padding.Empty;
            paramViewBuilder.AutoSize = true;
            paramViewBuilder.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            TableLayoutPanelTools.InsertColumn(paramViewBuilder.ColumnCount, paramViewBuilder);
            TableLayoutPanelTools.InsertColumn(paramViewBuilder.ColumnCount, paramViewBuilder);
            TableLayoutPanelTools.InsertRow(paramViewBuilder.RowCount, paramViewBuilder);
            control.ControlsPanel.Controls.Add(paramViewBuilder);

            var paramBox = new ChoiceListRow("", _fullOrderInfoComboBoxItems, true, null, true);
            paramBox.RemoveClickEvent += RemoveParam;

            paramBox.ApplyToTable(paramViewBuilder, paramViewBuilder.RowCount-1, false);

            _dragablePanels.Add(control);
            _paramBoxes.Add(paramBox);
            
            var curPanelControls = new Control[_mainPanel.Controls.Count];
            _mainPanel.Controls.CopyTo(curPanelControls, 0);
            var panelControls = controlObjects.Union(curPanelControls).ToArray();
            _mainPanel.Controls.Clear();
            _mainPanel.Controls.AddRange(panelControls);
        }

        void RemoveParam(object sender, EventArgs e)
        {
            var row = (ChoiceListRow)sender;

            var removeIndex = _paramBoxes.IndexOf(row);

            var controlObjects = _dragablePanels[removeIndex].GetTabControls();

            foreach (var remControl in controlObjects)
            {
                _mainPanel.Controls.Remove(remControl);
            }

            _dragablePanels.RemoveAt(removeIndex);
            _paramBoxes.RemoveAt(removeIndex);
        }

        public void Reset()
        {
            
        }
    }
}
