﻿using AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo;
using System.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System;

namespace AutomaticalChemistry.FormBuilder.Tools.PrintCheck
{
    public static class CheckPrinter
    {
        private const string DriverSourceFilePath = "Temp/AtolDriverSource.txt";

        public static void PrintCheck(DBOrder order)
        {
            if (order.Spots == null || order.Spots.Length < 1)
            {
                ShowInfoForUser.Message("У данного заказа пятен нет!");
                return;
            }

            SellItem[] items = (from x in order.Spots select new SellItem() { count = 1, name = x.Description, price = (x.Price.PriceWholePart + x.Price.PriceFractionalPart / 100f)}).ToArray();

            var sendData = JsonConvert.SerializeObject(items);

            var dir = Path.GetDirectoryName(DriverSourceFilePath);

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            File.WriteAllText(DriverSourceFilePath, sendData);
            
            try
            {
                var senderWork = Process.Start(Application.StartupPath + "\\" + ConfigsControl.ReadedConfigs.CheckPrinterName);
            }
            catch (Exception ex)
            {
                ShowInfoForUser.Error("Не удалось запустить программу-драйвер!\r\n" + ex.Message);
            }
        }
    }
}
