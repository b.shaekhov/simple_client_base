﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.FormBuilder.Tools
{
    public static class TextProcessor
    {
        const string Liters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZабвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
        const string Digits = "0123456789";
        const string Brackets = "()<>[]{}";
        const string PhoneSymbols = "()+-";
        const string TextSymbols = "+-.?,.^$%#;@\"\\/&!~=_*| ''";

        [Flags]
        public enum AllowableSymbols
        {
            None,
            Liters,
            Digits,
            Brackets,
            PhoneSymbols,
            TextSymbols
        }

        public static bool CheckText(string text, AllowableSymbols reqSymbols)
        {
            return FindBadIndex(text, 0, text.Length - 1, GetSymbols(reqSymbols)) < 0;
        }

        public static bool NormaliseText(ref string text, AllowableSymbols reqSymbols)
        {
            bool changed = false;

            var symbols = GetSymbols(reqSymbols);

            int startIndex = FindBadIndex(text, 0, text.Length - 1, symbols);
            while (startIndex > -1)
            {
                text = text.Remove(startIndex, 1);
                startIndex = FindBadIndex(text, startIndex, text.Length - 1, symbols);
                changed = true;
            }

            return changed;
        }

        static string[] GetSymbols(AllowableSymbols reqSymbols)
        {
            var ans = new string[5];

            if ((reqSymbols & AllowableSymbols.Liters) == AllowableSymbols.Liters)
                ans[0] = Liters;

            if ((reqSymbols & AllowableSymbols.Digits) == AllowableSymbols.Digits)
                ans[1] = Digits;

            if ((reqSymbols & AllowableSymbols.Brackets) == AllowableSymbols.Brackets)
                ans[2] = Brackets;

            if ((reqSymbols & AllowableSymbols.PhoneSymbols) == AllowableSymbols.PhoneSymbols)
                ans[3] = PhoneSymbols;

            if ((reqSymbols & AllowableSymbols.TextSymbols) == AllowableSymbols.TextSymbols)
                ans[4] = TextSymbols;

            return ans;
        }

        static int FindBadIndex(string text, int startIndex, int endIndex, string[] allowableSymbols)
        {
            for (int i = startIndex; i <= endIndex; i++)
            {
                for (int j = 0; j < allowableSymbols.Length; j++)
                {
                    if (allowableSymbols[j] == null)
                        continue;

                    for (int jj = 0; jj < allowableSymbols[j].Length; jj++)
                    {
                        if (text[i] == allowableSymbols[j][jj])
                            goto flag;
                    }
                }

                return i;

                flag:;
            }

            return -1;
        }
    }
}
