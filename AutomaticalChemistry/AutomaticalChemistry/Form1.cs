﻿using AutomaticalChemistry.Properties;
using System.Windows.Forms;

namespace AutomaticalChemistry
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            Icon = Resources.Program;
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            //InitializeComponent();
        }
    }
}
