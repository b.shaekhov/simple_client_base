﻿using AutomaticalChemistry.FormBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace AutomaticalChemistry
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (MainLogic.MainLogic.Init())
                Application.Run(MainLogic.MainLogic.MainForm.Form);
        }
    }
}
