﻿using AutomaticalChemistry.DatabaseWork.HAPI;
using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo;
using AutomaticalChemistry.FormBuilder;
using AutomaticalChemistry.FormBuilder.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutomaticalChemistry.MainLogic
{
    public static class MainLogic
    {
        public static MainFormSheath MainForm { get; private set; }

        public delegate void EmptyD();
        public static event EmptyD ClientsDBUpdatedEvent;
        public static event EmptyD OrdersDBUpdatedEvent;

        public static bool Init()
        {
            DBController.Init();
            MainForm = new MainFormSheath();
            var ans = MainForm.Init();
            if (ans)
                MainForm.Form.FormClosing += Close;
            return ans;
        }

        public enum CreateClientErrors
        {
            None,
            EmptyNumber,
            EmptyName,
            DublicateNumbers,
            NumberAlreadyExist
        }
        
        public enum CreateOrderErrors
        {
            None,
            ClientInfoIsNull,
            StartDateLessToday,
            EndDateLessToday,
            EndDateLessStartDate, 
            ObjectNameIsEmpty,
            SpotNameIsEmpty,
            OrderPriceIsZero,
            DirtIsZero,
            CoatingStateIsZero
        }

        public static bool CreateNewClient(DBClientInfo clientInfo)
        {
            List<CreateClientErrors> ignoreErrors = new List<CreateClientErrors>();

            while (true)
            {
                var error = TryCreateNewClient(clientInfo, ignoreErrors, out string errorText);

                if (error == CreateClientErrors.None)
                    return true;

                if (error == CreateClientErrors.EmptyName)
                {
                    var result = ShowInfoForUser.Question(errorText + "\r\nПродолжить?");

                    if (result)
                    {
                        ignoreErrors.Add(error);
                        continue;
                    }
                    break;
                }
                else
                {
                    ShowInfoForUser.Error("Не удалось создать клиента:\r\n" + errorText);
                    break;
                }
            }

            return false;
        }
        
        static CreateClientErrors TryCreateNewClient(DBClientInfo clientInfo,IList<CreateClientErrors> ignoreErrors, out string errorMessage)
        {
            var error = CheckNewClientData(clientInfo, ignoreErrors, out errorMessage, -1);
            if (error != CreateClientErrors.None)
                return error;

            DBController.CreateAbsolutlyNewClientInfo(clientInfo);

            ClientsDBUpdatedEvent?.Invoke();

            return CreateClientErrors.None;
        }

        static CreateClientErrors CheckNewClientData(DBClientInfo clientInfo, IList<CreateClientErrors> ignoreErrors, out string errorMessage, int ignoreId)
        {
            if (ignoreErrors == null)
                ignoreErrors = new CreateClientErrors[0];

            if (!ignoreErrors.Contains(CreateClientErrors.EmptyNumber))
            {
                int counter = 0;
                foreach (var number in clientInfo.PhoneNumbers)
                {
                    if (string.IsNullOrEmpty(number.Value))
                    {
                        errorMessage = "Пустой " + counter + " номер!";
                        return CreateClientErrors.EmptyNumber;
                    }
                    counter++;
                }
            }

            if (!ignoreErrors.Contains(CreateClientErrors.EmptyName))
            {
                if (string.IsNullOrEmpty(clientInfo.Name.SubName))
                {
                    errorMessage = "Пустая строка фамилии!";
                    return CreateClientErrors.EmptyName;
                }

                if (string.IsNullOrEmpty(clientInfo.Name.Name))
                {
                    errorMessage = "Пустая строка имени!";
                    return CreateClientErrors.EmptyName;
                }

                if (string.IsNullOrEmpty(clientInfo.Name.FatherName))
                {
                    errorMessage = "Пустая строка отчества!";
                    return CreateClientErrors.EmptyName;
                }
            }

            if (!ignoreErrors.Contains(CreateClientErrors.DublicateNumbers))
            {
                for (int i = 0; i < clientInfo.PhoneNumbers.Length; i++)
                {
                    for (int j = i+1; j < clientInfo.PhoneNumbers.Length; j++)
                    {
                        if (clientInfo.PhoneNumbers[i].Value == clientInfo.PhoneNumbers[j].Value)
                        {
                            errorMessage = "Веедено 2 одинаковых номера!\r\nНомер " + i + "\r\nНомер " + j;
                            return CreateClientErrors.DublicateNumbers;
                        }
                    }
                }
            }

            if (!ignoreErrors.Contains(CreateClientErrors.NumberAlreadyExist))
            {
                DBPhoneNum[] nums;
                if (ignoreErrors.Contains(CreateClientErrors.EmptyNumber))
                    nums = (from x in clientInfo.PhoneNumbers where !string.IsNullOrEmpty(x.Value) select x).ToArray();
                else
                    nums = clientInfo.PhoneNumbers;

                var dbNumsCount = DBController.SetInfoForPhoneNums(nums);

                if (dbNumsCount > 0)
                {
                    var dbNums = (from x in clientInfo.PhoneNumbers where !x.CheckNewNum() select x).ToArray();

                    errorMessage = "Некоторые номера уже есть в базе!";
                    foreach (var dbNum in dbNums)
                    {
                        var id = Array.IndexOf(nums, dbNum);
                        errorMessage += "\r\nНомер " + nums[id].Value;
                    }
                    return CreateClientErrors.NumberAlreadyExist;
                }
            }
            
            errorMessage = null;
            return CreateClientErrors.None;
        }

        public static bool ChangeClient(DBClientInfo oldInfo, DBClientInfo newInfo)
        {
            List<CreateClientErrors> ignoreErrors = new List<CreateClientErrors>();

            while (true)
            {
                DBController.RemoveClientFromId(oldInfo.Name.Id);

                var error = CheckNewClientData(newInfo, ignoreErrors, out string errorText, oldInfo.Name.Id);

                if (error == CreateClientErrors.None)
                {
                    newInfo.Name.Id = oldInfo.Name.Id;

                    for (int i = 0; i < newInfo.PhoneNumbers.Length; i++)
                    {
                        newInfo.PhoneNumbers[i].Owner = newInfo.Name.Id;
                    }

                    DBController.CreateClientToId(newInfo);
                    ClientsDBUpdatedEvent?.Invoke();
                    return true;
                } else
                {
                    DBController.CreateClientToId(oldInfo);
                }

                if (error == CreateClientErrors.EmptyName)
                {
                    var result = ShowInfoForUser.Question(errorText + "\r\nПродолжить?");

                    if (result)
                    {
                        ignoreErrors.Add(error);
                        continue;
                    }
                    break;
                }
                else
                {
                    ShowInfoForUser.Error("Не удалось создать клиента:\r\n" + errorText);
                    break;
                }
            }

            return false;
        }
        
        public static bool CreateNewOrder(DBOrder order, bool checkOnly)
        {
            List<CreateOrderErrors> ignoreErrors = new List<CreateOrderErrors>();
            
            while (true)
            {
                var error = TryCreateNewOrder(order, ignoreErrors, out string errorText, checkOnly);

                if (error == CreateOrderErrors.None)
                    return true;

                if (error == CreateOrderErrors.EndDateLessToday ||
                    error == CreateOrderErrors.OrderPriceIsZero ||
                    error == CreateOrderErrors.StartDateLessToday)
                {
                    var result = ShowInfoForUser.Question(errorText + "\r\nПродолжить?");

                    if (result)
                    {
                        ignoreErrors.Add(error);
                        continue;
                    }
                    break;
                }
                else
                {
                    ShowInfoForUser.Error("Не удалось создать заказ:\r\n" + error + "\r\n" + errorText);
                    break;
                }
            }

            return false;
        }

        public static void UpdateExecuteOrder(DBOrder order)
        {
            DBController.UpdateExecuteOrder(order);
            OrdersDBUpdatedEvent?.Invoke();
        }

        public static void DeleteOrder(DBOrder order)
        {
            DBController.DeleteOrder(order);
            OrdersDBUpdatedEvent?.Invoke();
        }

        static CreateOrderErrors TryCreateNewOrder(DBOrder order, IList<CreateOrderErrors> ignoreErrors, out string errorMessage, bool checkOnly)
        {
            var error = CheckNewOrderData(order, ignoreErrors, out errorMessage);
            if (error != CreateOrderErrors.None)
                return error;

            if (!checkOnly)
            {
                DBController.CreateNewOrder(order);

                OrdersDBUpdatedEvent?.Invoke();
            }

            return CreateOrderErrors.None;
        }

        static CreateOrderErrors CheckNewOrderData(DBOrder order, IList<CreateOrderErrors> ignoreErrors, out string errorMessage)
        {
            errorMessage = null;

            if (ignoreErrors == null)
                ignoreErrors = new CreateOrderErrors[0];

            if (order.MainInfo.ClientInfo == null)
            {
                errorMessage = "Не выбрана информация о клиенте!";
                return CreateOrderErrors.ClientInfoIsNull;
            }

            if (!ignoreErrors.Contains(CreateOrderErrors.StartDateLessToday))
            {
                if (order.MainInfo.StartDate < DateTime.Today)
                {
                    errorMessage = "Вы пытаетесь открыть заказ задним числом!";
                    return CreateOrderErrors.StartDateLessToday;
                }
            }

            if (!ignoreErrors.Contains(CreateOrderErrors.EndDateLessToday))
            {
                if (order.MainInfo.EndDate < DateTime.Today)
                {
                    errorMessage = "Вы пытаетесь закрыть заказ задним числом!";
                    return CreateOrderErrors.EndDateLessToday;
                }
            }

            if (!ignoreErrors.Contains(CreateOrderErrors.EndDateLessStartDate))
            {
                if (order.MainInfo.EndDate < order.MainInfo.StartDate)
                {
                    errorMessage = "Вы пытаетесь закрыть заказ перед его открытием!";
                    return CreateOrderErrors.EndDateLessStartDate;
                }
            }

            if (!ignoreErrors.Contains(CreateOrderErrors.ObjectNameIsEmpty))
            {
                if (string.IsNullOrEmpty(order.MainInfo.ObjectName))
                {
                    errorMessage = "Наименование изделия пустое!";
                    return CreateOrderErrors.ObjectNameIsEmpty;
                }
            }

            if (!ignoreErrors.Contains(CreateOrderErrors.SpotNameIsEmpty))
            {
                StringBuilder erMessageBuilder = new StringBuilder();
                bool error = false;
                for (int i = 0; i < order.Spots.Length; i++)
                {
                    if (string.IsNullOrEmpty(order.Spots[i].Description))
                    {
                        erMessageBuilder.Append("Описание пятна "+i+" пустое!");
                        error = true;
                    }
                }

                if (error)
                {
                    errorMessage = erMessageBuilder.ToString();
                    return CreateOrderErrors.SpotNameIsEmpty;
                }
            }

            if (!ignoreErrors.Contains(CreateOrderErrors.OrderPriceIsZero))
            {
                if (order.FullPrice.PriceWholePart <= 0 && order.FullPrice.PriceFractionalPart <= 0)
                {
                    errorMessage = "Сумма заказа = 0!";
                    return CreateOrderErrors.OrderPriceIsZero;
                }
            }

            if (!ignoreErrors.Contains(CreateOrderErrors.DirtIsZero))
            {
                if (order.AdditionalInfo.DirtValue == 0)
                {
                    errorMessage = "Не установлена степень загрязнения!";
                    return CreateOrderErrors.DirtIsZero;
                }
            }

            if (!ignoreErrors.Contains(CreateOrderErrors.CoatingStateIsZero))
            {
                if (order.AdditionalInfo.CoatingState == 0)
                {
                    errorMessage = "Не установлены дефекты покрытия!";
                    return CreateOrderErrors.CoatingStateIsZero;
                }
            }

            return CreateOrderErrors.None;
        }

        public static bool TryRemoveClient(DBClientInfo info)
        {
            if (DBController.GetClientOrdersCount(info.Name.Id) > 0)
            {
                ShowInfoForUser.Error("Невозможно удалить! Клиент привязан к заказу!");
                return false;
            }

            var result = ShowInfoForUser.Question("Вы действительно хотите удалить запись о клиенте?\r\n" + info);

            if (result)
            {
                DBController.RemoveClientFromId(info.Name.Id);
                ClientsDBUpdatedEvent?.Invoke();
                return true;
            }
            return false;
        }

        static void Close(object sender, EventArgs e)
        {
            DBController.Close();
        }
    }
}
