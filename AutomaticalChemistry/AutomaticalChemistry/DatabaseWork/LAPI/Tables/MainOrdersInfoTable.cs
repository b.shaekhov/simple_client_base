﻿using AutomaticalChemistry.DatabaseWork.HAPI;
using AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo;
using System;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.DatabaseWork.LAPI.Tables
{
    public class MainOrdersInfoTable : IDBTable
    {
        //"Date" = yyyy-MM-dd

        const string _tableName = "MainOrdersInfo";
        string[] _columnNames = new string[] { "Id", "StartDate", "EndDate", "ClientId", "ObjectName", "Executed" };
        string[] _columnTypes = new string[] { "INTEGER PRIMARY KEY", "DATE", "DATE", "INTEGER", "TEXT", "BOOLEAN" };

        DBConnectionEstablisher _connectionEstablisher;

        public void Init(DBConnectionEstablisher connectionEstablisher)
        {
            StringBuilder comandBuilder = new StringBuilder("(");
            for (int i = 0; i < _columnNames.Length; i++)
            {
                if (i != 0)
                    comandBuilder.Append(", ");
                comandBuilder.Append(_columnNames[i]);
                comandBuilder.Append(" ");
                comandBuilder.Append(_columnTypes[i]);
            }
            comandBuilder.Append(")");

            _connectionEstablisher = connectionEstablisher;
            _connectionEstablisher.CheckOrCreateTable(_tableName, comandBuilder.ToString());
        }

        public void NewMainInfo(DBMainOrderInfo mainInfo)
        {
            _connectionEstablisher.RunNonQuerySQL(string.Format("INSERT INTO '{0}' ('{1}', '{3}', '{5}', '{7}', '{9}') VALUES ('{2}', '{4}', '{6}', '{8}', '{10}');", 
                                                                _tableName, 
                                                                _columnNames[1], 
                                                                mainInfo.DBStartDate, 
                                                                _columnNames[2], 
                                                                mainInfo.DBEndDate, 
                                                                _columnNames[3], 
                                                                mainInfo.ClientInfo.Name.Id, 
                                                                _columnNames[4], 
                                                                mainInfo.ObjectName,
                                                                _columnNames[5],
                                                                mainInfo.Executed ? "1" : "0"));

            var reader = _connectionEstablisher.RunQuerySQL("SELECT last_insert_rowid();");

            reader.Read();
            mainInfo.Id = int.Parse(reader.GetValue(0).ToString());
        }

        public int GetMaxId()
        {
            var reader = _connectionEstablisher.RunQuerySQL(string.Format("SELECT MAX({0}) FROM {1};", _columnNames[0], _tableName));
            reader.Read();
            var value = reader.GetValue(0).ToString();
            return int.Parse(string.IsNullOrEmpty(value) ? "0" : value);
        }

        public DBMainOrderInfo[] GetAllOrdersInfo()
        {
            var reader = _connectionEstablisher.RunQuerySQL(string.Format("SELECT * FROM {0};", _tableName));

            var allClientsInfo = DBController.GetAllClientInfo(true);

            return (from DbDataRecord record in reader select
                new DBMainOrderInfo()
                {
                    Id = int.Parse(record[_columnNames[0]].ToString()),
                    StartDate = DateTime.Parse(record[_columnNames[1]].ToString()),
                    EndDate = DateTime.Parse(record[_columnNames[2]].ToString()),

                    ClientInfo = allClientsInfo[int.Parse(record[_columnNames[3]].ToString())],
                    ObjectName = record[_columnNames[4]].ToString(),
                    Executed = (bool)record[_columnNames[5]]
                }
            ).ToArray();
        }

        public int GetClientOrdersCount(int id)
        {
            var com = string.Format("SELECT COUNT(*) FROM {0} WHERE {1}='{2}';", _tableName, _columnNames[3], id);
            var reader = _connectionEstablisher.RunQuerySQL(com);
            reader.Read();
            var value = reader.GetValue(0).ToString();
            return int.Parse(string.IsNullOrEmpty(value) ? "0" : value);
        }

        public void UpdateOrderInfo(DBMainOrderInfo mainInfo)
        {
            _connectionEstablisher.RunNonQuerySQL(string.Format("UPDATE '{0}' SET {3} = '{4}', {5} = '{6}', {7} = '{8}', {9} = '{10}', {11} = '{12}' WHERE {1} = '{2}';",
                                                                _tableName,
                                                                _columnNames[0],
                                                                mainInfo.Id,
                                                                _columnNames[1],
                                                                mainInfo.DBStartDate,
                                                                _columnNames[2],
                                                                mainInfo.DBEndDate,
                                                                _columnNames[3],
                                                                mainInfo.ClientInfo.Name.Id,
                                                                _columnNames[4],
                                                                mainInfo.ObjectName,
                                                                _columnNames[5],
                                                                mainInfo.Executed ? "1" : "0"));
        }

        public void DeleteInfo(DBMainOrderInfo mainInfo)
        {
            _connectionEstablisher.RunNonQuerySQL(string.Format("DELETE FROM '{0}' WHERE {1}='{2}';", _tableName, _columnNames[0], mainInfo.Id));
        }
    }
}
