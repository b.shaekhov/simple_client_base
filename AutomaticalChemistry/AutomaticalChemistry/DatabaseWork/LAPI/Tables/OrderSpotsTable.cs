﻿using AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.DatabaseWork.LAPI.Tables
{
    public class OrderSpotsTable : IDBTable
    {
        const string _tableName = "OrderSpots";
        string[] _columnNames = new string[] { "Id"                 , "OrderId", "Description", "PriceWholePart", "PriceFracPart"};
        string[] _columnTypes = new string[] { "INTEGER PRIMARY KEY", "INTEGER", "TEXT"       , "INTEGER"       , "INTEGER"      };

        DBConnectionEstablisher _connectionEstablisher;

        public void Init(DBConnectionEstablisher connectionEstablisher)
        {
            StringBuilder comandBuilder = new StringBuilder("(");
            for (int i = 0; i < _columnNames.Length; i++)
            {
                if (i != 0)
                    comandBuilder.Append(", ");
                comandBuilder.Append(_columnNames[i]);
                comandBuilder.Append(" ");
                comandBuilder.Append(_columnTypes[i]);
            }
            comandBuilder.Append(")");

            _connectionEstablisher = connectionEstablisher;
            _connectionEstablisher.CheckOrCreateTable(_tableName, comandBuilder.ToString());
        }

        //ВНИМАНИЕ Id тут Не устанавливается!
        public void WriteNewSpots(IList<DBOrderSpot> spots)
        {
            StringBuilder comand = new StringBuilder();
            foreach (var spot in spots)
            {
                comand.Append(string.Format("INSERT INTO '{0}' ('{1}', '{3}', '{5}', '{7}') VALUES ('{2}', '{4}', '{6}', '{8}');", 
                                                         _tableName, 
                                                         _columnNames[1],
                                                         spot.OrderId, 
                                                         _columnNames[2],
                                                         spot.Description,
                                                         _columnNames[3],
                                                         spot.Price.PriceWholePart,
                                                         _columnNames[4],
                                                         spot.Price.PriceFractionalPart
                                                         ));
            }

            _connectionEstablisher.RunNonQuerySQL(comand.ToString());
        }

        public void DeleteSpots(int orderId)
        {
            _connectionEstablisher.RunNonQuerySQL(string.Format("DELETE FROM '{0}' WHERE {1}='{2}';", _tableName, _columnNames[1], orderId));
        }

        public DBOrderSpot[] GetOrderIdSpotsInfo(int id)
        {
            return GetSpotsInfoFromComand(string.Format("SELECT * FROM '{0}' WHERE {1}={2};", _tableName, _columnNames[1], id));
        }

        public DBOrderSpot[] GetAllSpotsInfo()
        {
            return GetSpotsInfoFromComand(string.Format("SELECT * FROM '{0}';", _tableName));
        }

        DBOrderSpot[] GetSpotsInfoFromComand(string comand)
        {
            var reader = _connectionEstablisher.RunQuerySQL(comand);
            
            return (from DbDataRecord record in reader select 
                    new DBOrderSpot()
                    {
                        Id        = int.Parse(record[_columnNames[0]].ToString()),
                        OrderId   = int.Parse(record[_columnNames[1]].ToString()),
                        Description         = record[_columnNames[2]].ToString(),
                        Price = new Price()
                        {
                            PriceWholePart = int.Parse(record[_columnNames[3]].ToString()),
                            PriceFractionalPart = int.Parse(record[_columnNames[4]].ToString())
                        }
                    }
                    ).ToArray();
        }
    }
}
