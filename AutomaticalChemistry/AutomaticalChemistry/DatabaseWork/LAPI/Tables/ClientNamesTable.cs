﻿using AutomaticalChemistry.DatabaseWork.HAPI;
using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.DatabaseWork.LAPI.Tables
{
    public class ClientNamesTable : IDBTable
    {
        const string _tableName = "ClientNames";

        string[] _columnNames = new string[] { "Id", "SubName", "Name", "FatherName", "AgreeSubscription", "PhoneNotification" };
        string[] _columnTypes = new string[] { "INTEGER PRIMARY KEY", "Text", "Text", "Text", "BOOLEAN", "BOOLEAN" };

        DBConnectionEstablisher _connectionEstablisher;

        public void Init(DBConnectionEstablisher connectionEstablisher)
        {
            StringBuilder comandBuilder = new StringBuilder("(");
            for (int i = 0; i < _columnNames.Length; i++)
            {
                if (i != 0)
                    comandBuilder.Append(", ");
                comandBuilder.Append(_columnNames[i]);
                comandBuilder.Append(" ");
                comandBuilder.Append(_columnTypes[i]);
            }
            comandBuilder.Append(")");

            _connectionEstablisher = connectionEstablisher;
            _connectionEstablisher.CheckOrCreateTable(_tableName, comandBuilder.ToString());
        }

        public void CreateName(DBClientName name)
        {
            _connectionEstablisher.RunNonQuerySQL(string.Format("INSERT INTO '{0}' ('{1}', '{3}', '{5}', '{7}', '{9}') VALUES ('{2}', '{4}', '{6}', '{8}', '{10}');", _tableName, _columnNames[1], name.SubName, _columnNames[2], name.Name, _columnNames[3], name.FatherName, _columnNames[4], name.AgreeSubscription ? "1" : "0", _columnNames[5], name.PhoneNotification ? "1" : "0"));

            var reader = _connectionEstablisher.RunQuerySQL("SELECT last_insert_rowid();");
            
            reader.Read();
            name.Id = int.Parse(reader.GetValue(0).ToString());
            
        }

        public void CreateNameFromId(DBClientName name)
        {
            _connectionEstablisher.RunNonQuerySQL(string.Format("INSERT INTO '{0}' ('{1}', '{3}', '{5}', '{7}', '{9}', '{11}') VALUES ('{2}', '{4}', '{6}', '{8}', '{10}', '{12}');", 
                                                                _tableName,
                                                                _columnNames[0],
                                                                name.Id,
                                                                _columnNames[1], 
                                                                name.SubName, 
                                                                _columnNames[2], 
                                                                name.Name, 
                                                                _columnNames[3], 
                                                                name.FatherName, 
                                                                _columnNames[4], 
                                                                name.AgreeSubscription ? "1" : "0", 
                                                                _columnNames[5], 
                                                                name.PhoneNotification ? "1" : "0"
                                                                ));
        }

        public void UpdateName(DBClientName name)
        {
            _connectionEstablisher.RunNonQuerySQL(string.Format("UPDATE '{0}' SET {3}='{4}', {5}='{6}', {7}='{8}', {9}='{10}', {11}='{12}' WHERE {1}='{2}';", _tableName, _columnNames[0], name.Id, _columnNames[1], name.SubName, _columnNames[2], name.Name, _columnNames[3], name.FatherName, _columnNames[4], name.AgreeSubscription ? "1":"0", _columnNames[5], name.PhoneNotification ? "1" : "0"));
        }

        public void DeleteNameFromId(int id)
        {
            _connectionEstablisher.RunNonQuerySQL(string.Format("DELETE FROM '{0}' WHERE {1}='{2}';", _tableName, _columnNames[0], id));
        }

        public DBClientName GetClientNameFromId(int id)
        {
            StringBuilder comand = new StringBuilder();
            comand.Append(string.Format("SELECT * FROM '{0}' WHERE ", _tableName));
            
            comand.Append(string.Format("{0}={1};", _columnNames[0], id));

            var reader = _connectionEstablisher.RunQuerySQL(comand.ToString());
            
            var ans = (from DbDataRecord record in reader select 
                    new DBClientName()
                    {
                        Id = int.Parse(record[_columnNames[0]].ToString()),
                        SubName = record[_columnNames[1]].ToString(),
                        Name = record[_columnNames[2]].ToString(),
                        FatherName = record[_columnNames[3]].ToString(),
                        AgreeSubscription = (bool)record[_columnNames[4]],
                        PhoneNotification = (bool)record[_columnNames[5]]
                    }
                    ).ToArray();

            if (ans.Length > 1)
                throw new Exception("more 1 ids count! Id=" + id);

            if (ans.Length < 1)
                return null;

            return ans[0];
        }

        public int GetMaxId()
        {
            var reader = _connectionEstablisher.RunQuerySQL(string.Format("SELECT MAX({0}) FROM {1};", _columnNames[0], _tableName));
            reader.Read();
            var value = reader.GetValue(0).ToString();
            return int.Parse(string.IsNullOrEmpty(value)? "0" : value);
        }

        public DBClientName[] GetAllClientNames()
        {
            var reader = _connectionEstablisher.RunQuerySQL(string.Format("SELECT * FROM {0};", _tableName));
            
            return (from DbDataRecord record in reader select
                new DBClientName()
                {
                    Id = int.Parse(record[_columnNames[0]].ToString()),
                    SubName = record[_columnNames[1]].ToString(),
                    Name = record[_columnNames[2]].ToString(),
                    FatherName = record[_columnNames[3]].ToString(),
                    AgreeSubscription = (bool)record[_columnNames[4]],
                    PhoneNotification = (bool)record[_columnNames[5]]
                }).ToArray();
        }
    }
}
