﻿using AutomaticalChemistry.DatabaseWork.HAPI;
using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.DatabaseWork.LAPI.Tables
{
    public class PhoneNumbersTable : IDBTable
    {
        const string _tableName = "PhoneNumbers";

        string[] _columnNames = new string[] { "Id", "Owner", "Number" };
        string[] _columnTypes = new string[] { "INTEGER PRIMARY KEY", "INTEGER", "Text" };

        DBConnectionEstablisher _connectionEstablisher;

        public void Init(DBConnectionEstablisher connectionEstablisher)
        {
            StringBuilder comandBuilder = new StringBuilder("(");
            for (int i = 0; i < _columnNames.Length; i++)
            {
                if (i != 0)
                    comandBuilder.Append(", ");
                comandBuilder.Append(_columnNames[i]);
                comandBuilder.Append(" ");
                comandBuilder.Append(_columnTypes[i]);
            }
            comandBuilder.Append(")");

            _connectionEstablisher = connectionEstablisher;
            _connectionEstablisher.CheckOrCreateTable(_tableName, comandBuilder.ToString());
        }

        //ВНИМАНИЕ Id тут Не устанавливается!
        public void WriteNewPhoneNums(IList<DBPhoneNum> nums)
        {
            StringBuilder comand = new StringBuilder();
            foreach (var num in nums)
            {
                comand.Append(string.Format("INSERT INTO '{0}' ('{1}', '{3}') VALUES ('{2}', '{4}');", _tableName, _columnNames[1], num.Owner, _columnNames[2], num.Value));
            }

            _connectionEstablisher.RunNonQuerySQL(comand.ToString());
        }

        public void DeleteNumsFromId(int id)
        {
            _connectionEstablisher.RunNonQuerySQL(string.Format("DELETE FROM '{0}' WHERE {1}='{2}';", _tableName, _columnNames[1], id));
        }

        public int SetInfoForPhoneNums(IList<DBPhoneNum> numbers)
        {
            StringBuilder comand = new StringBuilder();
            comand.Append(string.Format("SELECT * FROM '{0}' Where ", _tableName));
            
            bool flag = false;

            foreach (var number in numbers)
            {
                if (flag)
                    comand.Append("OR ");

                comand.Append(string.Format("{0}='{1}' ", _columnNames[2], number.Value));

                flag = true;
            }

            comand.Append(";");
            var reader = _connectionEstablisher.RunQuerySQL(comand.ToString());

            int answer = 0;
            foreach (DbDataRecord record in reader)
            {
                foreach (var number in numbers)
                {
                    if (number.Value == record[_columnNames[2]].ToString())
                    {
                        number.Id = int.Parse(record[_columnNames[0]].ToString());
                        number.Owner = int.Parse(record[_columnNames[1]].ToString());
                        answer++;
                        break;
                    }
                }
            }

            return answer;
        }

        public DBPhoneNum[] GetIdClientNumbers(int id)
        {
            return GetNumbersFromComand(string.Format("SELECT * FROM '{0}' WHERE {1}={2};", _tableName, _columnNames[1], id));
        }

        public DBPhoneNum[] GetAllNumbers()
        {
            return GetNumbersFromComand(string.Format("SELECT * FROM '{0}';", _tableName));
        }

        DBPhoneNum[] GetNumbersFromComand(string comand)
        {
            var reader = _connectionEstablisher.RunQuerySQL(comand);
            
            return (from DbDataRecord record in reader select 
                    new DBPhoneNum()
                    {
                        Id = int.Parse(record[_columnNames[0]].ToString()),
                        Owner = int.Parse(record[_columnNames[1]].ToString()),
                        Value = record[_columnNames[2]].ToString()
                    }
                    ).ToArray();
        }
    }
}
