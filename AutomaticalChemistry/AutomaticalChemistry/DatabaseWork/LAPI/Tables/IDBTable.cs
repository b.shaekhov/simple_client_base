﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.DatabaseWork.LAPI.Tables
{
    interface IDBTable
    {
        void Init(DBConnectionEstablisher connectionEstablisher);
    }
}
