﻿using AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.DatabaseWork.LAPI.Tables
{
    public class AdditionalOrdersInfoTable : IDBTable
    {
        const string _tableName = "AdditionalOrdersInfo";
        string[] _columnNames = new string[] { "Id"                 , "OrderId", "ObjectType", "DirtValue", "WearValue", "CoatingState", "ClayAndDetailsExist", "DifColor", "MarkExist", "PayType" , "PrePayPercent" };
        string[] _columnTypes = new string[] { "INTEGER PRIMARY KEY", "INTEGER", "INTEGER"   , "INTEGER"  , "INTEGER"  , "INTEGER"     , "INTEGER"            , "INTEGER" , "INTEGER"  , "INTEGER" , "INTEGER" };

        DBConnectionEstablisher _connectionEstablisher;

        public void Init(DBConnectionEstablisher connectionEstablisher)
        {
            StringBuilder comandBuilder = new StringBuilder("(");
            for (int i = 0; i < _columnNames.Length; i++)
            {
                if (i != 0)
                    comandBuilder.Append(", ");
                comandBuilder.Append(_columnNames[i]);
                comandBuilder.Append(" ");
                comandBuilder.Append(_columnTypes[i]);
            }
            comandBuilder.Append(")");

            _connectionEstablisher = connectionEstablisher;
            _connectionEstablisher.CheckOrCreateTable(_tableName, comandBuilder.ToString());
        }

        //ВНИМАНИЕ Id тут Не устанавливается!
        public void WriteNewInfo(DBAdditionalOrderInfo info)
        {
            _connectionEstablisher.RunNonQuerySQL(string.Format("INSERT INTO '{0}' ('{1}', '{3}', '{5}', '{7}',  '{9}', '{11}', '{13}', '{15}', '{17}', '{19}') VALUES " +
                                                                                  "('{2}', '{4}', '{6}', '{8}', '{10}', '{12}', '{14}', '{16}', '{18}', '{20}');", 
                                                                _tableName,
                                                                _columnNames[1],
                                                                info.OrderId, 
                                                                _columnNames[2],
                                                                info.ObjectType,
                                                                _columnNames[3],
                                                                info.DirtValue,
                                                                _columnNames[4],
                                                                info.WearValue,
                                                                _columnNames[5],
                                                                info.CoatingState,
                                                                _columnNames[6],
                                                                info.ClayAndDetailsExist,
                                                                _columnNames[7],
                                                                info.DifColor,
                                                                _columnNames[8],
                                                                info.MarkExist,
                                                                _columnNames[9],
                                                                info.PayType,
                                                                _columnNames[10],
                                                                info.PrePayPercent
                                                                ));
        }

        public DBAdditionalOrderInfo[] GetOrderIdAdditionalInfo(int id)
        {
            return GetAdditionalInfoFromComand(string.Format("SELECT * FROM '{0}' WHERE {1}={2};", _tableName, _columnNames[1], id));
        }

        public DBAdditionalOrderInfo[] GetAllAdditionalInfo()
        {
            return GetAdditionalInfoFromComand(string.Format("SELECT * FROM '{0}';", _tableName));
        }

        DBAdditionalOrderInfo[] GetAdditionalInfoFromComand(string comand)
        {
            var reader = _connectionEstablisher.RunQuerySQL(comand);
            
            return (from DbDataRecord record in reader select 
                    new DBAdditionalOrderInfo()
                    {
                        Id              = int.Parse(record[_columnNames[0]].ToString()),
                        OrderId         = int.Parse(record[_columnNames[1]].ToString()),
                        ObjectType      = int.Parse(record[_columnNames[2]].ToString()),
                        DirtValue       = int.Parse(record[_columnNames[3]].ToString()),
                        WearValue       = int.Parse(record[_columnNames[4]].ToString()),
                        CoatingState    = int.Parse(record[_columnNames[5]].ToString()),
                        ClayAndDetailsExist = int.Parse(record[_columnNames[6]].ToString()),
                        DifColor            = int.Parse(record[_columnNames[7]].ToString()),
                        MarkExist           = int.Parse(record[_columnNames[8]].ToString()),
                        /*Price = new Price()
                        {
                            PriceWholePart      = int.Parse(record[_columnNames[9]].ToString()),
                            PriceFractionalPart = int.Parse(record[_columnNames[10]].ToString())
                        },*/
                        PayType       = int.Parse(record[_columnNames[9]].ToString()),
                        PrePayPercent = int.Parse(record[_columnNames[10]].ToString()),
                    }
                    ).ToArray();
        }

        public void DeleteInfo(DBAdditionalOrderInfo info)
        {
            _connectionEstablisher.RunNonQuerySQL(string.Format("DELETE FROM '{0}' WHERE {1}='{2}';", _tableName, _columnNames[1], info.OrderId));
        }
    }
}
