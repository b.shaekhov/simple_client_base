﻿using System.Data.SQLite;
using System.IO;

namespace AutomaticalChemistry.DatabaseWork.LAPI
{
    public class DBConnectionEstablisher
    {
        const string databasePath = @"Resources\ChemistryDB.db";
        private static SQLiteConnection _dbConnection = null;

        public void Connect()
        {
            CheckOrCreateDBFile();
            OpenConnection();
        }

        static void CheckOrCreateDBFile()
        {
            if (!File.Exists(databasePath))
                SQLiteConnection.CreateFile(databasePath);
        }

        static void OpenConnection()
        {
            _dbConnection = new SQLiteConnection(string.Format("Data Source={0};", databasePath));
            _dbConnection.Open();
        }

        /*public string[] GetAllTableNames()
        {
            SQLiteCommand command = new SQLiteCommand("SELECT name FROM sqlite_master WHERE type='table';", _dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            return (from DbDataRecord x in reader select x["name"].ToString()).ToArray();
        }*/

        public bool TableExist(string name)
        {
            var reader = RunQuerySQL("SELECT COUNT(name), name FROM sqlite_master WHERE type='table' AND name='" + name + "';");
            reader.Read();
            return int.Parse(reader.GetValue(0).ToString()) > 0;
        }

        public void CheckOrCreateTable(string name, string createParams) //createParams like: (id INTEGER PRIMARY KEY, phone_number TEXT)
        {
            if (!TableExist(name))
                RunNonQuerySQL("CREATE TABLE " + name + " "+createParams+";");

        }

        public SQLiteDataReader RunQuerySQL(string command)
        {
            SQLiteCommand Objcommand = new SQLiteCommand(command, _dbConnection);
            return Objcommand.ExecuteReader();
        }

        public void RunNonQuerySQL(string command)
        {
            SQLiteCommand Objcommand =
                new SQLiteCommand(command, _dbConnection);
            Objcommand.ExecuteNonQuery();
        }
        
        public void Disconnect()
        {
            _dbConnection.Close();
        }
    }
}
