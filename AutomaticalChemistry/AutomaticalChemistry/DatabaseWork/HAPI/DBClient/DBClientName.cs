﻿using AutomaticalChemistry.MainLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.DatabaseWork.HAPI.DBClient
{
    public class DBClientName
    {
        public static string[] ClientVarsNames = {
                                        "Фамилия",
                                        "Имя",
                                        "Отчество",
                                        "Рассылка",
                                        "СМС уведомления"
                                        };

        public static string[] AgreeSubscriptionTexts = new string[] { "Рассылать", "Не_рассылать" };
        public static string[] PhoneNotificationTexts = new string[] { "Уведомлять", "Не_уведомлять" };

        public int Id = int.MinValue;
        public string SubName;
        public string Name;
        public string FatherName;
        public bool AgreeSubscription;
        public bool PhoneNotification;

        public string GetValueFromNameIndex(int id)
        {
            switch (id)
            {
                case 0: return SubName;
                case 1: return Name;
                case 2: return FatherName;
                case 3: return AgreeSubscriptionTexts[AgreeSubscription ? 0 : 1];
                case 4: return PhoneNotificationTexts[PhoneNotification ? 0 : 1];
            }

            return null;
        }

        public static bool operator ==(DBClientName a, DBClientName b)
        {
            return a.SubName == b.SubName && a.Name == b.Name && a.FatherName == b.FatherName;
        }

        public static bool operator !=(DBClientName a, DBClientName b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (obj is DBClientName)
                return this == (DBClientName)obj;

            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return SubName.GetHashCode() + Name.GetHashCode() + FatherName.GetHashCode();
        }

        public override string ToString()
        {
            return SubName + " " + Name + " " + FatherName;
        }
    }
}
