﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.DatabaseWork.HAPI.DBClient
{
    public class DBClientInfo
    {
        public DBClientName Name;
        public DBPhoneNum[] PhoneNumbers = new DBPhoneNum[0];

        public string BuildStringFromPhoneNums()
        {
            var ans = new StringBuilder();

            foreach (var num in PhoneNumbers)
            {
                ans.Append(num.Value);
                ans.Append("; ");
            }
            
            return ans.ToString();
        }

        public override string ToString()
        {
            var ans = new StringBuilder(/*"\r\n"+*/Name.Name.ToString());

            foreach (var num in PhoneNumbers)
            {
                ans.Append("\r\n");
                ans.Append(num.Value);
            }

            return ans.ToString();
        }
    }
}
