﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.DatabaseWork.HAPI.DBClient
{
    public class DBPhoneNum
    {
        public int Id = int.MinValue;
        public int Owner = int.MinValue;
        public string Value;

        public bool CheckNewNum()
        {
            if (Id == int.MinValue && Owner == int.MinValue)
                return true;

            if (Id == int.MinValue || Owner == int.MinValue)
                throw new Exception("Id or Owner == int.MinValue!");

            return false;
        }
    }
}
