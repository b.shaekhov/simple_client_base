﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo
{
    public class DBOrder
    {
        public DBMainOrderInfo MainInfo;
        public DBAdditionalOrderInfo AdditionalInfo;
        public DBOrderSpot[] Spots;

        public Price FullPrice
        {
            get
            {
                Price sum = Price.Zero;
                foreach (var spot in Spots)
                {
                    sum += spot.Price;
                }
                return sum;
            }
        }

        public string BuildStringFromSpots()
        {
            var ans = new StringBuilder();

            foreach (var spot in Spots)
            {
                ans.Append(spot.Description);
                ans.Append(";    ");
            }

            return ans.ToString();
        }

        public class DBOrderEndDateComparer : IComparer<DBOrder>
        {
            public int Compare(DBOrder x, DBOrder y)
            {
                if (x == null)
                    return 1;
                if (y == null)
                    return -1;

                if (x.MainInfo.Executed && !y.MainInfo.Executed)
                    return 1;

                if (!x.MainInfo.Executed && y.MainInfo.Executed)
                    return -1;

                return x.MainInfo.EndDate.CompareTo(y.MainInfo.EndDate);
            }
        }
    }
}
