﻿using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo
{
    public class DBMainOrderInfo
    {
        public static string[] OrderMainInfoVarsNames = {
                                        "Заказ №",
                                        "Дата приёма",
                                        "Дата выполнения",
                                        "Наименование изделия",
                                        "Исполненность",
                                        "Информация о клиенте"
                                      };

        public int Id = -1;

        public string DBStartDate { get { return StartDate.ToString("yyyy-MM-dd"); } set { StartDate = DateTime.Parse(value); } }
        public string DBEndDate { get { return EndDate.ToString("yyyy-MM-dd"); } set { EndDate = DateTime.Parse(value); } }

        public DateTime StartDate;
        public DateTime EndDate;
        
        public string ObjectName;

        public bool Executed;
        public DBClient.DBClientInfo ClientInfo;

        public string GetValueFromNameIndex(int id)
        {
            switch (id)
            {
                case 0: return Id.ToString();
                case 1: return StartDate.ToString("dd.MM.yyyy");
                case 2: return EndDate.ToString("dd.MM.yyyy");
                case 3: return ObjectName;
                case 4: return Executed ? "Исполнено" : "Не исполнено";
                case 5: return ClientInfo.ToString();
            }

            return null;
        }
    }
}
