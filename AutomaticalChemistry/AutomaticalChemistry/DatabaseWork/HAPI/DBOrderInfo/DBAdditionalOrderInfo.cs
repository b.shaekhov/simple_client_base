﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo
{
    public class DBAdditionalOrderInfo
    {
        /*ObjectType        
        DirtValue           
        WearValue           
        CoatingState        
        ClayAndDetailsExist 
        DifColor            
        MarkExist           
        Price               
        PayType             
        PrePayPercent       */

        public static string[] OrderAdditionalInfoVarsNames = {
                                        "Тип изделия",
                                        "Степень загрязнения",
                                        "Износ",
                                        "Дефекты покрытия",
                                        "Наличие клеевых швов и деталей",
                                        "Разноотенночность кусков изделия",
                                        "Наличие маркировки",
                                        //"Сумма",
                                        "Тип оплаты",
                                        "Предоплата%"
                                      };

        public static string[] ObjectTypeTexts      = new string[] { "Мужской", "Женский", "Универсал" };
        public static string[] DirtValueTexts       = new string[] { "Очень сильное", "Сильное", "Общее", "Наличие пятен", "Засаленость" };
        public static string[] WearValueTexts       = new string[] { "0%", "10%", "30%", "50%", "80%", "100%" };
        public static string[] CoatingStateTexts    = new string[] { "Сход покрытия", "Слабое закрепление красителя", "Выгорание", "Нет" };
        public static string[] ClayOrDetailsTexts   = new string[] { "Имеется", "Не имеется" };
        public static string[] DifColorTexts        = new string[] { "Имеется", "Не имеется" };
        public static string[] MarkExistTexts       = new string[] { "Имеется", "Отсутствует", "Запрещающее химчистку" };
        public static string[] PayTypeTexts         = new string[] { "Наличные", "Карта", "Другое" };
        

        public int Id = -1;
        public int OrderId = -1;

        public int ObjectType = -1;
        public int DirtValue = -1;
        public int WearValue = -1;
        public int CoatingState = -1;
        public int ClayAndDetailsExist = -1;
        public int DifColor = -1;
        public int MarkExist = -1;
        //public Price Price;
        public int PayType = -1;
        public int PrePayPercent = -1;

        public string GetValueFromNameIndex(int id)
        {
            switch (id)
            {
                case 0: return ObjectTypeTexts[ObjectType];
                case 1: return BuildDecodedTexts(DirtValue, DirtValueTexts);
                case 2: return WearValueTexts[WearValue];
                case 3: return BuildDecodedTexts(CoatingState, CoatingStateTexts);
                case 4: return ClayOrDetailsTexts[ClayAndDetailsExist];
                case 5: return DifColorTexts[DifColor];
                case 6: return MarkExistTexts[MarkExist];
                //case 7: return Price.ToString();
                case 7: return PayTypeTexts[PayType];
                case 8: return PrePayPercent.ToString();
            }

            return null;
        }

        public static string BuildDecodedTexts(int bitFlagsInt, string[] values)
        {
            StringBuilder ansBuilder = new StringBuilder();
            int counter = 0;
            while (bitFlagsInt > 0)
            {
                if ((bitFlagsInt & 1) != 0)
                {
                    ansBuilder.Append(values[counter] + "; ");
                }

                bitFlagsInt >>= 1;

                counter++;
            }
            return ansBuilder.ToString();
        }
    }
}
