﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo
{
    public class Price
    {
        public static Price Zero { get { return new Price(); } }
        public const string WholePartName = "руб.";
        public const string FractionalPartName = "коп.";

        public int PriceWholePart = 0;
        public int PriceFractionalPart = 0;

        public override string ToString()
        {
            return PriceWholePart + " " + WholePartName + " " + PriceFractionalPart + " " + FractionalPartName;
        }

        public static Price operator + (Price a, Price b)
        {
            var fract = a.PriceFractionalPart + b.PriceFractionalPart;
            var whole = a.PriceWholePart + b.PriceWholePart + (int)(fract / 100);
            fract %= 100;
            return new Price() { PriceWholePart = whole, PriceFractionalPart = fract };
        }
    }
}
