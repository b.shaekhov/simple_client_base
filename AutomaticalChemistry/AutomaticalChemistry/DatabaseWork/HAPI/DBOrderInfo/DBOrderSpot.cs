﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo
{
    public class DBOrderSpot
    {
        public int Id = -1;
        public int OrderId = -1;
        public string Description;
        public Price Price;
    }
}
