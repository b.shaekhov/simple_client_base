﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutomaticalChemistry.DatabaseWork.LAPI;
using AutomaticalChemistry.DatabaseWork.LAPI.Tables;
using AutomaticalChemistry.DatabaseWork.HAPI.DBClient;
using AutomaticalChemistry.DatabaseWork.HAPI.DBOrderInfo;
using System.Windows.Forms;

namespace AutomaticalChemistry.DatabaseWork.HAPI
{
    public static class DBController
    {
        static DBConnectionEstablisher _connectionEstablisher;
        static ClientNamesTable _clientNamesTable;
        static PhoneNumbersTable _phoneNumbersTable;
        static MainOrdersInfoTable _mainOrdersInfoTable;
        static AdditionalOrdersInfoTable _additionalOrdersInfoTable;
        static OrderSpotsTable _orderSpotsTable;

        public static void Init()
        {
            _connectionEstablisher = new DBConnectionEstablisher();
            _connectionEstablisher.Connect();
            _clientNamesTable = new ClientNamesTable();
            _phoneNumbersTable = new PhoneNumbersTable();
            _mainOrdersInfoTable = new MainOrdersInfoTable();
            _additionalOrdersInfoTable = new AdditionalOrdersInfoTable();
            _orderSpotsTable = new OrderSpotsTable();
            _clientNamesTable.Init(_connectionEstablisher);
            _phoneNumbersTable.Init(_connectionEstablisher);
            _mainOrdersInfoTable.Init(_connectionEstablisher);
            _additionalOrdersInfoTable.Init(_connectionEstablisher);
            _orderSpotsTable.Init(_connectionEstablisher);
        }

        public static void CreateAbsolutlyNewClientInfo(DBClient.DBClientInfo info)
        {
            _clientNamesTable.CreateName(info.Name);
            
            for (int i = 0; i < info.PhoneNumbers.Length; i++)
            {
                info.PhoneNumbers[i].Owner = info.Name.Id;
            }

            WriteNewPhoneNums(info.PhoneNumbers);
        }

        public static void CreateClientToId(DBClient.DBClientInfo info)
        {
            _clientNamesTable.CreateNameFromId(info.Name);
            WriteNewPhoneNums(info.PhoneNumbers);
        }

        public static void RemoveClientFromId(int id)
        {
            _clientNamesTable.DeleteNameFromId(id);
            _phoneNumbersTable.DeleteNumsFromId(id);
        }

        /*public static void UpdateClientName(DBClientName name)
        {
            _clientNamesTable.UpdateName(name);
        }*/

        public static void WriteNewPhoneNums(IList<DBPhoneNum> nums)
        {
            _phoneNumbersTable.WriteNewPhoneNums(nums);
        }
        
        public static int SetInfoForPhoneNums(IList<DBPhoneNum> numbers)
        {
            return _phoneNumbersTable.SetInfoForPhoneNums(numbers);
        }

        public static DBClientName GetClientNameFromId(int id)
        {
            return _clientNamesTable.GetClientNameFromId(id);
        }
        
        public static DBClient.DBClientInfo[] GetAllClientInfo(bool nullableArray)
        {
            var maxId = _clientNamesTable.GetMaxId();
            DBClient.DBClientInfo[] answer = new DBClient.DBClientInfo[maxId + 1];

            var allNames = _clientNamesTable.GetAllClientNames();

            foreach (var name in allNames)
            {
                answer[name.Id] = new DBClient.DBClientInfo()
                {
                    Name = name
                };
            }

            var allNumbers = _phoneNumbersTable.GetAllNumbers();

            foreach (var number in allNumbers)
            {
                if (answer[number.Owner].PhoneNumbers == null)
                {
                    answer[number.Owner].PhoneNumbers = new DBPhoneNum[1];
                } else
                {
                    Array.Resize(ref answer[number.Owner].PhoneNumbers, answer[number.Owner].PhoneNumbers.Length+1);
                }

                answer[number.Owner].PhoneNumbers[answer[number.Owner].PhoneNumbers.Length - 1] = number;
            }

            if (nullableArray)
                return answer;
            else 
                return (from x in answer where x!=null select x).ToArray();
        }

        public static DBOrder[] GetAllOrders()
        {
            var maxId = GetMaxOrderId();
            DBOrder[] answer = new DBOrder[maxId + 1];

            var allMainInfo = _mainOrdersInfoTable.GetAllOrdersInfo();
            var allAdditionalInfo = _additionalOrdersInfoTable.GetAllAdditionalInfo();
            var allSpots = _orderSpotsTable.GetAllSpotsInfo();

            foreach (var oneMainInfo in allMainInfo)
            {
                answer[oneMainInfo.Id] = new DBOrder();
                answer[oneMainInfo.Id].MainInfo = oneMainInfo;
            }

            foreach (var oneAdditionalInfo in allAdditionalInfo)
            {
                answer[oneAdditionalInfo.OrderId].AdditionalInfo = oneAdditionalInfo;
            }

            foreach (var oneSpot in allSpots)
            {
                if (answer[oneSpot.OrderId].Spots == null)
                {
                    answer[oneSpot.OrderId].Spots = new DBOrderSpot[] { oneSpot };
                } else
                {
                    Array.Resize(ref answer[oneSpot.OrderId].Spots, answer[oneSpot.OrderId].Spots.Length+1);
                    answer[oneSpot.OrderId].Spots[answer[oneSpot.OrderId].Spots.Length - 1] = oneSpot;
                }
            }

            return (from x in answer where x!=null select x).ToArray();
        }

        public static int GetClientOrdersCount(int id)
        {
            return _mainOrdersInfoTable.GetClientOrdersCount(id);
        }
        
        public static DBClientInfo GetClientInfoFromId(int id)
        {
            var name = _clientNamesTable.GetClientNameFromId(id);
            var nums = _phoneNumbersTable.GetIdClientNumbers(id);

            return new DBClient.DBClientInfo()
            {
                Name = name,
                PhoneNumbers = nums
            };
        }

        public static DBPhoneNum[] GetAllPhoneNumbers()
        {
            return _phoneNumbersTable.GetAllNumbers();
        }

        public static DBClientName[] GetAllClientNames()
        {
            return _clientNamesTable.GetAllClientNames();
        }

        public static void UpdateExecuteOrder(DBOrder order)
        {
            _mainOrdersInfoTable.UpdateOrderInfo(order.MainInfo);
        }

        public static void DeleteOrder(DBOrder order)
        {
            _mainOrdersInfoTable.DeleteInfo(order.MainInfo);
            _additionalOrdersInfoTable.DeleteInfo(order.AdditionalInfo);
            _orderSpotsTable.DeleteSpots(order.Spots[0].OrderId);
        }

        /*public static void CreateOldOrder(DBOrder order)
        {
            _mainOrdersInfoTable.MainInfoFromId(order.MainInfo);
            order.AdditionalInfo.OrderId = order.MainInfo.Id;
            foreach (var spot in order.Spots)
                spot.OrderId = order.MainInfo.Id;
            _additionalOrdersInfoTable.WriteNewInfo(order.AdditionalInfo);
            _orderSpotsTable.WriteNewSpots(order.Spots);
        }*/

        public static void CreateNewOrder(DBOrder order)
        {
            _mainOrdersInfoTable.NewMainInfo(order.MainInfo);
            order.AdditionalInfo.OrderId = order.MainInfo.Id;
            foreach (var spot in order.Spots)
                spot.OrderId = order.MainInfo.Id;
            _additionalOrdersInfoTable.WriteNewInfo(order.AdditionalInfo);
            _orderSpotsTable.WriteNewSpots(order.Spots);
        }

        public static int GetMaxOrderId()
        {
            return _mainOrdersInfoTable.GetMaxId();
        }

        public static void Close()
        {
            _connectionEstablisher.Disconnect();
        }
    }
}
